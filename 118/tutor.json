{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第一讲：图形的平移与旋转",
            "description": "1.\n掌握平移与旋转的概念，探索它们的基本性质.\n<br/>2.\n能够按要求作出简单平面图形平移后或旋转后的图形.\n"
        },
        {
            "kind": "kpt",
            "name": "平移的定义",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "平移的定义",
                    "sub": [
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在平面内，将一个图形沿着某个方向移动一定的距离，这样的图形运动称为平移.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "平移的要素",
                    "sub": [
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平移的两个要素为：平移的方向（直线方向）和平移的距离.\n<br><b >注：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平移只改变图形的位置，不改变图形的形状、大小、方向.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 平移的识别",
            "type": 1,
            "Q": "下列各组图形中，可以经过平移由一个图形得到另一个图形的是（   ）<br/>",
            "way": "<i>A</i>中两个三角形的形状、大小、方向完全相同，符合要求；<i>B</i>中两个正方形大小不同，不符合要求；<i>C</i>中两个长方形形状和大小相同，但方向不相同；<i>D</i>中两个图形的形状和大小一样，但方向不同，不符合要求.\n",
            "step": "故选 A",
            "notice": "",
            "A": "",
            "opts": [
                "<img src=\"2.png\" style=\"width: 10em;\">",
                "<img src=\"8.png\" style=\"width: 10em;\">",
                "<img src=\"9.png\" style=\"width: 10em;\">",
                "<img src=\"10.png\" style=\"width: 6em;\">"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": " 下列运动属于平移的是（     ）",
            "way": "紧扣平移的特征：图形位置改变，形状、大小方向都不变.\n判断即可.\n",
            "step": "<i>A</i>项中小气泡旋转上升且变大，不符合平移特征；<i>B</i>选项符合平移特征；<i>C</i>选项抛出的小石子运动方向不是直线方向；<i>D</i>选项中风筝在空中的移动方向不断变化.\n<br/>故选 B",
            "notice": "",
            "A": "",
            "opts": [
                "冷水加热过程中，水中小气泡上升成大气泡",
                "急刹车时汽车在地面上的滑动",
                "随手抛出的小石子的运动",
                "随风飘动的风筝在空中的运动"
            ]
        },
        {
            "kind": "kpt",
            "name": "平移的性质",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "",
                    "sub": [
                        "平移后，新图形与原图形的形状、大小及方向完全相同.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "",
                    "sub": [
                        "一个图形和它经过平移所得的图形中，对应点所连的线段平行（或在一条直线上）且相等；对应线段平行（或在一条直线上）且相等，对应角相等.\n<br>注：<br>（1）连接对应点的线段的长度就是平移的距离；<br>（2）起始点到到终止点的方向为平移的方向.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 平移元素的认识及性质的应用",
            "type": 9,
            "Q": " 如图，梯形<var class = \"latex\">EFGH</var>是由梯形<var class = \"latex\">ABCD</var>向右平移<var class = \"latex\">2cm</var>后得到的.\n<br/><img style = \"width:12.13em;vertical-align:middle;float:right;\" src = \"4.png\">（1）指出图中的对应点、对应线段和对应角.\n<br/>（2）线段<var class = \"latex\">AE</var>，<var class = \"latex\">BF</var>，<var class = \"latex\">CG</var>，<var class = \"latex\">DH</var>之间有什么数量关系？<br/>（3）<var class = \"latex\">AB</var>与<var class = \"latex\">EF</var>，<var class = \"latex\">BC</var>与<var class = \"latex\">FG</var>，<var class = \"latex\">CD</var>与<var class = \"latex\">GH</var>，<var class = \"latex\">AD</var>与<var class = \"latex\">EH</var>之间有什么关系？<br/>（4）<var class = \"latex\">∠BAD</var>与<var class = \"latex\">∠FEH</var>，<var class = \"latex\">∠ABC</var>与<var class = \"latex\">∠EFG</var>，<var class = \"latex\">∠BCD</var>与<var class = \"latex\">∠FGH</var>，<var class = \"latex\">∠ADC</var>与<var class = \"latex\">∠EHG</var>之间有什么数量关系？",
            "way": "根据平移的性质可知：平移只改变图形的位置，不改变图形的大小；平移得到的图形与原来的图形是完全一样的，所以对应的线段之间是平行且相等的.\n",
            "step": "（1）线段<var class = \"latex\">AE</var>，<var class = \"latex\">BF</var>，<var class = \"latex\">CG</var>，<var class = \"latex\">DH</var>的长度相等，都为<var class = \"latex\">2cm</var>；<br/>（2）<var class = \"latex\">AB</var>与<var class = \"latex\">EF</var>，<var class = \"latex\">BC</var>与<var class = \"latex\">FG</var>，<var class = \"latex\">CD</var>与<var class = \"latex\">GH</var>，<var class = \"latex\">AD</var>与<var class = \"latex\">EH</var>平行且相等.\n<br/>（3）<var class = \"latex\">∠BAD</var>与<var class = \"latex\">∠FEH</var>，<var class = \"latex\">∠ABC</var>与<var class = \"latex\">∠EFG</var>，<var class = \"latex\">∠BCD</var>与<var class = \"latex\">∠FGH</var>，<var class = \"latex\">∠ADC</var>与<var class = \"latex\">∠EHG</var>对应相等.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "平移的作图",
            "sub": [
                "    平移作图是平移性质的应用，利用平移可以得到许多美丽的图案，具体作图时应分四步：",
                "<b >（1）定：</b>确定平移的方向和距离；",
                "<b >（2）找：</b>找出图形的关键点（图形的顶点，拐点，连接点）；",
                "<b >（3）移：</b>过关键点作平行且相等的线段，得到关键点的对应点；",
                "<b >（4）连：</b>按原图顺次连接对应点.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一 格点图中的平移作图",
            "type": 9,
            "Q": "<img style = \"width:16.25em;vertical-align:middle;float:right;\" src = \"6.png\"> 已知如图所示的图案及图案上的点<var class = \"latex\">A</var>，把图案平移后，<var class = \"latex\">A</var>点的对应点为<var class = \"latex\">A'</var>点，请你利用两种不用的画法画出平移后的图形.\n",
            "way": "画图的关键是在于画出关键点平移后的对应点，可以通过特殊点平移情况作图，也可以通过整体的平移情况，作出平移后的图形.\n",
            "step": "<img style = \"width:16.25em;vertical-align:middle;float:right;\" src = \"7.png\">画法一：连接<var class = \"latex\">AA'</var>，过图案各顶点分别作<var class = \"latex\">AA'</var>的平行线，并截取长度等于<var class = \"latex\">AA'</var>的长度，得到各顶点的对应点；顺次连接各顶点，即可.\n<br/>画法二：把图案的各顶点分别向上平移1格，再向右平移5格，得到各顶点的对应点，顺次连接各顶点，即可.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "<img style = \"width:9.94em;vertical-align:middle;float:right;\" src = \"11.png\">如图，已知每个小正方形的边长都是<var class = \"latex\">1</var>个单位长度，将图中的格点三角形<var class = \"latex\">ABC</var>先向右平移<var class = \"latex\">3</var>个单位长度，再向上平移<var class = \"latex\">2</var>个单位长度，得到三角形<var class = \"latex\">A<sub>1</sub>B<sub>1</sub>C<sub>1</sub></var>，请在图中画出三角形<var class = \"latex\">A<sub>1</sub>B<sub>1</sub>C<sub>1</sub></var>.\n",
            "way": "",
            "step": "<img style = \"width:10.44em;vertical-align:middle;float:right;\" src = \"13.png\"><b >如右图：</b>",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "平面直角坐标系中，图形的平移变换",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "图形在坐标平面内的平移",
                    "sub": [
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在坐标系中，在保持坐标轴不动的情况下，图形的整体移动.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "图形的平移与图形上各点的坐标变化的关系",
                    "sub": [
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（1）图形的平移是整体平移，所以已知图形的平行情况，即可得到图形上各点坐标的变化情况；",
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（2）平移时，因为图形上各点的变化情况相同，所以已知图形上某点的坐标变化情况，即可知道图形的变化情况.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一\n由点的坐标变化确定平移情况",
            "type": 9,
            "Q": "<img style = \"width:12.44em;vertical-align:middle;float:right;\" src = \"15.png\"> 如图，已知<var class = \"latex\">A( －1，0 )</var>，<var class = \"latex\">B(\n1，1 )</var>，把线段<var class = \"latex\">AB</var>平移，使点<var class = \"latex\">B</var>移动到点<var class = \"latex\">D</var><var class = \"latex\">( 3，4 )</var>处，这时点<var class = \"latex\">A</var>移动到点<var class = \"latex\">C</var>处.\n<br/>（1）请写出点<var class = \"latex\">C</var>的坐标；<br/>（2）如果平移只能左右或者上下移动，叙述线段<var class = \"latex\">AB</var>是怎样平移得到线段<var class = \"latex\">CD</var>的.\n",
            "way": "根据图形的变化是整体变化，点<i>A</i>的平移情况和点<i>B</i>一样，即可得解.\n",
            "step": "（1）点<i>C</i>的坐标为<var class = \"latex\">( 1，3 )</var>；<br/>（2）线段<i>AB</i>向右平移了2个单位，向上平移了3个单位.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型二 由图形的平移情况确定点的坐标",
            "type": 1,
            "Q": "如图<img style = \"width:12.31em;vertical-align:middle;;float:right;\" src = \"19.png\">，在平面直角坐标系中，△<var class = \"latex\">ABC</var>的顶点都在方格纸的格点上，如果将△<i>ABC</i>先向右移<var class = \"latex\">4</var>个单位长度，再向下平移<var class = \"latex\">1</var>个单位长度，得到△<var class = \"latex\">A<sub>1</sub>B<sub>1</sub>C<sub>1</sub></var>，那么点<var class = \"latex\">A</var>的对应点<var class = \"latex\">A<sub>1</sub></var>的坐标为（　　）",
            "way": "根据点的平移坐标变化规律求解即可.\n",
            "step": "∵<var class = \"latex\">A( －2，6 )</var>先向右移<var class = \"latex\">4</var>个单位长度，再向下平移<var class = \"latex\">1</var>个单位长度<br/>∴<var class = \"latex\">A<sub>1</sub>( 2，5 )</var><br/>故选D",
            "notice": "",
            "A": "",
            "opts": [
                "<var class = \"latex\">( 4，3 )</var>",
                "<var class = \"latex\">( 2，4 )</var>",
                "<var class = \"latex\">( 3，1 )</var>",
                "<var class = \"latex\">( 2，5 )</var>"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "在平面直角坐标系中，将线段OA向左平移2个单位长度，平移后，点<var class = \"latex\">O</var>，A的对应点分别为点<var class = \"latex\">O<sub>1</sub></var>，<var class = \"latex\">A<sub>1</sub></var>.\n若点<var class = \"latex\">O( 0，0 )</var>，<var class = \"latex\">A( 1，4 )</var>，则点<var class = \"latex\">O<sub>1</sub></var>，<var class = \"latex\">A<sub>1</sub></var>的坐标分别是（    ）",
            "way": "直接根据点的平移坐标变化规律求解即可.\n",
            "step": "向左平移2个单位即横坐标减2.\n<br/>∴<var class = \"latex\">O<sub>1</sub>( －2，0 )</var>，<var class = \"latex\">A<sub>1</sub>( －1，4 )</var><br/>故选D",
            "notice": "",
            "A": "",
            "opts": [
                "<var class = \"latex\">( 0，0 )</var>，<var class = \"latex\">( 1，4 )</var>",
                "<var class = \"latex\">( 0，0 )</var>，<var class = \"latex\">( 3，4 )</var>",
                "<var class = \"latex\">( －2，0 )</var>，<var class = \"latex\">( 1，4 )</var>",
                "<var class = \"latex\">( －2，0 )</var>，<var class = \"latex\">( －1，4 )</var>"
            ]
        },
        {
            "kind": "kpt",
            "name": "旋转的相关概念",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "旋转的定义",
                    "sub": [
                        "在平面内，将一个图形绕一个定点按某个方向转动一个角度，这样的图形运动称为旋转，这个定点称为旋转中心，转动的角称为旋转角.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "旋转的三要素",
                    "sub": [
                        "旋转中心、旋转角、旋转方向.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "旋转的对应",
                    "sub": [
                        "旋转得到的图形能够与原图形重合，我们把能够重合的点叫对应点，能够重合的线段叫对应线段，能够重合的角叫做对应角.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一  识别生活中的旋转现象",
            "type": 1,
            "Q": "下列现象属于旋转的是（  ）",
            "way": "根据旋转的定义：在平面内，把一个图形绕着某一个点<i>O</i>旋转一个角度的图形变换叫做旋转即可选出答案.\n",
            "step": "<i>A</i>、摩托车在急刹车时向前滑动是平移，故此选项错误；<br/><i>B</i>、飞机起飞后冲向空中的过程是平移，故此选项错误；<br/><i>C</i>、幸运大转盘转动的过程是旋转，故此选项正确；<br/><i>D</i>、笔直的铁轨上飞驰而过的火车是平移，故此选项错误；<br/>故选：C.\n",
            "notice": "",
            "A": "",
            "opts": [
                "摩托车在急刹车时向前滑动",
                "飞机起飞后冲向空中的过程",
                "幸运大转盘转动的过程",
                "笔直的铁轨上飞驰而过的火车"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列运动属于旋转的是（  ）",
            "way": "根据旋转变换的概念，对选项进行一一分析，排除错误答案.\n",
            "step": "<i>A</i>、滚动过程中的篮球属于滚动，不是绕着某一个固定的点转动，不属旋转；<br/><i>B</i>、钟表的钟摆的摆动，符合旋转变换的定义，属于旋转；<br/><i>C</i>、气球升空的运动是平移，不属于旋转；<br/><i>D</i>、一个图形沿某直线对折的过程是轴对称，不属于旋转.\n<br/>故选B.\n",
            "notice": "",
            "A": "",
            "opts": [
                "滚动过程中的篮球的滚动",
                "钟表的钟摆的摆动",
                "气球升空的运动",
                "一个图形沿某直线对折的过程"
            ]
        },
        {
            "kind": "example",
            "label": "题型二  旋转要素的识别",
            "type": 9,
            "Q": "<img style = \"width:10.00em;vertical-align:middle;float:right;\" src = \"20.png\">如图所示，△<var class = \"latex\">ABC</var>为等边三角形，△<var class = \"latex\">AP'B</var>逆时针旋转后能与△<var class = \"latex\">APC</var>重合.\n<br/>(1)旋转中心是哪一点？<br/>(2)旋转角是多少度？<br/>(3)<var class = \"latex\">∠PAP'</var>等于多少度？",
            "way": "观察图形在旋转的过程中各部分的位置变化情况，发现点<var class = \"latex\">A</var>的位置没有发生变化，因此点<var class = \"latex\">A</var>为旋转中心.\n点<var class = \"latex\">P</var>与<var class = \"latex\">P'</var>，点<var class = \"latex\">C</var>与<var class = \"latex\">B</var>分别为对应点，因此<var class = \"latex\">∠PAP'</var>和<var class = \"latex\">∠CAB</var>都是旋转角.\n",
            "step": "（1）旋转中心是点<var class = \"latex\">A</var>；（2）旋转角是<var class = \"latex\">60°</var>；（3）<var class = \"latex\">∠PAP' = ∠CAB = 60°</var>.\n",
            "notice": "首先确定旋转中心：旋转前后位置没有发生变化的点；然后确定对应点：旋转前后能够重合的点；最后确定旋转角：对应点与旋转中心所连线段的夹角.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "<img style = \"width:8.38em;vertical-align:middle;float:right;\" src = \"22.png\"><b >  </b>如图所示，可以看到，△<var class = \"latex\">AOB</var>经旋转后到达△<var class = \"latex\">A'OB'</var>，可知点<var class = \"latex\">A</var>旋转到点<var class = \"latex\">A'</var>，<var class = \"latex\">OA</var>旋转到<var class = \"latex\">OA'</var>，<var class = \"latex\">∠AOB</var>旋转到<var class = \"latex\">∠A'OB'</var>，则点<var class = \"latex\">B</var>的对应点是________点，线段<var class = \"latex\">OB</var>的对应线段是线段________，线段<var class = \"latex\">AB</var>的对应线段是线段________，<var class = \"latex\">∠A</var>的对应角是________，<var class = \"latex\">∠B</var>的对应角是________，旋转中心是点________，旋转角是________.\n",
            "way": "根据旋转的定义、旋转中心、旋转角、旋转的对应点等的概念来解题.\n",
            "step": "<var class = \"latex\">B'</var>；<var class = \"latex\">OB'</var>；<var class = \"latex\">A'B'</var>；<var class = \"latex\">∠A'</var>；<var class = \"latex\">∠B'</var>；<var class = \"latex\">O</var>；<var class = \"latex\">∠AOA'</var>或<var class = \"latex\">∠BOB'</var>.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "旋转的性质",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "旋转的性质",
                    "sub": [
                        "一个图形和它经过旋转所得的图形中，对应点到旋转中心的距离相等.\n任意一组对应点与旋转中心的连线所成的角都等于旋转角；对应线段相等，对应角相等.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一  根据旋转的性质求线段长",
            "type": 9,
            "Q": "<img style = \"width:10.63em;vertical-align:middle;float:right;\" src = \"24.png\">如图，将△<var class = \"latex\">ABC</var>绕点<var class = \"latex\">A</var>顺时针旋转<var class = \"latex\">60°</var>得到△<var class = \"latex\">AED</var>，若线段<var class = \"latex\">AB = 3</var>，则<var class = \"latex\">BE = </var>________ .\n",
            "way": "根据旋转的性质找到旋转角及对应关系，<br/>即可求出答案.\n",
            "step": "∵将△<var class = \"latex\">ABC</var>绕点<var class = \"latex\">A</var>顺时针旋转<var class = \"latex\">60°</var>得到△<var class = \"latex\">AED</var>，<br/>∴<var class = \"latex\">∠BAE = 60°</var>，<var class = \"latex\">AB = AE</var>，<br/>∴△<var class = \"latex\">BAE</var>是等边三角形，<br/>∴<var class = \"latex\">BE = 3</var>",
            "notice": "根据旋转的特征，若旋转角为60°，则一组对应边与旋转角组成的三角形是等边三角形.\n然后利用等边三角形的特殊性求线段的长度.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "<img style = \"width:10.56em;vertical-align:middle;float:right;\" src = \"25.png\">如图，<var class = \"latex\">Rt</var>△<var class = \"latex\">ABC</var>的斜边<var class = \"latex\">AB = 16</var>，<var class = \"latex\">Rt</var>△<var class = \"latex\">ABC</var>绕点<var class = \"latex\">O</var>顺时针旋转后得到<var class = \"latex\">Rt</var>△<var class = \"latex\">A'B'C'</var>，则<var class = \"latex\">Rt</var>△<var class = \"latex\">A'B'C'</var>的斜边<var class = \"latex\">A'B'</var>上的中线<var class = \"latex\">C'D</var>的长度为________.\n",
            "way": "根据旋转是全等变换，对应三角形是全等三角形求解即可.\n",
            "step": "∵旋转是全等变换<br/>∴<var class = \"latex\">Rt</var>△<var class = \"latex\">ABC</var>≌<var class = \"latex\">Rt</var>△<var class = \"latex\">A'B'C'</var><br/>∴<var class = \"latex\">AB = A'B'</var><br/>∴<span class = \"mathquill-rendered-math\">{C}'D = \\frac{1}{2}{A}'{B}' = \\frac{1}{2}AB = 8</span>",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型二  根据旋转的性质求角度",
            "type": 1,
            "Q": "<img style = \"width:8.75em;vertical-align:middle;float:right;\" src = \"27.png\">如图，△<var class = \"latex\">OAB</var>绕点<var class = \"latex\">O</var>逆时针旋转<var class = \"latex\">85°</var>得到△<var class = \"latex\">OCD</var>，若<var class = \"latex\">∠A = 110°</var>，<var class = \"latex\">∠D = 40°</var>，则<var class = \"latex\">∠α</var>的度数是（  ）",
            "way": "根据旋转的性质即可求出答案.\n",
            "step": "由题意可知：<var class = \"latex\">∠DOB = 85°</var>，<br/>∵△<var class = \"latex\">DCO</var>≌△<var class = \"latex\">BAO</var>，<br/>∴<var class = \"latex\">∠B = ∠D = 40°</var><br/>∴<var class = \"latex\">∠AOB = 180－40° －110° = 30°</var><br/>∴<var class = \"latex\">∠α = 85° －30° = 55°</var><br/>故选C",
            "notice": "",
            "A": "",
            "opts": [
                "<var class = \"latex\">35°</var>",
                "<var class = \"latex\">45°</var>",
                "<var class = \"latex\">55°</var>",
                "<var class = \"latex\">65°</var>"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "<img style = \"width:9.38em;vertical-align:middle;float:right;\" src = \"29.png\">（中考真题）如图，将<var class = \"latex\">Rt</var>△<var class = \"latex\">ABC</var>绕直角顶点<var class = \"latex\">C</var>顺时针旋转<var class = \"latex\">90°</var>，得到△<var class = \"latex\">A'B'C</var>，连接<var class = \"latex\">AA'</var>，若<var class = \"latex\">∠1 = 25°</var>，则<var class = \"latex\">∠BAA'</var>的度数是（  ）",
            "way": "根据旋转的性质可得<var class = \"latex\">AC = A'C</var>，然后判断出△<var class = \"latex\">ACA'</var>是等腰直角三角形，根据等腰直角三角形的性质可得<var class = \"latex\">∠CAA' = 45°</var>，再根据三角形的内角和定理可得结果.\n",
            "step": "∵<var class = \"latex\">Rt</var>△<var class = \"latex\">ABC</var>绕直角顶点<var class = \"latex\">C</var>顺时针旋转<var class = \"latex\">90°</var>得到△<var class = \"latex\">A'B'C</var>，<br/>∴<var class = \"latex\">AC = A'C</var>，<br/>∴△<var class = \"latex\">ACA'</var>是等腰直角三角形，<br/>∴<var class = \"latex\">∠CA'A = 45°</var>，<var class = \"latex\">∠CA'B' = 45°\n－∠1 = 20° = ∠BAC</var><br/>∴<var class = \"latex\">∠BAA' = ∠BAC + ∠CAA' = 20° + 45° = 65°</var><br/>故选：C.\n",
            "notice": "",
            "A": "",
            "opts": [
                "<var class = \"latex\">55°</var>",
                "<var class = \"latex\">60°</var>",
                "<var class = \"latex\">65°</var>",
                "<var class = \"latex\">70°</var>"
            ]
        },
        {
            "kind": "kpt",
            "name": "利用旋转作图",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "作一个图形的旋转图形的依据是旋转的性质",
                    "sub": [
                        "对应点到旋转中心的距离相等，每组对应点都旋转相同的角度.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "简单旋转作图的一般步骤",
                    "sub": [
                        "（1）找出图形的关键点；（2）确定旋转中心、旋转方向和旋转角；（3）将关键点与旋转中心连接起来，然后按旋转方向分别将它们旋转一个角度，得到关键点的对应点；（4）按原图形的顺序连接这些对应点，得到的图形就是旋转后的图形.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 旋转的基本作图",
            "type": 9,
            "Q": "<img style = \"width:11.94em;vertical-align:middle;float:right;\" src = \"31.png\">如图，△<var class = \"latex\">ABC</var>绕点<var class = \"latex\">O</var>旋转，使点<var class = \"latex\">A</var>旋转到点<var class = \"latex\">D</var>处，画出顺时针旋转后的三角形，并写出简要作法.\n",
            "way": "抓住“关键点”<var class = \"latex\">A</var>，<var class = \"latex\">B</var>，<var class = \"latex\">C</var>，<var class = \"latex\">D</var>，旋转中心<var class = \"latex\">O</var>，旋转角<var class = \"latex\">∠AOD</var>这些要素，按步骤作图即可.\n",
            "step": "<img style = \"width:10.63em;vertical-align:middle;float:right;\" src = \"33.png\">如图：（1）连接<var class = \"latex\">OA</var>，<var class = \"latex\">OB</var>，<var class = \"latex\">OC</var>，<var class = \"latex\">OD</var>；（2）分别以<var class = \"latex\">OB</var>，<var class = \"latex\">OC</var>为边作<var class = \"latex\">∠BOM = ∠CON = ∠AOD</var>；（3）分别在<var class = \"latex\">OM</var>，<var class = \"latex\">ON</var>上截取<var class = \"latex\">OE = OB</var>，<var class = \"latex\">OF = OC</var>；（4）依次连接<var class = \"latex\">DE</var>，<var class = \"latex\">EF</var>，<var class = \"latex\">FD</var>；则△<var class = \"latex\">DEF</var>就是所求作的三角形.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型二 确定旋转中心",
            "type": 1,
            "Q": "<img style = \"width:9.31em;vertical-align:middle;float:right;\" src = \"35.png\">如图，在方格纸上，△<i>DEF</i>是由△<i>DEF</i>绕定点<i>P</i>顺时针旋转得到的，如果用（2，1）表示方格纸上点<i>A</i>的位置，（1，2）表示点<i>B</i>的位置，那么点<i>P</i>的位置为（　　）",
            "way": "<img style = \"width:9.19em;vertical-align:middle;float:right;\" src = \"37.png\">如图连接<var class = \"latex\">AD</var>，<var class = \"latex\">CF</var>，然后作它们的垂直平分线，相交于点P，则旋转中心为点P，易得P点的坐标为<var class = \"latex\">( 5，2 )</var>.\n",
            "step": "故选A",
            "notice": "",
            "A": "",
            "opts": [
                "<var class = \"latex\">( 5，2 )</var>",
                "<var class = \"latex\">( 2，5 )</var>",
                "<var class = \"latex\">( 2，1 )</var>",
                "<var class = \"latex\">( 1，2 )</var>"
            ]
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "<img style = \"width:10.13em;vertical-align:middle;float:right;\" src = \"39.png\">如图，已知<var class = \"latex\">BC</var>与<var class = \"latex\">CD</var>重合，<var class = \"latex\">∠ABC = ∠CDE = 90°</var>，△<var class = \"latex\">ABC≌</var>△<var class = \"latex\">CDE</var>，并且△<var class = \"latex\">CDE</var>可由△<var class = \"latex\">ABC</var>逆时针旋转得到.\n请用尺规作图作出旋转中心<var class = \"latex\">O</var>，并直接写出旋转角度是________.\n",
            "way": "连接两组对应点，作它们的垂直平分线即可找到旋转中心.\n",
            "step": "如图，用尺规作图分别作<var class = \"latex\">AC</var>，<var class = \"latex\">CE</var>的垂直平分线，交点即为点<var class = \"latex\">O</var>.\n易得旋转角为：<var class = \"latex\">90°</var>.\n<br/><img style = \"width:12.63em;vertical-align:middle;float:right;\" src = \"41.png\">",
            "notice": "",
            "A": ""
        }
    ]
}