{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第一讲　圆的概念及垂径定理",
            "description": "1.\n了解圆及弦、弧（劣弧、优弧）、弦心距同心圆、等圆、等弧等概念.\n<br/>2.\n通过观察实验，理解圆的对称性，知道圆既是轴对称图形又是中心对称图形.\n<br/>3.\n掌握垂径定理及其推论，理解垂径定理的推导过程，能用垂径定理及其推论解决实际问题.\n"
        },
        {
            "kind": "kpt",
            "name": "圆的定义",
            "sub": [
                "<img src=\"1.png\" style=\"width:4.81em;vertical-align:middle;float:right;padding:5px;\">如图，在一个平面内，线段<i>OA</i>绕它固定的一个端点<i>O</i>旋转一周，另一个端点<i>A</i>随之旋转所形成的图形叫做圆，固定的端点<i>O</i>叫做圆心，线段<i>OA</i>叫做半径.\n以点<i>O</i>为圆心的圆，记作“⊙<i>O</i>”，读作“圆<i>O</i>”.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　圆的定义",
            "type": 1,
            "Q": "下列说法错误的有(　　)<br>①经过点<i>P</i>的圆有无数个；②以点<i>P</i>为圆心的圆有无数个；③半径为3<i>cm</i>且经过点<i>P</i>的圆有无数个；④以点<i>P</i>为圆心，3<i>cm</i>为半径的圆有无数个.\n",
            "opts": [
                "1个",
                "2个",
                "3个",
                "4个"
            ],
            "way": "",
            "step": "因为经过不在同一条直线上的三个点确定一个圆，<br>①经过一个点<i>P</i>的圆有无数个，正确；<br>②以点<i>P</i>为圆心的圆，半径不确定，所以有无数个，正确；<br>③半径为3<i>cm</i>且经过点<i>P</i>的圆，圆心不确定，所以有无数个，正确；<br>④以点<i>P</i>为圆心，以3<i>cm</i>为半径的圆，圆心半径都确定，所以只有1个圆，错误.\n<br>故选：A.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "以已知点<i>O</i>为圆心，已知线段<i>a</i>为半径作圆，可以作(　　)",
            "opts": [
                "1个",
                "2个",
                "3个",
                "无数个"
            ],
            "way": "根据圆心确定圆的位置，半径确定圆的大小，则可以作一个圆.\n",
            "step": "到定点距离等于定长的点只有一个，即以定点为圆心，定长为半径的圆.\n<br/>故选A.\n",
            "notice": "确定圆的两要素：圆心和半径.\n",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "与圆有关的概念",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "弦：",
                    "sub": [
                        "连结圆上任意两点的线段叫做弦.\n<br><b>直径：</b>经过圆心的弦叫做直径.\n<br><b>注：</b>直径是圆中最特殊的弦，也是最长的弦.\n<br><b>弦心距：</b>圆心到弦的距离叫做弦心距.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "弧：",
                    "sub": [
                        "圆上任意两点间的部分叫做圆弧，简称弧.\n<br>以<i>A、B</i>为端点的弧记作<img src=\"2.svg\" style=\"vertical-align:top;width:1.5em;\">，读作“圆弧<i>AB</i>”或“弧<i>AB</i>”.\n<br>半圆：圆的任意一条直径的两个端点把圆分成两条弧，每一条弧都叫做半圆；<br>优弧：大于半圆的弧叫做优弧；<br>劣弧：小于半圆的弧叫做劣弧.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "同心圆与等圆",
                    "sub": [
                        "同心圆：圆心相同，半径不等的两个圆叫做同心圆.\n<br>等圆：圆心不同，半径相等的两个圆.\n<br>同圆或等圆的半径相等.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "等弧",
                    "sub": [
                        "在同圆或等圆中，能够完全重合的弧叫做等弧.\n<br><b>注：</b><br>①等弧成立的前提条件是在同圆或等圆中，不能忽视；<br>②圆中两平行弦所夹的弧相等.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　圆的有关概念的题型",
            "type": 1,
            "Q": "以下命题：（1）半圆是弧，但弧不一定是半圆；（2）过圆上任意一点只能作一条弦，且这条弦是直径；（3）弦是直径；（4）直径是圆中最长的弦；（5）直径不是弦；（6）优弧大于劣弧；（7）以<i>O</i>为圆心可以画无数个圆.\n正确的个数为(　　)",
            "way": "",
            "step": "（1）半圆是弧的一种，但弧不一定是半圆，弧可以分为劣弧、半圆、优弧三种，正确；（2）过圆上任意一点可以作无数条弦，错误；（3）直径是过圆心的特殊弦，但弦不一定是直径，错误；（4）圆有无数条弦，过圆心的弦最长，即直径是圆中最长的弦，正确；（5）直径是过圆心的特殊弦，错误；（6）在同圆或等圆中，优弧大于劣弧，错误；（7）确定圆心，但不确定半径，可画出无数个大小不等的同心圆，正确.\n<br>故选C.\n",
            "notice": "",
            "A": "",
            "opts": [
                "1",
                "2",
                "3",
                "4"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "有以下结论：<br>①直径是弦；②弦是直径；③半圆是弧，但弧不一定是半圆；④半径相等的两个半圆是等弧；⑤长度相等的两条弧是等弧.\n其中错误的有(　　)",
            "way": "根据弦和直径的定义对①②进行判断；根据弧的定义对③进行判断；根据等弧的定义对④⑤进行判断.\n",
            "step": "直径是最长的弦，所以①正确；弦不一定是直径，所以②错误；半圆是弧，但弧不一定是半圆，所以③正确；半径相等的两个半圆是等弧，所以④正确；长度相等的两条弧不一定是等弧，所以⑤错误.\n<br>故选B.\n",
            "notice": "本题考查了圆的认识：掌握与圆有关的概念（弦、直径、半径、弧、半圆、优弧、劣弧、等圆、等弧等）.\n",
            "A": "",
            "opts": [
                "1个",
                "2个",
                "3个",
                "4个"
            ]
        },
        {
            "kind": "example",
            "label": "题型二　弦与弧的条数",
            "type": 1,
            "Q": "如图，点<i>A、O、D</i>与点<i>B、O、C</i>分别在同一直线上，图中弦的条数为(　　)<br/><img style=\"width:6.31em;vertical-align:middle;padding:5px;\" src=\"3.png\">",
            "way": "由图可知，点<i>A、B、E、C</i>是⊙<i>O</i>上的点，图中的弦有<i>AB、BC、CE</i>，一共3条.\n",
            "step": "答案：B.\n",
            "notice": "",
            "A": "",
            "opts": [
                "2",
                "3",
                "4",
                "5"
            ]
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图所示，圆中以<i>A</i>为一个端点的优弧有________条，分别是________；以<i>A</i>为一个端点的劣弧有________条，分别是________；以<i>A</i>为一个端点的半圆有________个.\n<br/><img style=\"width:9.06em;vertical-align:middle;padding:5px;\" src=\"4.png\">",
            "way": "根据直径、弦、优弧和劣弧的定义写出答案即可.\n",
            "step": "圆中有<i>AB</i>一条直径，<i>AB、CD、EF</i>三条弦，圆中以<i>A</i>为一个端点的优弧有四条，劣弧有四条，半圆有两个.\n<br>故答案为：4，<img style=\"vertical-align:top;width:2.2em;\" src=\"5.svg\">、<img style=\"vertical-align:top;width:2.2em;\" src=\"6.svg\">、<img style=\"vertical-align:top;width:2.2em;\" src=\"7.svg\">、<img style=\"vertical-align:top;width:2.2em;\" src=\"8.svg\">；4，<img style=\"vertical-align:top;width:1.5em;\" src=\"9.svg\">、<img style=\"vertical-align:top;width:1.5em;\" src=\"10.svg\">、<img style=\"vertical-align:top;width:1.5em;\" src=\"11.svg\">、<img style=\"vertical-align:top;width:1.5em;\" src=\"12.svg\">；2.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型三　弦与弧有关综合题",
            "type": 1,
            "Q": "如图，甲顺着大半圆从<i>A</i>地到<i>B</i>地，乙顺着两个小半圆从<i>A</i>地到<i>B</i>地，设甲、乙走过的路程分别为<i>a、b</i>，则(　　)<br/><img style=\"width:10.12em;vertical-align:middle;padding:5px;\" src=\"13.png\">",
            "way": "",
            "step": "设图中甲走过的大圆半径为<i>R</i>，乙走过的两个较小圆的半径分别为<i>r</i><sub>1</sub>、<i>r</i><sub>2</sub>，看图可以发现甲走过的半圆的直径等于乙走过的两个较小圆的直径之和，即2<i>R</i> = 2<i>r</i><sub>1</sub> + 2<i>r</i><sub>2</sub>，得<i>R</i> = <i>r</i><sub>1</sub> + <i>r</i><sub>2</sub>.\n甲走过的路程<i>a = πR</i>，乙走过的路程<i>B</i> = <i>πr</i><sub>1</sub> + <i>πr</i><sub>2</sub> = <i>π</i>(<i>r</i><sub>1</sub> + <i>r</i><sub>1</sub>)，所以<i>a = b</i>.\n故本题正确答案为A.\n",
            "notice": "",
            "A": "",
            "opts": [
                "<i>a = b</i>",
                "<i>a &lt; b</i>",
                "<i>a &gt; b</i>",
                "不能确定"
            ]
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，<i>OA</i>是⊙<i>O</i>的半径，<i>AB</i>是弦，∠<i>OAB</i> = 45°，<i>OA</i> = 8，则<i>AB</i> = ________.\n<br><img style=\"width:6.25em;vertical-align:middle;padding:5px;\" src=\"14.png\">",
            "way": "连结<i>OB</i>.\n则<i>OA = OB</i>，根据∠<i>OAB</i> = 45°，可得△<i>OAB</i>是等腰直角三角形，再根据等腰直角三角形的性质即可求解.\n",
            "step": "连结<i>OB</i>.\n<br><img style=\"width:6.25em;vertical-align:middle;padding:5px;\" src=\"15.png\"><br>∵∠<i>OAB</i> = 45°，<i>OA</i> = 8，<br>∴∠<i>OBA</i> = 45°，<i>OB</i> = 8，<br>∴△<i>OAB</i>是等腰直角三角形，<br>∴<i>AB</i> = <span class=\"mathquill-rendered-math\">8\\sqrt{2}</span>.\n<br>故答案为：<span class=\"mathquill-rendered-math\">8\\sqrt{2}</span>.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "圆的对称性",
            "sub": [
                "1.\n圆是轴对称图形，任何一条直径所在直线都是圆的对称轴.\n<br>2.\n圆是中心对称图形，圆心就是它的对称中心.\n不仅如此，把圆绕圆心旋转任意一个角度，所得的图形都与原图形重合，即圆还具有旋转不变性.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　判断对称轴的条数",
            "type": 1,
            "Q": "在下列轴对称图形中，对称轴条数最多的图形是(　　)",
            "way": "长方形有2条对称轴，即过对边中点的直线；正方形有4条对称轴，即过对边中点的连线和对角线所在的直线；等腰三角形有1条对称轴，即底上的高所在的直线；圆有无数条对称轴，即直径所在的直线.\n",
            "step": "长方形有2条对称轴，正方形有4条对称轴，等腰三角形有1条对称轴，圆有无数条对称轴.\n<br>故选：D.\n",
            "notice": "此题是考查轴对称图形的对称轴的条数及位置.\n根据对称轴的意义及各图形的特征即可判定.\n圆是对称轴最多的图形.\n",
            "A": "",
            "opts": [
                "长方形",
                "正方形",
                "等腰三角形",
                "圆"
            ]
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "圆是轴对称图形，任何一条________所在的直线都是圆的对称轴，所以圆有________条对称轴；半圆形有________条对称轴.\n",
            "way": "根据轴对称图形的特点，如果一个图形沿着一条直线对折，两侧的图形能够完全重合，这样的图形就是轴对称图形，可知：圆是轴对称图形，圆的任意一条直径所在的直线都是圆的对称轴，圆有无数条对称轴；半圆形有1条对称轴.\n由此解答.\n",
            "step": "圆是轴对称图形，任何一条直径所在的直线都是圆的对称轴，所以圆有无数条对称轴；半圆形有1条对称轴.\n<br>故答案为：直径，无数，1.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "垂径定理",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "垂径定理：",
                    "sub": [
                        "垂直于弦的直径平分弦，并且平分弦所对的两条弧，如图，<i>CD</i>⊥<i>AB</i>于点<i>E，CD</i>是⊙<i>O</i>的直径，那么可用几何语言表述为：<img src=\"16.png\" style=\"width:10.5em;vertical-align:middle;\">.\n<img src=\"18.png\" style=\"width:11em;vertical-align:middle;float:right;\">"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "垂径定理推论：",
                    "sub": [
                        "（1）平分弦（不是直径）的直径垂直于弦，并且平分弦所对的两条弧，如图，<i>CD</i>是⊙<i>O</i>的直径，<i>AB</i>是弦（非直径），<i>AB</i>与<i>CD</i>相交于点<i>E</i>，且<i>AE = BE</i>，那么<i>CD</i>垂直于<i>AB</i>，并且<img src=\"26.svg\" style=\"vertical-align:top;width:4em;\">，<img src=\"27.svg\" style=\"vertical-align:top;width:4em;\">.\n可用几何语言表述为：<img src=\"17.png\" style=\"width:11.5em;vertical-align:middle;\">.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　垂径定理及其推论的理解",
            "type": 9,
            "Q": "垂径定理及其推论：<br>定理：垂直于弦的直径________________________________________________________.\n<br>推论：<br>（1）平分弦（不是直径）的直径________________________________________________.\n<br>（2）弦的垂直垂直平分线经过圆心，并且________________________________________.\n<br>（3）平分弦所对的一条弧的直径，垂直平分弦，并且______________________________.\n",
            "way": "根据垂径定理的内容和垂径定理的推论的内容可以直接得出结论.\n",
            "step": "定理：垂直于弦的直径平分弦，并且平分弦所对的两条弧.\n<br>推论：（1）平分弦（不是直径）的直径 垂直于弦，并且平分弦所对的两条弧.\n<br>（2）弦的垂直垂直平分线经过圆心，并且平分弦所对的两条弧.\n<br>（3）平分弦所对的一条弧的直径，垂直平分弦，并且平分弦所对的另一条弧.\n<br>故答案为：平分弦，并且平分弦所对的两条弧，垂直于弦，并且平分弦所对的两条弧，平分弦所对的两条弧，平分弦所对的另一条弧.\n",
            "notice": "本题考查了垂径定理的内容的运用，垂径定理的推论的运用，解答时熟悉垂径定理的内容和推论的内容是关键.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列判断中不正确的是(　　)",
            "way": "根据垂径定理对各选项进行逐一分析即可.\n",
            "step": "A、平分弦（不是直径）的直径垂直于弦，故本选项错误；<br>B、符合垂径定理，故本选项正确；<br>C、符合垂径定理，故本选项正确；<br>D、符合垂径定理，故本选项正确.\n<br>故选A.\n",
            "notice": "本题考查的是垂径定理，熟知平分弦的直径平分这条弦，并且平分弦所对的两条弧是解答此题的关键.\n",
            "A": "",
            "opts": [
                "平分弦的直径垂直于弦",
                "垂直于弦的直径也平分弦",
                "弦的垂直平分线必平分弦所对的两条弧",
                "平分一条弧的直径必平分这条弧所对的弦"
            ]
        },
        {
            "kind": "example",
            "label": "题型二　运用垂径定理证明相等的量",
            "type": 1,
            "Q": "如图所示，<i>AB</i>是⊙<i>O</i>的直径，<i>CD</i>为弦，<i>CD</i>⊥<i>AB</i>于点<i>E</i>，则下列结论中不一定成立的是(　　)<br><img src=\"21.png\" style=\"width:7em;vertical-align:middle;padding:5px;\">",
            "way": "本题考查了垂径定理和圆心角、弧之间的关系.\n",
            "step": "由垂径定理可知<i>B、D</i>均成立；由圆心角、弧之间的关系可得<i>A</i>也成立.\n不一定成立的是<i>OE = BE</i>.\n<br>故答案为：C.\n",
            "notice": "根据垂径定理及圆心角、弧之间的关系定理解答.\n",
            "A": "",
            "opts": [
                "∠<i>COE</i> = ∠<i>DOE</i>",
                "<i>CE = DE</i>",
                "<i>OE = BE</i>",
                "<img src=\"19.svg\" style=\"vertical-align:top;width:1.5em;\"> = <img src=\"20.svg\" style=\"vertical-align:top;width:1.5em;\">"
            ]
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "已知在以点<i>O</i>为圆心的两个同心圆中，大圆的弦<i>AB</i>交小圆于点<i>C，D</i>（如图）.\n<br><img src=\"22.png\" style=\"width:7.18em;vertical-align:middle;padding:5px;\"><br>（1）求证：<i>AC = BD</i>；<br>（2）若大圆的半径<i>R</i> = 10，小圆的半径<i>r</i> = 8，且圆<i>O</i>到直线<i>AB</i>的距离为6，求<i>AC</i>的长.\n",
            "way": "（1）过<i>O</i>作<i>OE</i>⊥<i>AB</i>，根据垂径定理得到<i>AE = BE</i>，<i>CE = DE</i>，从而得到<i>AC = BD</i>；<br>（2）由（1）可以知道，<i>OE</i>⊥<i>AB</i>且<i>OE</i>⊥<i>CD</i>，连接<i>OC，OA</i>，再根据勾股定理求出<i>CE</i>及<i>AE</i>的长，根据<i>AC = AE－CE</i>即可得出结论.\n",
            "step": "（1）证明：过<i>O</i>作<i>OE</i>⊥<i>AB</i>于点<i>E</i>，<br><img src=\"23.png\" style=\"width:7.18em;vertical-align:middle;padding:5px;\"><br>则<i>CE = DE，AE = BE</i>，∴<i>BE－DE = AE－CE</i>，即<i>AC = BD</i>；<br>（2）解：由（1）可以知道，<i>OE</i>⊥<i>AB</i>且<i>OE</i>⊥<i>CD</i>，连接<i>OC，OA</i>，<br>∴<i>OE</i> = 6，<br>∴<span class=\"mathquill-rendered-math\">CE = \\sqrt{{OC}^{2}－{OE}^{2}} = \\sqrt{{8}^{2}－{6}^{2}} = 2\\sqrt{7}</span>，<span class=\"mathquill-rendered-math\">AE = \\sqrt{{OA}^{2}－{OE}^{2}} = \\sqrt{{10}^{2}－{6}^{2}} = 8</span>，<br>∴<i>AC = AE－CE</i> = <span class=\"mathquill-rendered-math\">8－2\\sqrt{7}</span>.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型三　利用垂径定理及推论的相关计算及应用",
            "type": 9,
            "Q": "如图，<i>AB</i>为半圆直径，<i>O</i>为圆心，<i>C</i>为半圆上一点，<i>E</i>是弧<i>AC</i>的中点，<i>OE</i>交弦<i>AC</i>于点<i>D</i>，若<i>AC</i> = 8<i>cm</i>，<i>DE</i> = 2<i>cm</i>，求<i>OD</i>的长.\n<br><img src=\"24.png\" style=\"width:8.31em;vertical-align:middle;padding:5px;\">",
            "way": "由<i>E</i>是弧<i>AC</i>的中点，由垂径定理的推论得：<span class=\"mathquill-rendered-math\">AD = \\frac{1}{2}AC</span>，又<i>OD = OE－DE</i>，故在<i>Rt</i>△<i>OAD</i>中，运用勾股定理可将<i>OE</i>的长求出，继而求得<i>OD</i>的长.\n",
            "step": "∵<i>E</i>为弧<i>AC</i>的中点，<br>∴<i>OE</i>⊥<i>AC</i>，<br>∴<span class=\"mathquill-rendered-math\">AD = \\frac{1}{2}AC</span> = 4<i>cm</i>，<br>∵<i>OD = OE－DE</i> = (<i>OE</i>－2)<i>cm</i>，<i>OA = OE</i>，<br>∴在<i>Rt</i>△<i>OAD</i>中，<i>OA<sup>2</sup> = OD<sup>2</sup> + AD<sup>2</sup></i>即<i>OA<sup>2</sup> = (OE－2)<sup>2</sup> + 4<sup>2</sup></i>，又知<i>OA = OE</i>，<br>解得：<i>OE</i> = 5，<br>∴<i>OD = OE－DE</i> = 3<i>cm</i>.\n",
            "notice": "主要是由半径、弦的一半和弦心距（圆心到弦的垂线段的长度）构成的直角三角形.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图1，某公园的一座石拱桥是圆弧形（劣弧），其跨度为24<i>m</i>，拱的半径为13<i>m</i>，则拱高为(　　)<br><img src=\"25.png\" style=\"width:23.18em;vertical-align:middle;padding:5px;\">",
            "way": "解决此题的关键是将这样的实际问题转化为数学问题，即能够把题目中的已知条件和要求的问题转化为数学问题中的已知条件和问题.\n",
            "step": "如图2，<img src=\"2.svg\" style=\"vertical-align:top;width:1.5em;\">表示桥拱，弦<i>AB</i>的长表示桥的跨度，<i>C</i>为<img src=\"2.svg\" style=\"vertical-align:top;width:1.5em;\">的中点，<i>CD</i>⊥<i>AB</i>于<i>D，CD</i>表示拱高，<i>O</i>为<img src=\"2.svg\" style=\"vertical-align:top;width:1.5em;\">的圆心，根据垂径定理的推论可知，<br><i>C、D、O</i>三点共线，且<i>OC</i>平分<i>AB</i>.\n<br>在<i>Rt</i>△<i>AOD</i>中，<i>OA</i> = 13，<i>AD</i> = 12，则<i>OD</i><sup>2</sup> = <i>OA</i><sup>2</sup>－<i>AD</i><sup>2</sup> = 13<sup>2</sup>－12<sup>2</sup> = 25.\n<br>∴<i>OD</i> = 5，<br>∴<i>CD = OC－OD</i> = 13－5 = 8，即拱高为8<i>m</i>.\n",
            "notice": "在解答有关弓形问题时，首先应找弓形的弧所在圆的圆心，然后构造直角三角形，运用垂径定理（推论）及勾股定理求解.\n",
            "A": ""
        }
    ]
}