{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第一讲：二次根式的概念和性质",
            "description": "1.\n理解二次根式的定义和性质；<br>2.\n理解并掌握积的算术平方根和商的算术平方根；<br>3.\n理解同类二次根式概念，掌握最简二次根式的化简方法.\n"
        },
        {
            "kind": "kpt",
            "name": "二次根式（重点）",
            "sub": [
                      {
                          "kind": "kpt",
                          "name": "二次根式：",
                          "sub": ["一般地，我们把形如<span class=\"mathquill-rendered-math\">\\sqrt{a}\\left(a\\ge 0 \\right)</span>的式子叫做二次根式，“<img src=\"1.png\" style=\"width:1.35em;vertical-align:middle;\">”称为二次根号，<i>a</i> 叫做被开方数.\n<br><b>注：</b>二次根式的两个要素：①根指数为2；②被开方数为非负数.\n<br>（1）二次根式的定义是从式子的结构形式上界定的，必须含有二次根号“<img src=\"1.png\" style=\"width:1.35em;vertical-align:middle;\">”，“<img src=\"1.png\" style=\"width:1.35em;vertical-align:middle;\">”的根指数为2，即 <img src=\"2.png\" style=\"width:1.5em;vertical-align:middle;\">，“ 2 ”一般省略不写.\n<br>（2）被开方数 <i>a</i> 可以是一个数，也可以是一个含有字母的式子，但前提是 <i>a</i> 必须大于或等于0.\n"
                           ]
                     },
                     {
                         "kind": "kpt",
                         "name": "易错警示：",
                         "sub": ["（1）二次根式是从形式上定义的，不能从化简结果上判断.\n如<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}}}</span>，<span class=\"mathquill-rendered-math\">\\sqrt{0}</span>，<span class=\"mathquill-rendered-math\">\\sqrt{4}</span>等都是二次根式.\n<br>（2）像<span class=\"mathquill-rendered-math\">\\sqrt{a} + 1\\left(a\\ge 0 \\right)</span>这样的式子只能称为含有二次根式的式子，不能称为二次根式.\n"
                          ]
                    }
            ]
        },
        {
            "kind": "video",
            "title": "二次根式的概念",
            "name": "二次根式的概念",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18680.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　二次根式的识别",
            "type": 9,
            "Q": "判断下列各式是否为二次根式.\n<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{12}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt[3]{－5}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{－4}</span>；（4）<span class=\"mathquill-rendered-math\">\\sqrt{{m}^{2} + 1}</span>；（5）<span class=\"mathquill-rendered-math\">\\sqrt{－{(a－4)}^{2}}</span>",
            "step": "（1）∵ 12 &gt; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{12}</span>符合二次根式的定义；故是二次根式；<br>（2）∵ 根指数为3，∴<span class=\"mathquill-rendered-math\">\\sqrt[3]{－5}</span>不符合二次根式的定义；故不是二次根式；<br>（3）∵ －4 &gt; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{－4}</span>故不是二次根式；<br>（4）∵ <i>m</i><sup>2</sup> &ge; 0，∴ <i>m</i><sup>2</sup> + 1 &gt; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{{m}^{2} + 1}</span>是二次根式；<br>（5）当 <i>a</i> = 4 时，<i>a</i>－4 = 0，<span class=\"mathquill-rendered-math\">\\sqrt{－{(a－4)}^{2}}</span>是二次根式；<br>当 <i>a</i> ≠ 4 时，－(<i>a</i>－4)<sup>2</sup> &lt; 0，<span class=\"mathquill-rendered-math\">\\sqrt{－{(a－4)}^{2}}</span>不是二次根式；<br>∴<span class=\"mathquill-rendered-math\">\\sqrt{－{(a－4)}^{2}}</span>不一定是二次根式.\n",
            "notice": "判断一个式子是不是二次根式，一定要紧扣定义，看所给式子是否同时具备二次根式的两个特征：（1）带二次根号“<img src=\"1.png\" style=\"width:1.35em;vertical-align:middle;\">”；（2）被开方数不小于0.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "判断下列各式是不是二次根式？并说明理由.\n<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt[3]{8}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{3} + 2</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{{a}^{2}}</span>；（4）<span class=\"mathquill-rendered-math\">\\sqrt{－{n}^{2}}</span>；（5）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}}</span>",
            "step": "（1）∵ 根指数为3，∴<span class=\"mathquill-rendered-math\">\\sqrt[3]{8}</span>不符合二次根式的定义；故不是二次根式；<br>（2）∵ 3 &gt; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{3} + 2</span>虽含有二次根式的式子，∴ 不符合二次根式的定义，故不是二次根式.\n<br>（3）∵ 因为 <i>a</i><sup>2</sup> &ge; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{{a}^{2}}</span>是二次根式；<br>（4）∵ <i>n</i></i><sup>2</sup> &ge; 0，∴ －<i>n</i></i><sup>2</sup> &le; 0，∴ 当 <i>n</i> = 0 时，<span class=\"mathquill-rendered-math\">\\sqrt{－{n}^{2}}</span>才是二次根式；<br>（5）当 <i>x</i> = －3 时，<span class=\"mathquill-rendered-math\">\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}</span>无意义，所以<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}}</span>也无意义，<br>当 <i>x</i> ≠ 3 时，<span class=\"mathquill-rendered-math\">\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}</span>&gt; 0，所以<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}}</span>是二次根式，<br>所以<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{{{\\left(x + 3 \\right)}^{2}}}}</span>不一定是二次根式.\n",
            "notice": "我们可以根据二次根式的定义和性质进行判断：式子<span class=\"mathquill-rendered-math\">\\sqrt{a}</span> (<i>a</i> &ge; 0) 叫二次根式，<span class=\"mathquill-rendered-math\">\\sqrt{a}</span> (<i>a</i> &ge; 0) 是一个非负数.\n本题考查了二次根式的定义.\n一般形如<span class=\"mathquill-rendered-math\">\\sqrt{a}</span> (<i>a</i> &ge; 0) 的代数式叫做二次根式.\n当 <i>a</i> &ge; 0 时，表示 <i>a</i> 的算术平方根；当 <i>a</i> 小于0时，不是二次根式.\n"
        },
        {
            "kind": "video",
            "title": "二次根式的非负性",
            "name": "二次根式的非负性",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18681.mp4"
        },
        {
            "kind": "example",
            "label": "题型二　求式子有意义时字母的取值范围",
            "type": 9,
            "Q": "当取怎样的数时，下列各式在实数范围内有意义？<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{2x－6} + {{\\left(x－5 \\right)}^{0}}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{3x + 7}}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{x－2}－\\sqrt{5－x}</span>；（4）<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{x + 4}}{x－2}</span>.\n",
            "way": "要使二次根式有意义，则被开方数是非负数，如果同时有分式，那么分式中的分母不能为零.\n",
            "step": "（1）欲使<span class=\"mathquill-rendered-math\">\\sqrt{2x－6} + {{\\left(x－5 \\right)}^{0}}</span>有意义，则必有 2<i>x</i>－6 &ge; 0 且 <i>x</i>－5 ≠ 0，所以 <i>x</i> &ge; 3 且 <i>x</i> ≠ 5.\n<br>（2）欲使<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{3x + 7}}</span>有意义，则必有<span class=\"mathquill-rendered-math\">\\frac{1}{3x + 7}\\ge 0</span>且 3<i>x</i> + 7 ≠ 0，所以 <i>x</i> &gt;<span class=\"mathquill-rendered-math\">－\\frac{7}{3}</span>.\n<br>（3）欲使<span class=\"mathquill-rendered-math\">\\sqrt{x－2}－\\sqrt{5－x}</span>有意义，则必有 <i>x</i>－2 &ge; 0 且 5－<i>x</i> &ge; 0，所以 2 &le; <i>x</i> &le; 5.\n<br>（4）欲使<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{x + 4}}{x－2}</span>有意义，则必有 <i>x</i> + 4 &ge; 0 且 <i>x</i>－2 ≠ 0，所以 <i>x</i> &ge; －4 且 <i>x</i> ≠ 2.\n",
            "notice": "求式子有意义时字母的取值范围的方法：<br>第一步，明确式子有意义的条件，对于单个的二次根式，只需满足被开方数为非负数；对于含有多个二次根式的，则必须满足多个被开方数同时为非负数；对于零指数幂，则必须满足底数不能为零；对于含有分式的，则需满足分母不能为零，第二步，利用式子中所有有意义的条件，建立不等关系，第三步，求出字母的取值范围.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "若代数式<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{x + 1}}{{{\\left(x－3 \\right)}^{2}}}</span>有意义，则实数 <i>x</i> 的取值范围是（　　）",
            "opts": [
                "<i>x</i> &ge; －1",
                "<i>x</i> &ge; －1 且 <i>x</i> ≠ 3",
                "<i>x</i> &gt; －1",
                "<i>x</i> &gt; －1 且 <i>x</i> ≠ 3"
            ],
            "way": "本题主要考查分式的概念和二次根式的概念.\n",
            "step": "根据分式的概念可知使分式有意义的条件为(<i>x</i>－3)<sup>2</sup>  ≠ 0，即 <i>x</i> ≠ 3，根据二次根式的概念可知，使二次根式有意义的条件为 <i>x</i> + 1 &ge; 0，即 <i>x</i> &ge; －1，故实数 <i>x</i> 的取值范围为 <i>x</i> &ge; －1 且 <i>x</i> ≠ 3.\n<br>故本题正确答案为B.\n"
        },
        {
            "kind": "example",
            "type": 9,
            "Q": "若等式<span class=\"mathquill-rendered-math\">\\sqrt{x + 2}\\cdot \\sqrt{x－2}</span><span class=\"mathquill-rendered-math\"> = \\sqrt{{{x}^{2}}－4}</span>成立，则 <i>x</i> 的取值范围是________.\n",
            "way": "利用负数没有平方根确定出 <i>x</i> 的范围即可.\n",
            "step": "根据题意得：<img src=\"3.svg\" style=\"vertical-align:middle;width:4.5em\">，<br>解得：<i>x</i> &ge; 2，<br>则 <i>x</i> 的取值范围为 <i>x</i> &ge; 2，<br>故答案为：<i>x</i> &ge; 2.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "根据下列条件，求字母 <i>x</i> 的取值范围：<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{2}}－2x + 1} = 1－x</span>； （2）<span class=\"mathquill-rendered-math\">\\sqrt{{{\\left(x－2 \\right)}^{2}}} + \\sqrt{{{\\left(x－3 \\right)}^{2}}} = 1</span>.\n",
            "way": "将已知等式适当变形，求解即可.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{2}}－2x + 1}</span><span class=\"mathquill-rendered-math\"> = \\sqrt{{{\\left(x－1 \\right)}^{2}}}</span><span class=\"mathquill-rendered-math\"> = |x－1|</span><span class=\"mathquill-rendered-math\"> = 1－x</span>，∴ <i>x</i>－1 &le; 0，∴ <i>x</i> &le; 1；<br>（2）<span class=\"mathquill-rendered-math\">\\sqrt{{{\\left(x－2 \\right)}^{2}}} + \\sqrt{{{\\left(x－3 \\right)}^{2}}}</span><span class=\"mathquill-rendered-math\"> = |x－2| + |x－3|</span><span class=\"mathquill-rendered-math\"> = 1</span>，∴<img src=\"4.svg\" style=\"vertical-align:middle;width:4.5em\">，∴ 2 &le; <i>x</i> &le; 3.\n"
        },
        {
            "kind": "kpt",
            "name": "二次根式的性质",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "积的算术平方根的性质：",
                         "sub": ["积的算术平方根等于乘积中各个因式的算术平方根的积.\n即<span class=\"mathquill-rendered-math\">\\sqrt{ab}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{a}\\cdot \\sqrt{b}</span> (<i>a</i> &ge; 0，<i>b</i> &ge; 0).\n"
                          ]
                     },
                     {
                         "kind": "kpt",
                         "name": "商的算术平方根的性质：",
                         "sub": ["商的算术平方根等于被除式的算术平方根除以除式的算术平方根.\n即<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{a}{b}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{a}}{\\sqrt{b}}</span> (<i>a</i> &ge; 0，<i>b</i> &gt; 0).\n"
                          ]
                     }
            ]
        },
        {
            "kind": "video",
            "title": "去根号法则",
            "name": "去根号法则",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18682.mp4"
        },
        {
            "kind": "video",
            "title": "二次根式的乘除法则",
            "name": "二次根式的乘除法则",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18683.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　积的算术平方根化简",
            "type": 9,
            "Q": "化简：（1）<span class=\"mathquill-rendered-math\">\\sqrt{125}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\left(－7 \\right) × (－14)}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{{{36}^{2}} + {{48}^{2}}}</span>；（4）<span class=\"mathquill-rendered-math\">\\sqrt{{{m}^{3}}{{n}^{5}}}</span> (<i>m</i> &ge; 0，<i>n</i> &ge; 0)；（5）<span class=\"mathquill-rendered-math\">\\sqrt{0.49{{x}^{5}}{{y}^{6}}}</span> (<i>x</i> &ge; 0，<i>y</i> &ge; 0).\n",
            "way": "应用积的算术平方根性质的前提是被开方数是乘积的形式，若不是，则需将它们转化为乘积的形式，其次是每个因数（式）必须是非负数，（1）（2）（3）中被开方数为数，（4）（5）中被开方数是含有字母的单项式，都可直接或经过变形后利用<span class=\"mathquill-rendered-math\">\\sqrt{ab} = \\sqrt{a}\\cdot \\sqrt{b}</span> (<i>a</i> &ge; 0，<i>b</i> &ge; 0) 和<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}}} = a</span> (<i>a</i> &gt; 0) 进行化简.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\sqrt{125}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{25 × 5}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{25}</span>×<span class=\"mathquill-rendered-math\">\\sqrt{5}</span>=<span class=\"mathquill-rendered-math\">5\\sqrt{5}</span>.\n<br>（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\left(－7 \\right) × (－14)}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{7 × 14}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{2 × {{7}^{2}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{2}</span>×<span class=\"mathquill-rendered-math\">\\sqrt{{{7}^{2}}}</span>=<span class=\"mathquill-rendered-math\">7\\sqrt{2}</span>.\n<br>（3）<span class=\"mathquill-rendered-math\">\\sqrt{{{36}^{2}} + {{48}^{2}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{\\left(12 × 3 \\right)}^{2}} + {{\\left(12 × 4 \\right)}^{2}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{12}^{2}} × \\left({{3}^{2}} + {{4}^{2}} \\right)}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{12}^{2}}}</span>×<span class=\"mathquill-rendered-math\">\\sqrt{25}</span>=<span class=\"mathquill-rendered-math\">12 × 5 = 60</span>.\n<br>（4）<span class=\"mathquill-rendered-math\">\\sqrt{{{m}^{3}}{{n}^{5}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{m}^{2}}{{n}^{4}}\\cdot mn}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{m}^{2}}}\\cdot \\sqrt{{{n}^{4}}}\\cdot \\sqrt{mn}</span>=<span class=\"mathquill-rendered-math\">m{{n}^{2}}\\sqrt{mn}</span>.\n<br>（5）<span class=\"mathquill-rendered-math\">\\sqrt{0.49{{x}^{5}}{{y}^{6}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{0.7}^{2}}{{\\left({{x}^{2}} \\right)}^{2}}{{\\left({{y}^{3}} \\right)}^{2}}\\cdot x}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{{{0.7}^{2}}{{\\left({{x}^{2}} \\right)}^{2}}{{\\left({{y}^{3}} \\right)}^{2}}}\\cdot \\sqrt{x}</span>=<span class=\"mathquill-rendered-math\">0.7{{x}^{2}}{{y}^{3}}\\sqrt{x}</span>.\n",
            "notice": "在利用积的算术平方根的性质进行化简时号注意以下两点：<br>（1）注意被开方数的范围，<br>（2）注意被开方数一定是乘积的形式不要出现“<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}} + {{b}^{2}}} = \\sqrt{{{a}^{2}}} + \\sqrt{{{b}^{2}}}</span>”这样的错误.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "（1）已知 <i>a</i> &gt; 0，化简二次根式<span class=\"mathquill-rendered-math\">\\sqrt{－{{a}^{3}}b}</span>的结果是________.\n<br>（2）已知<span class=\"mathquill-rendered-math\">y = \\sqrt{2x－1}</span>－<span class=\"mathquill-rendered-math\">\\sqrt{1－2x}</span>+<span class=\"mathquill-rendered-math\">8x</span>，求<span class=\"mathquill-rendered-math\">\\sqrt{4x + 5y－6}</span>的平方根.\n<br>（3）当 －4 &gt; <i>x</i> &gt; 1 时，化简<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{2}} + 8x + 16}</span><span class=\"mathquill-rendered-math\">－2\\sqrt{{{x}^{2}}－2x + 1}</span>.\n",
            "step": "（1）∵ <i>a</i> &gt; 0，∴<span class=\"mathquill-rendered-math\">\\sqrt{－{{a}^{3}}b}</span><span class=\"mathquill-rendered-math\"> = －a\\sqrt{－ab}</span>.\n故答案为：<span class=\"mathquill-rendered-math\">\\－a\\sqrt{－ab}</span>.\n<br>（2）∵<span class=\"mathquill-rendered-math\">y = \\sqrt{2x－1}</span><span class=\"mathquill-rendered-math\">－\\sqrt{1－2x}</span>+<span class=\"mathquill-rendered-math\">8x</span>，∴<img src=\"5.svg\" style=\"width:5.5em;vertical-align:middle;\">，∴ 2<i>x</i>－1 = 0，解得 <i>x</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>，∴ <i>y</i> = 4，<br>∴<span class=\"mathquill-rendered-math\">\\sqrt{4x + 5y－6}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{2 + 20－6}</span>= 4，4的平方根是 ±2.\n故<span class=\"mathquill-rendered-math\">\\sqrt{4x + 5y－6}</span>的平方根是 ±2.\n<br>（3）∵ ﹣4 &gt; <i>x</i> &gt; 1，<br>∴<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{2}} + 8x + 16}－2\\sqrt{{{x}^{2}}－2x + 1}</span><br> = | <i>x</i> + 4 |－2| <i>x</i>－1 |<br> = <i>x</i> + 4 + 2(<i>x</i>－1)<br> = <i>x</i> + 4 + 2<i>x</i>－2<br> = 3<i>x</i> + 2"
        },
        {
            "kind": "example",
            "label": "题型二　利用商的算术平方根的性质进行化简",
            "type": 9,
            "Q": "将下列各式化简：（1）<span class=\"mathquill-rendered-math\">\\sqrt{1\\frac{7}{9}}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{32}}</span>；（3）<span class=\"mathquill-rendered-math\">a\\sqrt{\\frac{28}{{{a}^{3}}}}\\left(a &gt; 0 \\right)</span>.\n",
            "way": "（1）先将带分数化为假分数，再应用性质化简；<br>（2）需要将分子、分母同乘2，将分母化成一个完全平方数，然后应用性质化简；<br>（3）先将被开方数的分子、分母同乘 <i>a</i>，再应用“<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{a}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{b}}{\\sqrt{a}}</span> (<i>b</i> &ge; 0，<i>a</i> &gt; 0)”进行化简.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\sqrt{1\\frac{7}{9}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{16}{9}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{16}}{\\sqrt{9}}</span>=<span class=\"mathquill-rendered-math\">\\frac{4}{3}</span>.\n<br>（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{32}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{2}{64}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{2}}{8}</span>.\n<br>（3）<span class=\"mathquill-rendered-math\">a\\sqrt{\\frac{28}{{{a}^{3}}}}</span>=<span class=\"mathquill-rendered-math\">a\\sqrt{\\frac{{{2}^{2}} × 7}{{{a}^{2}}\\cdot a}}</span>=<span class=\"mathquill-rendered-math\">a\\sqrt{\\frac{{{2}^{2}} × 7 × a}{{{a}^{2}}\\cdot {{a}^{2}}}}</span>=<span class=\"mathquill-rendered-math\">a\\cdot \\frac{2\\sqrt{7a}}{{{a}^{2}}}</span>=<span class=\"mathquill-rendered-math\">\\frac{2}{a}\\sqrt{7a}</span>.\n",
            "notice": "（1）若被开方数的分母是一个完全平方数（式），则可以直接利用商的算术平方根的性质，先将分子分母分开平方，然后求商；<br>（2）若被开方数的分母不是完全平方数（式）可根据分式的基本性质先将分式的分子、分母同时乘一个不等于0的数或整式，使分母变成一个完全平方数（式）然后和用商的算术平方根的性质进行化简.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "化简：（1）<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{24a}}{\\sqrt{3{{a}^{3}}}}\\left(a &gt; 0 \\right)</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{5}}</span>÷<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{20{{a}^{2}}}}\\left(a &gt; 0，b &gt; 0 \\right)</span>.\n",
            "way": "观察题目特征，利用公式<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{a}}{\\sqrt{b}} = \\sqrt{\\frac{a}{b}}</span>来计算.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{24a}}{\\sqrt{3{{a}^{3}}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{24a}{3{{a}^{3}}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{8}{{{a}^{2}}}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{8}}{\\sqrt{{{a}^{2}}}}</span>=<span class=\"mathquill-rendered-math\">\\frac{2\\sqrt{2}}{a}</span>.\n<br>（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{5}}</span>÷<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{20{{a}^{2}}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{5} ÷ \\frac{b}{20{{a}^{2}}}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{b}{5}\\cdot \\frac{20{{a}^{2}}}{b}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{4{{a}^{2}}}</span>=<span class=\"mathquill-rendered-math\">2a</span>.\n",
            "notice": "由公式<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{a}}{\\sqrt{b}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{a}{b}}</span>可以转化为<span class=\"mathquill-rendered-math\">\\sqrt{a}</span>÷<span class=\"mathquill-rendered-math\">\\sqrt{b}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{a ÷ b}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{a}{b}}</span>.\n"
        },
        {
            "kind": "kpt",
            "name": "最简二次根式",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "最简二次根式：",
                         "sub": ["一般地，被开方数不含分母，也不含能开得尽方的因数或因式，叫做最简二次根式.\n"
                          ]
                     },
                     {
                         "kind": "kpt",
                         "name": "去掉分母中的根号（分母有理化）的方法：",
                         "sub": ["（1）当分母是<span class=\"mathquill-rendered-math\">\\sqrt{a}</span>或<span class=\"mathquill-rendered-math\">b\\sqrt{a}</span>的形式时，分子与分母同乘<span class=\"mathquill-rendered-math\">\\sqrt{a}</span>；<br>（2）当分母是<span class=\"mathquill-rendered-math\">a + \\sqrt{b}</span>的形式时，分子与分母同乘<span class=\"mathquill-rendered-math\">a－\\sqrt{b}</span>，利用平方差公式将分母中的根号去掉；<br>（3）当分母是<span class=\"mathquill-rendered-math\">\\sqrt{a} + \\sqrt{b}</span>的形式时，分子与分母同乘<span class=\"mathquill-rendered-math\">\\sqrt{a}－\\sqrt{b}</span>，利用平方差公式将分母中的根号去掉.\n"
                          ]
                     },
                     {
                         "kind": "kpt",
                         "name": "易错警示：",
                         "sub": ["（1）分母中含有根式的式子不是最简二次根式；<br>（2）化简时容易忽视隐含条件，将负数移到根号外.\n"
                          ]
                     }
            ]
        },
        {
            "kind": "video",
            "title": "最简二次根式",
            "name": "最简二次根式",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18684.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　最简二次根式的识别",
            "type": 1,
            "Q": "下列二次根式中，是最简二次根式的是（　　）",
            "opts": [
                "<span class=\"mathquill-rendered-math\">\\sqrt{0.2}</span>",
                "<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}}－{{b}^{2}}}</span>",
                "<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{x}}</span>",
                "<span class=\"mathquill-rendered-math\">\\sqrt{4a}</span>"
            ],
            "way": "A选项中含有小数；D选项的被开方数中含有能开得尽方的因数；C选项的被开方数中含有分母；<br>因此这三个选项都不符合最简二次根式的要求.\n所以本题的答案应该是B.\n",
            "step": "A.\n<span class=\"mathquill-rendered-math\">\\sqrt{0.2}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{2}{10}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{5}}{5}</span>，不是最简二次根式；<br>B.\n<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}}－{{b}^{2}}}</span>，不含有未开尽方的因数或因式，是最简二次根式；<br>C.\n<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{x}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{x}}{x}</span>，被开方数中含有分母，故不是最简二次根式；<br>D.\n<span class=\"mathquill-rendered-math\">\\sqrt{4a}</span>=<span class=\"mathquill-rendered-math\">2\\sqrt{a}</span>，不是最简二次根式.\n<br>只有选项B中的是最简二次根式，故选B.\n",
            "notice": "在判断最简二次根式的过程中要注意：<br>（1）在二次根式的被开方数中，只要含有分数或小数，就不是最简二次根式；<br>（2）在二次根式的被开方数中的每一个因式（或因数），如果幂的指数大于或等于2，也不是最简二次根式.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "下列各式中，哪些是最简二次根式，哪些不是最简二次根式？不是最简二次根式的请说明理由.\n<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{2}}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{2}} + 1}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{0.2}</span>；（4）<span class=\"mathquill-rendered-math\">\\sqrt{8}</span>；（5）<span class=\"mathquill-rendered-math\">\\sqrt{24x}</span>；（6）<span class=\"mathquill-rendered-math\">\\sqrt{{{x}^{3}} + 6{{x}^{2}} + 9x}</span>.\n",
            "way": "判断一个式子是不是最简二次根式，要抓住最简二次根式应满足的条件.\n",
            "step": "（1）被开方数不含分母（2）被开方数中不含开得尽方的因式，满足这两个条件的二次根式为最简二次根式.\n<br>(1)(3)中被开方数含分母（小数可化为分数），故(1)(3)不是最简二次根式，(4)(5)(6)可分别化为<span class=\"mathquill-rendered-math\">\\sqrt{{{2}^{2}} × 2}</span>，<span class=\"mathquill-rendered-math\">\\sqrt{{{2}^{2}} × 6 × x}</span>，<span class=\"mathquill-rendered-math\">\\sqrt{x{{(x + 3)}^{2}}}</span>被开方数中含开得尽方的因数或因式，故(4)(5)(6)不是最简二次根式，故(2)是最简二次根式，(1)(3)(4)(5)(6)均不是最简二次根式.\n故答案为：只有(2)是最简二次根式.\n",
            "notice": "解决此类题目时，根据最简二次根式的概念，先将被开方数中的小数转化成分数，利用分数的基本性质将分母化为完全平方数（式）再将其算术平方根移到根号外，最后将分母中的二次根式化成整数或整式.\n"
        },
        {
            "kind": "example",
            "label": "题型二　最简二次根式的化简",
            "type": 9,
            "Q": "把下列二次根式化成最简二次根式：<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{3.5}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{4\\frac{1}{16}}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{27}{3x}}</span>；（4）<span class=\"mathquill-rendered-math\">\\sqrt{25－10x + {{x}^{2}}}</span> (<i>x</i> &ge; 5).\n",
            "way": "（1）先化为假分数，再分子分母同乘以2即可；<br>（2）先化为假分数再化为最简二次根式即可；<br>（3）分子分母同乘 3<i>x</i> 即可；<br>（4）根据完全平方公式，化为<span class=\"mathquill-rendered-math\">\\sqrt{{{\\left(5－x \\right)}^{2}}}</span>即可.\n",
            "step": "（1）原式 =<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{7}{2}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{14}}{2}</span>；<br>（2）原式 =<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{65}{16}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{65}}{4}</span>；<br>（3）原式 =<span class=\"mathquill-rendered-math\">\\frac{3\\sqrt{3} × \\sqrt{3x}}{3x}</span>=<span class=\"mathquill-rendered-math\">\\frac{3\\sqrt{x}}{x}</span>；<br>（4）原式 =<span class=\"mathquill-rendered-math\">\\sqrt{{{\\left(5－x \\right)}^{2}}} = x－5</span>.\n",
            "notice": "本题考查了最简二次根式的定义.\n根据最简二次根式的定义，最简二次根式必须满足两个条件：<br>（1）被开方数不含分母；<br>（2）被开方数不含能开得尽方的因数或因式.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "下列各式化成最简二次根式：<br>（1）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{4}{3}}</span>；（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{2} + \\frac{1}{3}}</span>；（3）<span class=\"mathquill-rendered-math\">\\sqrt{45{{a}^{2}}b}\\left(a &gt; 0，b &gt; 0 \\right)</span>；（4）<span class=\"mathquill-rendered-math\">{{x}^{2}}\\sqrt{\\frac{1}{8{{x}^{3}}}}\\left(x &gt; 0 \\right)</span>.\n",
            "way": "根据二次根式的乘除法，可化简成最简二次根式.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{4}{3}}</span>=<span class=\"mathquill-rendered-math\">\\frac{2}{3}\\sqrt{3}</span>；<br>（2）<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{1}{2} + \\frac{1}{3}}</span>=<span class=\"mathquill-rendered-math\">\\sqrt{\\frac{5}{6}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{30}}{6}</span>；<br>（3）<span class=\"mathquill-rendered-math\">\\sqrt{45{{a}^{2}}b}</span>(<i>a</i> &gt; 0，<i>b</i> &gt; 0) =<span class=\"mathquill-rendered-math\">\\sqrt{9{{a}^{2}}\\cdot 5b}</span>=<span class=\"mathquill-rendered-math\">3a\\sqrt{5b}</span>；<br>（4）<span class=\"mathquill-rendered-math\">{{x}^{2}}\\sqrt{\\frac{1}{8{{x}^{3}}}}</span>=<span class=\"mathquill-rendered-math\">{{x}^{2}}\\sqrt{\\frac{2x}{16{{x}^{4}}}}</span>=<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{2x}}{4}</span>.\n"
        },
        {
            "kind": "example",
            "label": "题型三　分母有理化",
            "type": 9,
            "Q": "去掉下列各式分母中的根号：<br>（1）<span class=\"mathquill-rendered-math\">\\frac{3}{\\sqrt{3}}</span>；（2）<span class=\"mathquill-rendered-math\">\\frac{5}{\\sqrt{32}}</span>；（3）<span class=\"mathquill-rendered-math\">\\frac{2}{\\sqrt{ab}}\\left(ab &gt; 0 \\right)</span>.\n",
            "way": "将分母有理化，使分母转化为<span class=\"mathquill-rendered-math\">{{\\left(\\sqrt{a} \\right)}^{2}}</span>或<span class=\"mathquill-rendered-math\">\\sqrt{{{a}^{2}}}</span>的形式.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\frac{3}{\\sqrt{3}}</span><span class=\"mathquill-rendered-math\"> = \\frac{3 × \\sqrt{3}}{\\sqrt{3} × \\sqrt{3}}</span><span class=\"mathquill-rendered-math\"> = \\frac{3\\sqrt{3}}{3}</span><span class=\"mathquill-rendered-math\"> = \\sqrt{3}</span>.\n<br>（2）<span class=\"mathquill-rendered-math\">\\frac{5}{\\sqrt{32}}</span><span class=\"mathquill-rendered-math\"> = \\frac{5}{\\sqrt{16 × 2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{5}{\\sqrt{16} × \\sqrt{2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{5}{4\\sqrt{2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{5 × \\sqrt{2}}{4\\sqrt{2} × \\sqrt{2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{5\\sqrt{2}}{8}</span>.\n<br>（3）<span class=\"mathquill-rendered-math\">\\frac{2}{\\sqrt{2ab}}</span><span class=\"mathquill-rendered-math\"> = \\frac{2 × \\sqrt{2ab}}{\\sqrt{2ab} × \\sqrt{2ab}}</span><span class=\"mathquill-rendered-math\"> = \\frac{2\\sqrt{2ab}}{2ab}</span><span class=\"mathquill-rendered-math\"> = \\frac{\\sqrt{2ab}}{ab}</span>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "去掉下列各式分母中的根号：<br>（1）<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{2}}{3\\sqrt{48}}</span>；（2）<span class=\"mathquill-rendered-math\">\\frac{3}{\\sqrt{a + 2}}</span>；（3）<span class=\"mathquill-rendered-math\">\\frac{2}{\\sqrt{5}－\\sqrt{3}}</span>；（4）<span class=\"mathquill-rendered-math\">\\frac{x－y}{\\sqrt{x} + \\sqrt{y}}</span>.\n",
            "way": "本题属于分母有理化的题目，解决此类题的关键是掌握分母有理化的方法；<br>1、对于（1），对给出的分式中的分子分母同时乘以<span class=\"mathquill-rendered-math\">\\sqrt{48}</span>，化简即可；<br>2、对于（2），对给出的分式中的分子分母同时乘以<span class=\"mathquill-rendered-math\">\\sqrt{a + 2}</span>即可化简，接下来自己试着对其余两个进行解答即可.\n",
            "step": "（1）<span class=\"mathquill-rendered-math\">\\frac{\\sqrt{2}}{3\\sqrt{48}}</span><span class=\"mathquill-rendered-math\"> = \\frac{\\sqrt{2} × \\sqrt{48}}{3\\sqrt{48} × \\sqrt{48}}</span><span class=\"mathquill-rendered-math\"> = \\frac{\\sqrt{6}}{36}</span>；<br>（2）<span class=\"mathquill-rendered-math\">\\frac{3}{\\sqrt{a + 2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{3\\sqrt{a + 2}}{\\sqrt{a + 2} × \\sqrt{a + 2}}</span><span class=\"mathquill-rendered-math\"> = \\frac{3\\sqrt{a + 2}}{a + 2}</span>；<br>（3）<span class=\"mathquill-rendered-math\">\\frac{2}{\\sqrt{5}－\\sqrt{3}}</span><span class=\"mathquill-rendered-math\"> = \\frac{2\\left(\\sqrt{5} + \\sqrt{3} \\right)}{\\left(\\sqrt{5} + \\sqrt{3} \\right)\\left(\\sqrt{5}－\\sqrt{3} \\right)}</span><span class=\"mathquill-rendered-math\"> = \\frac{2\\left(\\sqrt{5} + \\sqrt{3} \\right)}{2}</span><span class=\"mathquill-rendered-math\"> = \\sqrt{5} + \\sqrt{3}</span>；<br>（4）<span class=\"mathquill-rendered-math\">\\frac{x－y}{\\sqrt{x} + \\sqrt{y}}</span><span class=\"mathquill-rendered-math\"> = \\frac{\\left(x－y \\right)\\left(\\sqrt{x}－\\sqrt{y} \\right)}{\\left(\\sqrt{x} + \\sqrt{y} \\right)\\left(\\sqrt{x}－\\sqrt{y} \\right)}</span><span class=\"mathquill-rendered-math\"> = \\frac{(x－y)\\left(\\sqrt{x}－\\sqrt{y} \\right)}{(x－y)}</span><span class=\"mathquill-rendered-math\"> = \\sqrt{x}－\\sqrt{y}</span>.\n",
            "notice": "分母有理化一般经历如下三步：一移”，即将分子、分母中能开得尽方的因数式）开后方后移到根号外，“二乘”，即将分子、分母同乘分母的有理化因数式；“三化”，即化简计算.\n"
        }
    ]
}