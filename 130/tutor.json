{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第三讲 一元二次方程根与系数的关系及相关应用",
            "description": "1.\n掌握一元二次方程根的判别式判别方程根的情况；<br>2.\n掌握一元二次方程的根与系数的关系以及在各类问题中的运用；<br>3.\n通过列方程解应用题，进一步提高逻辑思维能力、分析问题和解决问题的能力.\n"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程根与系数的关系",
            "sub": [
                "　　<b>1.\n二次项系数为1的一元二次方程根与系数的关系：</b>若<i>x</i><sub>1</sub>，<i>x</i><sub>2</sub>是一元二次方程<i>x</i><sup>2</sup>  + <i>px</i> + <i>q</i> = 0的两根，则<i>x</i><sub>1</sub> + <i>x</i><sub>2</sub> = <i>p</i>，<i>x</i><sub>1</sub>·<i>x</i><sub>2</sub> = <i>q</i>.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)根与系数的关系：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若<i>x</i><sub>1</sub>，<i>x</i><sub>2</sub>是一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)的两根，则<span class=\"mathquill-rendered-math\">{{x}_{1}} + {{x}_{2}} = －\\frac{b}{a}</span>，<span class=\"mathquill-rendered-math\">{{x}_{1}}\\cdot{{x}_{2}} = \\frac{c}{a}</span>.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>注：</b>根与系数的关系也称为“韦达定理”，在使用时注意使用条件：(1) 必须是一元二次方程；(2) 有两个实数根.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用方程中各项系数求两根的和与积",
            "type": 9,
            "Q": "在不解方程的情况下，求下列方程的两根的和与积<br>(1) <i>x</i><sup>2</sup> －2<i>x</i>－3 = 0； (2) 3<i>x</i><sup>2</sup>  + <i>x</i>－1 = 0； (3) <span class=\"mathquill-rendered-math\">\\sqrt{2}{x}^{2} + 4x－1 = 0</span>.\n",
            "way": "首先确定<i>a，b，c</i>的值，然后直接利用根与系数的关系求解即可求得答案.\n",
            "step": "(1) ∵ <i>a</i> = 1，<i>b</i> = －2，<i>c</i> = －3，<br>∴ <span class=\"mathquill-rendered-math\">{{x}_{1}} + {{x}_{2}} = －\\frac{b}{a} = 2</span>，<span class=\"mathquill-rendered-math\">{{x}_{1}}\\cdot{{x}_{2}} = \\frac{c}{a} = －3</span>；<br>(2) ∵ <i>a</i> = 3，<i>b</i> = 1，<i>c</i> = －1，<br>∴ <span class=\"mathquill-rendered-math\">{{x}_{1}} + {{x}_{2}} = －\\frac{b}{a} = －\\frac{1}{3}</span>，<span class=\"mathquill-rendered-math\">{{x}_{1}}\\cdot{{x}_{2}} = \\frac{c}{a} = －\\frac{1}{3}</span>；<br>(3) ∵ <i>a</i> = <span class=\"mathquill-rendered-math\">\\sqrt{2}</span>，<i>b</i> = 4，<i>c</i> = －1，<br>∴ <span class=\"mathquill-rendered-math\">{{x}_{1}} + {{x}_{2}} = －\\frac{b}{a} = －\\frac{4}{\\sqrt{2}} = －2\\sqrt{2}</span>，<span class=\"mathquill-rendered-math\">{{x}_{1}}\\cdot{{x}_{2}} = \\frac{c}{a} = －\\frac{1}{\\sqrt{2}} = －\\frac{\\sqrt{2}}{2}</span>.\n",
            "notice": "1.\n此题考查了根与系数的关系.\n注意<i>x</i><sub>1</sub>，<i>x</i><sub>2</sub>是一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)的两根时，<span class=\"mathquill-rendered-math\">{{x}_{1}} + {{x}_{2}} = －\\frac{b}{a}</span>，<span class=\"mathquill-rendered-math\">{{x}_{1}}\\cdot{{x}_{2}} = \\frac{c}{a}</span>.\n<br>2.一元二次方程与系数的关系是建立在一元二次方程有实数根的前提条件下的。如一元二次方程<span class=\"mathquill-rendered-math\">x^{2}+x+2=0</span>无实数根，所以不能说这个方程的两根之和为-1。"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "（中考真题）一元二次方程<i>x</i><sup>2</sup>  + 4<i>x</i>－3 = 0的两根为<i>x</i><sub>1</sub>，<i>x</i><sub>2</sub>，则<i>x</i><sub>1</sub>·<i>x</i><sub>2</sub>的值是（　　）",
            "opts": [
                "4",
                "－4",
                "3",
                "－3"
            ],
            "step": "答案：D.\n"
        },
        {
            "kind": "example",
            "label": "题型二  求与两根有关的代数式的值",
            "type": 2,
            "Q": "（中考真题）已知一元二次方程2<i>x</i><sup>2</sup> －5<i>x</i> + 1 = 0的两根为<i>m，n</i>，则<i>m</i><sup>2</sup>  + <i>n</i><sup>2</sup> = ________.\n",
            "opts": [
                "没有实数根",
                "只有一个实数根",
                "有两个相等的实数根",
                "有两个不相等的实数根"
            ],
            "way": "先由根与系数的关系得：两根和与两根积，再将<i>m</i><sup>2</sup>  + <i>n</i><sup>2</sup> 进行变形，化成和或积的形式，代入即可.\n",
            "step": "由根与系数的关系得：<i>m + n</i> = <span class=\"mathquill-rendered-math\">\\frac{5}{2}</span>，<i>mn</i> = <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>，<br>∴ <i>m</i><sup>2</sup> + <i>n</i><sup>2</sup> = (<i>m + n</i>)<sup>2</sup> －2<i>mn</i> = (<span class=\"mathquill-rendered-math\">\\frac{5}{2}</span>)<sup>2</sup><span class=\"mathquill-rendered-math\">－2 × \\frac{1}{2} = \\frac{21}{4}</span>，<br>故答案为：<span class=\"mathquill-rendered-math\">\\frac{21}{4}</span>.\n",
            "notice": "本题考查了利用根与系数的关系求代数式的值，先将一元二次方程化为一般形式，写出两根的和与积的值，再将所求式子进行变形；如<span class=\"mathquill-rendered-math\">\\frac{1}{{{x}_{1}}} + \\frac{1}{{{x}_{2}}}</span>，<i>x</i><sub>1</sub><sup>2</sup>  + <i>x</i><sub>2</sub><sup>2</sup> 等等，本题是常考题型，利用完全平方公式进行转化.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "设<i>a，b</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的两个实数根，则<i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i>的值为（　　）",
            "opts": [
                "2014",
                "2015",
                "2016",
                "2017"
            ],
            "step": "∵ <i>a</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的实数根，<br>∴ <i>a</i><sup>2</sup>  + <i>a</i>－2016 = 0，<br>∴ <i>a</i><sup>2</sup> = －<i>a</i> + 2016，<br>∴ <i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i> = －<i>a</i> + 2016 + 2<i>a</i> + <i>b</i> = <i>a + b</i> + 2016，<br>∵ <i>a、b</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的两个实数根，<br>∴ <i>a + b</i> = －1，<br>∴ <i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i> = －1 + 2016 = 2015.\n<br>故选B.\n"
        },
        {
            "kind": "example",
            "label": "题型三 利用两个根的情况确定待定字母的取值范围",
            "type": 1,
            "Q": "（中考真题）关于<i>x</i>的一元二次方程(<i>m</i>－2)<i>x</i><sup>2</sup>  + (2<i>m</i> + 1)<i>x</i> + <i>m</i>－2 = 0有两个不相等的正实数根，则<i>m</i>的取值范围是（　　）",
            "opts": [
                "<i>m</i> &gt; <span class=\"mathquill-rendered-math\">\\frac{3}{4}</span>",
                "<i>m</i> &gt; <span class=\"mathquill-rendered-math\">\\frac{3}{4}</span>且<i>m</i> ≠ 2",
                "<span class=\"mathquill-rendered-math\">－\\frac{1}{2}</span> &lt; <i>m</i> < 2",
                "<span class=\"mathquill-rendered-math\">\\frac{3}{4}</span> &lt; <i>m</i> < 2"
            ],
            "way": "根据一元二次方程的定义和根的判别式的意义得到<i>m</i>－2 ≠ 0且Δ = (2<i>m</i> + 1)<sup>2</sup> －4(<i>m</i>－2)(<i>m</i>－2) &gt; 0，解得<span class=\"mathquill-rendered-math\">m &gt; \\frac{3}{4}</span>且<i>m</i> ≠ 2，再利用根与系数的关系得到<span class=\"mathquill-rendered-math\">－\\frac{2m + 1}{m－2}</span> &gt; 0，则<i>m</i>－2 &lt; 0时，方程有正实数根，于是可得到<i>m</i>的取值范围为<span class=\"mathquill-rendered-math\">\\frac{3}{4}</span> &lt; <i>m</i> < 2．",
            "step": "根据题意得<i>m</i>－2 ≠ 0且Δ = (2<i>m</i> + 1)<sup>2</sup> －4(<i>m</i>－2)(<i>m</i>－2) &gt; 0，<br>解得<span class=\"mathquill-rendered-math\">m &gt; \\frac{3}{4}</span>且<i>m</i> ≠ 2，<br>设方程的两根为<i>a、b</i>，则<i>a + b</i> = <span class=\"mathquill-rendered-math\">－\\frac{2m + 1}{m－2}</span> &gt; 0，<i>ab</i> = <span class=\"mathquill-rendered-math\">\\frac{m－2}{m－2}</span> = 1 &gt; 0，<br>而2<i>m</i> + 1 &gt; 0，<br>∴ <i>m</i>－2 &lt; 0，即<i>m</i> &lt; 2，<br>∴ <i>m</i>的取值范围为<span class=\"mathquill-rendered-math\">\\frac{3}{4}</span> &lt; <i>m</i> &lt; 2.\n<br>故选D.\n",
            "notice": "本题考查了根的判别式：一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)的根与Δ = <i>b</i><sup>2</sup> －4<i>ac</i>有如下关系：当Δ &gt; 0时，方程有两个不相等的实数根；当Δ = 0时，方程有两个相等的两个实数根；当Δ < 0时，方程无实数根．也考查了根与系数的关系．"
        },
        {
            "kind": "ques",
            "type": 2,
            "Q": "关于<i>x</i>的一元二次方程<i>x</i><sup>2</sup>  + 2<i>x</i>－2<i>m</i> + 1 = 0的两个实数根之积为负，则实数<i>m</i>的取值范围是________.\n",
            "step": "因为关于<i>x</i>的一元二次方程有两个实数根，所以判别式Δ = <i>b</i><sup>2</sup> －4<i>ac</i> = 2<sup>2</sup> －4 × 1 × (－2<i>m</i> + 1) = 8<i>m</i> &gt; 0，因为方程的两个实数根之积为负，根据韦达定理得<span class=\"mathquill-rendered-math\">\\frac{c}{a} = －2m + 1 &lt; 0</span>，则可列一元一次不等式组<img src=\"1.svg\" style=\"width:10em; vertical-align:middle\">，解①得<i>m</i> &gt; 0，解②得<i>m</i> &gt; <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>，所以<i>m</i>的取值范围是<i>m</i> &gt; <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>.\n<br>故本题正确答案为<i>m</i> &gt; <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>.\n"
        },
        {
            "kind": "example",
            "label": "题型四  利用根的定义及根与系数的关系求非对称代数式的值",
            "type": 2,
            "Q": "已知<i>m，n</i>是方程<i>x</i><sup>2</sup>  + 2<i>x</i>－5 = 0的两个实数根，则<i>m</i><sup>2</sup>  + 3<i>mn</i> + <i>n</i><sup>2</sup> = ________.\n",
            "way": "从要求的代数式看是非对称的，所以要单独代入方程的解，得到与待求值的代数式相关的结构.\n代数式中有<i>m</i><sup>2</sup> ，所以将<i>x = m</i>代入方程中，然后结合两根之和与两根之积进行整体代入求值.\n",
            "step": "∵ <i>m，n</i>是方程<i>x</i><sup>2</sup>  + 2<i>x</i>－5 = 0的两个实数根，<br>∴ <i>m + n</i> = －2，<i>mn</i> = －5，<br>∴ <i>m</i><sup>2</sup>  + 3<i>mn</i> + <i>n</i><sup>2</sup> = (<i>m + n</i>)<sup>2</sup>  + <i>mn</i> = (－2)<sup>2</sup> －5 = －1.\n<br>故答案为：－1.\n",
            "notice": "求与根有关的代数式的值时，看代数式是否具有对称性，若具有对称性，则直接变形，将两根之和或两根之积代入求值； 若不具有对称性，则将其中的某一个根单独代入方程中，得到与待求值的代数式相关的结构，进行整体代入求值.\n"
        },
        {
            "kind": "ques",
            "type": 2,
            "Q": "设<i>a，b</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的两个实数根，则<i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i>的值为________.\n",
            "step": "∵ <i>a</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的实数根，<br>∴ <i>a</i><sup>2</sup>  + <i>a</i>－2016 = 0，<br>∴ <i>a</i><sup>2</sup> = －<i>a</i> + 2016，<br>∴ <i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i> = －<i>a</i> + 2016 + 2<i>a</i> + <i>b</i> = <i>a</i> + <i>b</i> + 2016，<br>又∵  <i>a，b</i>是方程<i>x</i><sup>2</sup>  + <i>x</i>－2016 = 0的两个实数根，<br>∴ <i>a</i> + <i>b</i> = －1，<br>∴ <i>a</i><sup>2</sup>  + 2<i>a</i> + <i>b</i> = －1 + 2016 = 2015.\n"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程的实际问题－－病毒传播型问题",
            "sub": [
                "　　<b>病毒传播问题：</b>解决病毒传播问题关键扣住两点：一是传染源，二是传染的速度.\n若开始时传染源是1，传染的速度是<i>x</i>，则一轮传染后是(1 + <i>x</i>)；二轮传染时，传染源为(1 + <i>x</i>)，传染的速度还是<i>x</i>，则二轮传染后是(1 + <i>x</i>)<sup>2</sup> .\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用一元二次方程解决病毒传播问题",
            "type": 9,
            "Q": "有一个人患了流感，经过两轮传染后共有36人患了流感.\n<br>(1) 求每轮传染中平均一个人传染了几个人？<br>(2) 如果不及时控制，第三轮将又有多少人被传染？",
            "way": "(1) 设平均一人传染了<i>x</i>人，根据有一人患了流感，经过两轮传染后共有36人患了流感，列方程求解即可；<br>(2) 根据每轮传染中平均一个人传染的人数和经过两轮传染后的人数，列出算式求解即可.\n",
            "step": "(1) 设每轮传染中平均一个人传染了<i>x</i>个人，根据题意得：<br><i>x</i> + 1 + (<i>x</i> + 1)<i>x</i> = 36，<br>解得：<i>x</i> = 5或<i>x</i> = －7(舍去).\n<br>答：每轮传染中平均一个人传染了5个人；<br>(2) 根据题意得：5 × 36 = 180(个)，<br>答：第三轮将又有180人被传染.\n",
            "notice": "病毒传染问题，每轮传染都保留原体，若传染源为1，传播的速度为<i>x</i>，则经过<i>n</i>轮传染后被感染的数量为(1 + <i>x</i>)<sup>n</sup>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "某种电脑病毒传播非常快，如果一台电脑被感染，经过两轮感染后就会有81台电脑被感染.\n请你用学过的知识分析，每轮感染中平均一台电脑会感染几台电脑？若病毒得不到有效控制，3轮感染后，被感染的电脑会不会超过700台？",
            "step": "设每轮感染中平均每一台电脑会感染<i>x</i>台电脑，依题意得：1 + <i>x</i> + (1 + <i>x</i>)<i>x</i> = 81，<br>整理得(1 + <i>x</i>)<sup>2</sup> = 81，<br>则<i>x</i> + 1 = 9或<i>x</i> + 1 = －9，解得<i>x</i><sub>1</sub> = 8，<i>x</i><sub>2</sub> = －10(舍去)，<br>∴ (1 + <i>x</i>)<sup>2</sup>  + <i>x</i>(1 + <i>x</i>)<sup>2</sup> = (1 + <i>x</i>)<sup>3</sup> = (1 + 8)<sup>3</sup> = 729＞700.\n<br>答：每轮感染中平均每一台电脑会感染8台电脑，3轮感染后，被感染的电脑会超过700台.\n"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程的实际问题－－－（增）率问题",
            "sub": [
                "　　<b>1.\n连续增长问题：</b>如果起始量为<i>a</i>，平均增长率为<i>x</i>，变化后的量为<i>b</i>，则增长一次后的量为<i>a + ax</i> = <i>a</i>(1 + <i>x</i>)；再增长一次后的量为：<i>a</i>(1 + <i>x</i>) + <i>a</i>(1 + <i>x</i>)<i>x</i> = <i>a</i>(1 + <i>x</i>)<sup>2</sup> .\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n连续下降问题：</b>基数是<i>a</i>，两次平均降低率为<i>x</i>，则一次降低后的数量为<i>a</i>(1－<i>x</i>)，二次降低后的数量为<i>a</i>(1－<i>x</i>)<sup>2</sup> .\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用增长率进行预算",
            "type": 9,
            "Q": "（中考真题）为进一步发展基础教育，自2014年以来，某县加大了教育经费的投入，2014年该县投入教育经费6000万元.\n2016年投入教育经费8640万元.\n假设该县这两年投入教育经费的年平均增长率相同.\n<br>(1) 求这两年该县投入教育经费的年平均增长率；<br>(2) 若该县教育经费的投入还将保持相同的年平均增长率，请你预算2017年该县投入教育经费多少万元.\n",
            "way": "(1) 设该县投入教育经费的年平均增长率为<i>x</i>，根据2014年该县投入教育经费6000万元和2016年投入教育经费8640万元列出方程，再求解即可；<br>(2) 根据2016年该县投入教育经费和每年的增长率，直接得出2017年该县投入教育经费为8640 × (1 + 0.2)，再进行计算即可.\n",
            "step": "(1) 设该县投入教育经费的年平均增长率为<i>x</i>，根据题意得：<br>6000(1 + <i>x</i>)<sup>2</sup> = 8640，<br>解得：<i>x</i><sub>1</sub> = 0.2 = 20%，<i>x</i><sub>2</sub> = －2.2(不合题意，舍去).\n<br>答：该县投入教育经费的年平均增长率为20%；<br>(2) 因为2016年该县投入教育经费为8640万元，且增长率为20%，<br>所以2017年该县投入教育经费为：8640 × (1 + 0.2) = 10368(万元)，<br>答：预算2017年该县投入教育经费10368万元.\n",
            "notice": "此类问题容易出错的地方是：①审题不到位，看不出题目能反映全部含义的相等关系；②忽视检验方程的解是否符合实际问题.\n<br>设基数为<i>a</i>，平均增长(或降低)率为<i>x</i>，增长(或降低)的次数为<i>n</i>，增长(或降低)后的量为<i>y</i>，则有表达式<i>a</i>(1 ± <i>x</i>)<sup>n</sup> = <i>y</i>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "在“全民阅读”活动中，某中学对全校学生中坚持每天半小时阅读的人数进行了调查，2014年全校坚持每天半小时阅读有1000名学生，2015年全校坚持每天半小时阅读人数比2014年增加10%，2016年全校坚持每天半小时阅读人数比2015年增加340人.\n<br>(1) 求2016年全校坚持每天半小时阅读学生人数；<br>(2) 求从2014年到2016年全校坚持每天半小时阅读的人数的平均增长率.\n",
            "step": "（１）由题意，得：2015年全校学生人数为：1000 × (1 + 10%) = 1100(人)，<br>　　∴ 2016年全校学生人数为：1100 + 340 = 1440(人)；<br>　　答:2016年全校坚持每天半小时阅读学生人数为1440人。<br>（２）设从2014年到2016年全校坚持每天半小时阅读的人数的平均增长率为<i>x</i>，根据题意得：1000(1 + <i>x</i>)2 = 1440，<br>解得：<i>x</i><sub>1</sub> = 0.2 = 20%，<i>x</i><sub>2</sub> = －2.2(舍去)，<br>答：从2014年到2016年全校坚持每天半小时阅读的人数的平均增长率为20%.\n"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程的实际问题－－－商品经济问题",
            "sub": [
                "　　1.\n商品经济问题涉及到的量：进价，标价，售价，销售量，利润，利润率；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.\n商品经济问题常用的等量关系：利润 = 售价－进价，销售总利润 = 单个利润 × 销售量.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用增长率进行预算",
            "type": 9,
            "Q": "（中考真题）水果店张阿姨以每斤2元的价格购进某种水果若干斤，然后以每斤4元的价格出售，每天可售出100斤，通过调查发现，这种水果每斤的售价每降低0.1元，每天可多售出20斤，为保证每天至少售出260斤，张阿姨决定降价销售.\n<br>(1) 若将这种水果每斤的售价降低 <i>x</i> 元，则每天的销售量是________斤(用含 <i>x</i> 的代数式表示)；<br>(2) 销售这种水果要想每天盈利300元，张阿姨需将每斤的售价降低多少元？",
            "way": "(1) 根据“这种水果每斤的售价每降低0.1元，每天可多售出20斤”确定出每斤的售价降低 <i>x</i> 元时，销量增加的量，从而确定每天的销量；<br>(2) 根据总盈利等于每斤的盈利与销量的积列方程求解.\n",
            "step": "(1) 将这种水果每斤的售价降低 <i>x</i> 元，则每天的销售量是<span class=\"mathquill-rendered-math\">100 + \\frac{x}{0.1} × 20 = 100 + 200x</span>(斤)；<br>(2) 根据题意得：(4－2－<i>x</i>)(100 + 200<i>x</i>) = 300，<br>解得：<i>x</i> = <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>或<i>x</i> = 1，<br>当<i>x</i> = <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>时，销售量是100 + 200 × <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span> = 200(斤) &gt; 260(斤)；<br>当<i>x</i> = 1时，销售量是100 + 200 = 300(斤).\n<br>∵ 每天至少售出260斤，<br>∴ <i>x</i> = 1.\n<br>答：张阿姨需将每斤的售价降低1元.\n",
            "notice": "本题考查理解题意的能力，第一问关键求出每千克的利润，求出总销售量，从而利润.\n第二问，根据售价和销售量的关系，以利润做为等量关系列方程求解.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "某宾馆有50个房间供游客住宿，当每个房间的房价为每天200元时，房间会全部住满.\n当每个房间每天的房价每增加20元时，就会有一个房间空闲.\n宾馆需对游客居住的每个房间每天支出40元的各种费用，根据规定，每个房间每天的房价不得高于680元.\n设每个房间每天的房价为 <i>x</i> (元)( <i>x</i> 为10的正整数倍).\n<br>(1) 设一天订出的房间数为 <i>y</i>，求出 <i>y</i> 与 <i>x</i> 的函数关系式及自变量 <i>x</i> 的取值范围；<br>(2) 请你用含 <i>x</i> 的代数式表示宾馆的利润；<br>(3) 若宾馆的利润要达到14820元，且尽量降低宾馆的成本，一天应订出多少个房间？",
            "step": "(1) 由题意得：<br><span class=\"mathquill-rendered-math\">y = 50－\\frac{x－200}{20}</span>，且200 &le; <i>x</i> &le; 680，且 <i>x</i> 为10的正整数倍；<br>(2) 宾馆的利润为：<span class=\"mathquill-rendered-math\">(x－40)(50－\\frac{x－200}{20}) = －\\frac{1}{20}{x}^{2} + 62x－2400</span>(200 &le; <i>x</i> &le; 680)；<br>(3) 由题意，得<span class=\"mathquill-rendered-math\">－\\frac{1}{20}{x}^{2} + 62x－2400 = 14820</span>，<br>解得<i>x</i><sub>1</sub> = 420，<i>x</i><sub>2</sub> = 820(舍去)，<br>∵ 尽量降低宾馆的成本，<br>∴ <i>x</i> = 420，此时<span class=\"mathquill-rendered-math\">y = 50－\\frac{x－200}{20}</span> = 39，<br>答：一天应订出39个房间.\n"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程的实际问题－－－－图形面积问题",
            "sub": [
                "　　1.\n与几何图形有关的一元二次方程的应用题主要是将数量关系隐藏在图形中，用图形表示出来，这样的图形有三角形，四边形，主要涉及图形的周长，面积等；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.\n解决这类问题的关键是将不规则的图形通过平移，分割或补全变成规则图形，使问题得到解决.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用平移将不规则图形转化为规则图形解决问题",
            "type": 9,
            "Q": "一幅长20<i>cm</i>，宽12<i>cm</i>的图案，如图，其中有一横两竖的彩条，横、竖彩条的宽度比为3：2，设竖彩条的宽度为<i>xcm</i>，图案中三条彩条所占面积为<i>ycm</i><sup>2</sup> .\n<br><img src=\"1.png\" style=\"width: 12em; vertical-align:middle;padding:10px\"><br>(1) 求<i>y</i>与<i>x</i>之间的函数关系式.\n<br>(2) 若图案中三条彩条所占面积是图案面积的<span class=\"mathquill-rendered-math\">\\frac{2}{5}</span>，求横、竖彩条的宽度.\n",
            "step": "(1) 因为竖彩条的宽度为<i>xcm</i>，且横、竖彩条的宽度比为3：2，<br>所以横彩条的宽度为<span class=\"mathquill-rendered-math\">\\frac{3}{2}</span><i>xcm</i>，<br>所以根据题意得<span class=\"mathquill-rendered-math\">y = \\frac{3}{2}x × 20 + 2x × 12－2 × x × \\frac{3}{2}x = －3{{x}^{2}} + 54x</span>.\n<br>(2) 因为图案中三条彩条所占面积是图案面积的<span class=\"mathquill-rendered-math\">\\frac{2}{5}</span>，<br>所以<span class=\"mathquill-rendered-math\">\\frac{－3{{x}^{2}} + 54x}{20 × 12} = \\frac{2}{5}</span>，解得<i>x</i><sub>1</sub> = 2，<i>x</i><sub>2</sub> = 16(舍去)，<br>所以<i>x</i> = 2，所以<span class=\"mathquill-rendered-math\">\\frac{3}{2}x = \\frac{3}{2} × 2 = 3</span>，<br>即横彩条的宽度为3<i>cm</i>，竖彩条的宽度为2<i>cm</i>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "（中考真题）如图，某农场有一块长40<i>m</i>，宽32<i>m</i>的矩形种植地，为方便管理，准备沿平行于两边的方向纵、横各修建一条等宽的小路，要使种植面积为1140<i>m</i><sup>2</sup> ，求小路的宽.\n<br><img src=\"2.png\" style=\"width: 12em; vertical-align:middle;padding:10px\">",
            "way": "本题可设小路的宽为<i>xm</i>，将4块种植地平移为一个长方形，长为(40－<i>x</i>)<i>m</i>，宽为(32－<i>x</i>)<i>m</i>.\n根据长方形面积公式即可求出小路的宽.\n",
            "step": "设小路的宽为<i>xm</i>，依题意有.\n<br>(40－<i>x</i>)(32－<i>x</i>) = 1140，<br>整理，得<i>x</i><sup>2</sup> －72<i>x</i> + 140 = 0.\n<br>解得<i>x</i><sub>1</sub> = 2，<i>x</i><sub>2</sub> = 70(不合题意，舍去).\n<br>答：小路的宽应是2<i>m</i>.\n",
            "notice": "本题考查了一元二次方程的应用，应熟记长方形的面积公式.\n另外求出4块种植地平移为一个长方形的长和宽是解决本题的关键.\n"
        }
    ]
}