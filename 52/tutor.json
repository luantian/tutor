{
    "note": "平行四边形的性质与判定",
    "couID": 1,
    "no": 200,
    "pages": [
        {
            "kind": "cover",
            "title": "平行四边形的性质与判定",
            "description": "1.\n理解平行四边形及平行线间的距离的概念.\n<br/>2.\n掌握平行四边形的性质及判定，并能灵活运用解决问题，培养推理论证能力.\n<br/>3.\n理解三角形的中位线的概念，掌握三角形的中位线的概念，掌握三角形的中位线定理，并能运用这一定理解决有关线段的平行和平分问题.\n"
        },
        {
            "kind": "kpt",
            "name": "平行四边形的定义",
            "sub": [
                "",
                {
                    "kind": "kpt",
                    "name": "平行四边形的概念与表示方法",
                    "sub": [
                        "　　     两组对边分别平行的四边形叫做平行四边形.\n平行四边形用符号“▱”表示，平行四边形 记作“▱<i>ABCD</i>”，读作“平行四边形 <i>ABCD</i>”.\n<br><s class=\"key\"><b>注：</b>平行四边形的表示一般按一定方向（顺时针或逆时针）依次书写各顶点，不能乱序.\n</s>"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "平行四边形的基本元素",
                    "sub": [
                        "如图，平行四边形的基本元素有：边、角、对角线.\n<img src=\"2.png\" style=\"float:right;width:10em;vertical-align:middle;\"><br>邻边：<i>AD</i>和<i>AB</i> ，<i>BC</i>和<i>DC</i> ，<i>AD</i>和 <i>DC</i>，<i>AB</i>和<i>BC</i> .\n<br>对边：<i>AB</i>和<i>DC</i> ，<i>AD</i>和<i>BC</i>.\n<br>邻角：<i>∠BAD</i>和<i>∠ADC</i>，<i>∠BAD</i>和<i>∠ABC</i>，<i>∠ABC</i>和<i>∠BCD</i> ，<i>∠ADC</i>和<i>∠BCD</i> .\n<br>对角：<i>∠BAD</i>和<i>∠BCD</i> ，<i>∠ADC</i>和<i>∠ABC</i> .\n<br>对角线：<i>AC</i>和<i>BD</i> .\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 利用概念识别平行四边形与计数问题",
            "type": 1,
            "Q": "如图，在平行四边形 <i>ABCD</i>中，<i>EF</i> ∥ <i>AD</i>，<i>HN</i> ∥ <i>AB</i>，<i>EF</i>与<i>HN</i>相交于点<i>O</i> ，则图中共有平行四边形（　　）<img src=\"3.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "12个",
                "9个",
                "7个",
                "5个"
            ],
            "way": "认真观察图形，根据平行四边形的定义确定，不能遗漏.\n",
            "step": "∵四边形<i>ABCD</i>是平行四边形<br>∴<i>AD</i> ∥ <i>BC</i>，<i>AB</i> ∥ <i>CD</i><br>又∵<i>EF</i> ∥ <i>AD</i>，<i>HN</i> ∥ <i>AB</i>且<i>EF</i>与<i>HN</i>相交于点<i>O</i><br>∴<i>OH</i> ∥ <i>AE</i> ∥ <i>DF</i>，<i>ON</i> ∥ <i>BE</i> ∥ <i>CF</i>，<br><i>OE</i> ∥ <i>AH</i> ∥ <i>BN</i>，<i>OF</i> ∥ <i>HD</i> ∥ <i>NC</i><br>∴平行四边形有：▱<i>AEOH</i>，▱<i>HOFD</i>，▱<i>EBNO</i>，▱<i>ONCF</i>，▱<i>AEFD</i>，▱<i>EBCF</i>，▱<i>ABNH</i>，▱<i>HNCD</i>及▱<i>ABCD</i>.\n<br>故选<i>B</i>",
            "notice": "将几何图形按顺序或大小分类，就能做到不重不漏.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，▱<i>ABCD</i>中.\n<i>EF</i> ∥ <i>GH</i> ∥ <i>BC</i>，<i>MN</i> ∥ <i>AB</i>，则图中平行四边形的个数是（    ）<img src=\"4.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "13",
                "14",
                "15",
                "18"
            ],
            "way": "根据平行四边形的定义：两组对边分别平行的四边形是平行四边形",
            "step": "如图，则图中的四边形<i>AEOM</i>，<i>AGPM</i>，<i>ABNM</i>，<i>EGPO</i>，<i>EBNO</i>，<i>GBNP</i>，<i>MOFD</i>，<i>MPHD</i>，<i>MNCD</i>，<i>OPHF</i>，<i>ONCF</i>，<i>PNCH</i>，<i>AEFD</i>，<i>AGHD</i>，<i>ABCD</i>，<i>EGHF</i>，<i>EBCF</i>和<i>GBCH</i>都是平行四边形，共18个.\n",
            "notice": "几何计数题，将几何图形按顺序或大小分类，就能做到不重不漏.\n"
        },
        {
            "kind": "kpt",
            "name": "平行四边形的性质",
            "sub": [
                "<img src=\"25.png\" style=\"width:10em;vertical-align:middle;\">",
                {
                    "kind": "kpt",
                    "name": "边的性质",
                    "sub": [
                        "平行四边形两组对边分别平行且相等；<br><i>AB</i> ∥ <i>CD</i>，<i>AD</i> ∥ <i>BC</i>，<i>AB</i> = <i>CD</i>，<i>AD</i> = <i>BC</i>，"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "角的性质",
                    "sub": [
                        "平行四边形两组对角相等，邻角互补；<br><i>∠A = ∠C</i>，<i>∠B = ∠D</i>，<i>∠A + ∠B = 180°</i>，<i>∠B + ∠C = 180°</i>.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "对角线",
                    "sub": [
                        "对角线互相平分.\n<br><i>OA = OC</i>，<i>OB = OD</i>，"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 利用平行四边形的性质求线段长",
            "type": 1,
            "Q": "如图，在▱<i>ABCD</i>中，已知<i>AD</i> = 12<i>cm</i>，<i>AB</i> = 8<i>cm</i>，<i>AE</i>平分<i>∠BAD</i>交<i>BC</i>边于点 <i>E</i>，则<i>CE</i> 的长等于（      ）<img src=\"5.png\" style=\"float:right;width:10em\">",
            "opts": [
                "8cm",
                "6cm",
                "4cm",
                "2cm"
            ],
            "way": "利用平行四边形对边相等可知<i>BC</i> = <i>AD</i> ，对边平行的性质即有内错角相等结合平分线可得<i>BE</i> = <i>AB</i> ，再求解即可.\n",
            "step": "∵四边形<i>ABCD</i> 是平行四边形<br>∴<i>BC</i> ∥ <i>AD</i>且<i>BC</i> = <i>AD</i> = <i>12cm</i><br>∴∠<i>BEA</i> = ∠<i>DAE</i><br>又∵<i>AE</i>平分∠<i>BAD</i><br>∴∠<i>BEA</i> = ∠<i>DAE</i> = ∠<i>BAE</i><br>∴<i>BE</i> = <i>AB</i> = 8<i>cm</i>（等角对等边）<br>∴<i>CE</i> = <i>BC</i>－<i>BE</i> = 12－8 = 4<i>cm</i><br>故选<i>C</i>",
            "notice": "综合角平分线的定义，平行线的性质进行解题关键"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，在▱<i>ABCD</i>中，<i>AD = 2AB</i>，<i>CE</i>平分<i>∠BCD</i>交<i>AD</i>边于点<i>E</i>，且<i>AE = 3</i>，则<i>AB</i>的长为（　　）<img src=\"6.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "4",
                "3",
                "<span class=\"mathquill-rendered-math\">\\frac{5}{2}</span>",
                "2"
            ],
            "way": "由平行四边形对边平行的性质可知内错角相等，结合角平分线，可知<i>DE = DC = AB</i>，因为<i>AD = 2AB</i>，即可建立<i>AD</i>，<i>AE</i>，<i>DE</i>的关系，求解即可.\n",
            "step": "∵四边形 <i>ABCD</i>是平行四边形<br>∴ <i>BC = AD</i>，<i>DC = AB</i>（平行四边形对边相等）<br>又∵ <i>CE</i>平分 <i>∠BCD</i><br>∴ <i>∠DCE = ∠BCE = ∠DEC</i>（角平分线的定义与两直线平行内错角相等）<br> ∴<i>DE = CD = AB = <span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>AD = AE = 3</i><br>故选<i>B</i>"
        },
        {
            "kind": "example",
            "label": "题型二 利用平行四边形的性质求角度",
            "type": 1,
            "Q": "已知▱<i>ABCD</i>中，<i>∠A + ∠C = 200°</i>，则<i>∠B</i>的度数是（　　）",
            "opts": [
                "100°",
                "160°",
                "80°",
                "60°"
            ],
            "way": "结合平行四边形对角及邻角的性质求解即可.\n",
            "step": "∵四边形<i>ABCD</i> 是平行四边形<br>∴ <i>∠A = ∠C</i>（平行四边形对角相等）<br>又∵ <i>∠A + ∠C = 200°</i><br>∴ <i>∠A = ∠C = 100°</i><br>∴  <i>∠B = 180°－100° = 80°</i>（平行四边形的邻角互补）<br>故选<i>C</i>"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "已知▱<i>ABCD</i>中，<i>∠B = 4∠A</i>，则<i>∠C = </i>（　　）",
            "opts": [
                "18°",
                "36°",
                "72°",
                "144°"
            ],
            "way": "结合平行四边形对角及邻角的性质求解即可.\n",
            "step": "∵四边形 <i>ABCD</i>是平行四边形<br>∴ <i>∠B + ∠A = 180°</i><br>设<i>∠A = x</i>，则<i>∠B = 4x</i><br>即：<i>x + 4x = 180°</i><br>∴<i>∠A = x = 36°</i><br><i>∠C = ∠A = 36°</i><br>故选<i>B</i>"
        },
        {
            "kind": "example",
            "label": "题型三 平行四边形的性质与周长相关问题",
            "type": 1,
            "Q": "已知▱<i>ABCD</i>的周长为 32，<i>AB</i> = 4，则<i>BC</i>等于（　　）",
            "opts": [
                "4",
                "12",
                "24",
                "28"
            ],
            "way": "由于平行四边形的两组对边分别相等，可知平行四边形的周长为一组邻边的和的 2 倍，再结合已知条件，求解即可.\n",
            "step": "∵四边形<i>ABCD</i>的周长为32<br>∴<span class=\"mathquill-rendered-math\">AB + BC = \\frac{1}{2} × 32 = 16</span><br>又∵ <i>AB</i> = 4<br>∴<i>BC</i> = 16－4 = 12<br>故选<i>B</i>"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "平行四边形的周长为<i>24cm</i> ，相邻两边的差为 <i>2cm</i>，则平行四边形的各边长为（      ）",
            "opts": [
                "<i>4cm，4cm，8cm，8cm</i>",
                "<i>5cm，5cm，7cm，7cm</i>",
                "<i>5.5cm，5.5cm，6.5cm，6.5cm</i>",
                "<i>3cm，3cm，9cm，9cm</i>"
            ],
            "way": "由于平行四边形的两组对边分别相等，可知平行四边形的周长为一组邻边的和的 2 倍，再结合已知条件，求解即可.\n",
            "step": "设▱<i>ABCD</i>的一组邻边长分别为<i>x</i>和<i>x－2</i><br>∴由题意知：<span class=\"mathquill-rendered-math\">x + \\left（ x－2 \\right） = \\frac{1}{2} × 24 = 12</span><br>解得：<i>x = 7</i>，<i>x－2 = 5</i><br>∴此平行四边形的一组邻边长分别为5和7<br>故选<i>B</i>"
        },
        {
            "kind": "example",
            "label": "题型四  利用平行四边形的对角线性质求线段的取值范围",
            "type": 1,
            "Q": "如图，▱<i>ABCD</i>的对角线<i>AC</i>，<i>BD</i>相交于点<i>O</i>，且<i>AC = 8</i>，<i>BD = 6</i>，则边长 <i>AB</i>的取值范围是（　　）<img src=\"7.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "<i>1 &gt; AB &gt; 7</i>",
                "<i>2 &gt; AB &gt; 14</i>",
                "<i>6 &gt; AB &gt; 8</i>",
                "<i>3 &gt; AB &gt; 4</i>"
            ],
            "way": "由平行四边形的对角线性质对角线互相平分，可得<i>OA</i>和<i>OB</i>的值，再根据三角形三边关系可求得<i>AB</i>的取值范围.\n",
            "step": "∵▱<span class=\"mathquill-rendered-math\">ABCD</span>的对角线<span class=\"mathquill-rendered-math\">AC</span>，<span class=\"mathquill-rendered-math\">BD</span>相交于<span class=\"mathquill-rendered-math\">O</span>，<br>且<span class=\"mathquill-rendered-math\">AC = 8</span>，<span class=\"mathquill-rendered-math\">BC = 6</span>，<br>∴<span class=\"mathquill-rendered-math\">OA = \\frac{1}{2}AC = 4</span>，<span class=\"mathquill-rendered-math\">OB = \\frac{1}{2}BD = 3</span><br>∴<span class=\"mathquill-rendered-math\">4－3 &lt; AB &lt; 4 + 3</span><br>即：<span class=\"mathquill-rendered-math\">1 &lt; AB &lt; 7</span><br>故选<i>A</i>"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "已知平行四边形的一条边长为14 ，下列各组数中能分别作为它的两条对角线长的是（　　）<img src=\"8.png\" style=\"float:right;width:10em\">",
            "opts": [
                "10与16",
                "12与16",
                "20与22",
                "10与40"
            ],
            "way": "已知平行四边形的一条边求对角线的取值范围时，则需要用到验证法，分别令对角线为选项中的值，求出边长范围进行验证即可.\n",
            "step": "A选项中，对角线为10 与 16，则可求出边长<i>n</i> 范围是<i>3 &gt; n &gt; 13</i>；<br>B选项中，对角线为 12与16，则可求出边长<i>n</i> 范围是<i>2 &gt; n &gt; 14</i>；<br>C选项中，对角线为20与22，则可求出边长<i>n</i> 范围是<i>1 &gt; n &gt; 22</i>；<br>D选项中，对角线为10与40，则可求出边长<i>n</i>范围是<i>15 &gt; n &gt; 25</i>.\n<br>∵已知<i>n = 14</i><br>故选<i>C</i>"
        },
        {
            "kind": "kpt",
            "name": "两平行线之间的距离",
            "sub": [
                "<img src=\"9.png\" style=\"width:10em;vertical-align:middle;\">",
                {
                    "kind": "kpt",
                    "name": "定义",
                    "sub": [
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;两条平行线中，一条直线上的任意一点到另一条直线的距离，叫做这两条平行线间的距离.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "性质",
                    "sub": [
                        "①两条平行线之间的距离处处相等.\n<br>②两条平行线间的任何两条平行线段都是相等的.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 平行线间的距离的概念性辨析和求解",
            "type": 1,
            "Q": "如图，<i>a</i> ∥ <i>b</i>，<i>AB</i> ∥ <i>CD</i>，<i>CE</i> ⊥ <i>b</i>，<i>FG</i> ⊥ <i>b</i>，<i>E</i>，<i>G</i>为垂足，则下列说法不正确的是（　　）<img src=\"10.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "<i>AB = CD</i>",
                "<i>EC = FG</i>",
                "<i>A</i>，<i>B</i>两点间的距离就是线段<i>AB</i>的长度",
                "<i>a</i>与<i>b</i>的距离就是线段<i>CD</i>的长度"
            ],
            "way": "根据两条平行线之间的距离的定义判定即可.\n",
            "step": "<table border=\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><th>选项</th><th>依据</th><th>结论</th></tr><tr><td align=\"center\">A</td><td>平行四边形对边相等</td><td align=\"center\">正确</td></tr><tr><td align=\"center\">B</td><td>平行线间的距离处处相等</td><td align=\"center\">正确</td></tr><tr><td align=\"center\">C</td><td>两点间的距离的定义</td><td align=\"center\">正确</td></tr><tr><td align=\"center\">D</td><td>两平行线间的距离的定义</td><td align=\"center\">错误</td></tr></table>",
            "notice": "应注意点与点，点与直线，平行线之间的距离的区别与联系.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "     已知直线<i>a</i> ∥ <i>b</i>，点<i>M</i>到直线<i>a</i>的距离是<i>6cm</i>，到直线<i>b</i>的距离是<i>3cm</i>，那么直线<i>a</i>和直线<i>b</i>之间的距离是（　　）",
            "opts": [
                "<i>3cm</i>",
                "<i>9cm</i>",
                "<i>3cm</i>或<i>9cm</i>",
                "<i>6cm</i>"
            ],
            "way": "    点到直线的距离是指点到直线的垂线段的长度，结合不同两组点到直线的距离为平行线的距离求解即可.\n",
            "step": "<img src=\"12.png\" style=\"float:right;width:10em;vertical-align:middle;\"><img src=\"11.png\" style=\"float:right;width:10em;vertical-align:middle;\">当点<i>M</i>在<i>a</i>，<i>b</i>之间时，如图一<br><i>a</i>，<i>b</i>之间的距离为<i>6cm + 3cm = 9cm</i><br>当点<i>M</i>在<i>a</i>，<i>b</i>之外时，如图二<br><i>a</i>，<i>b</i>之间的距离为<i>6cm－3cm = 3cm</i><br>故选<i>C</i>",
            "notice": "当题意未指明具体情况时应注意分类讨论，防止漏解.\n"
        },
        {
            "kind": "example",
            "label": "题型二 平行线间的距离的应用－－－面积法",
            "type": 9,
            "Q": "如图，已知直线<i>a</i> ∥ <i>b</i>，点<i>C</i>，<i>D</i>在直线<i>a</i>上，点<i>A</i>，<i>B</i>在直线b上，线段<i>BC</i>，<i>AD</i>相交于点<i>E</i>，写出图中面积相等的所有三角形 ：____.\n<img src=\"13.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "way": "    由平行线间的距离处处相等可知同底等高的三角形面积相等，求解即可.\n",
            "step": "∵<i>a</i> ∥ <i>b</i>，且点<i>C</i>，<i>D</i>在直线<i>a</i>上，点<i>A</i>，<i>B</i>在直线<i>b</i>上<br>∴△<i>CAD</i>和△<i>CBD</i>同底等高，△<i>ACB</i>和△<i>ADB</i>同底等高<br>∴<i>S</i><sub class=\"tutor_sub\">△<i>CAD</i></sub> = <i>S</i><sub class=\"tutor_sub\">△<i>CBD</i></sub>，<i>S</i><sub class=\"tutor_sub\">△<i>ACB</i></sub> = <i>S</i><sub class=\"tutor_sub\">△<i>ADB</i></sub><br>故答案为：△<i>CAD</i>和△<i>CBD</i>，△<i>ACB</i>和△<i>ADB</i>"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "已知▱<i>ABCD</i>中，<i>F</i>为<i>BC</i>上一点，<i>BF：FC = 1：2</i>，则△<i>ABF</i>与△<i>ADC</i>的面积比是_____ .\n<img src=\"14.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "way": "    由平行线间的距离处处相等可知高相等的三角形面积比等于底边比，结合等底等高的三角形面积相等，求解即可.\n",
            "step": "∵四边形<i>ABCD</i>是平行四边形<br>∴<i>AD</i> ∥ <i> BC</i> 且 <i>AD</i> = <i>BC</i>（平行四边形对边平行，对边相等）<br>∴<i>S</i><sub class=\"tutor_sub\">△<i>ADC</i></sub> = <i>S</i><sub class=\"tutor_sub\">△<i>ABC</i></sub>（等底等高的三角形面积相等）<br>∵<i>BF</i> ：<i> FC</i> = 1 ：2<br>∴<i>S</i><sub class=\"tutor_sub\">△<i>ABF</i></sub> ：<i>S</i><sub class=\"tutor_sub\">△<i>AFC</i></sub> = 1 ：2（等高的三角形面积之比等于底边比）<br>∴<i>S</i><sub class=\"tutor_sub\">△<i>ABF</i></sub> ：<i>S</i><sub class=\"tutor_sub\">△<i>ABC</i></sub> = 1 ：3<br>即：<i>S</i><sub class=\"tutor_sub\">△<i>ABF</i></sub> ：<i>S</i><sub class=\"tutor_sub\">△<i>ADC</i></sub> = 1 ：3（等量代换）<br>故答案为： 1：3"
        },
        {
            "kind": "kpt",
            "name": "平行四边形的判定方法",
            "sub": [
                "",
                {
                    "kind": "kpt",
                    "name": "按边判定",
                    "sub": [
                        "（1）两组对边分别平行的四边形是平行四边形；<br>（2）两组对边分别相等的四边形是平行四边形；<br>（3）一组对边平行且相等的四边形是平行四边形.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "按角判定",
                    "sub": [
                        "　　    两组对角分别相等的四边形的是平行四边形.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "按对角线判定",
                    "sub": [
                        "　　    对角线互相平分的四边形是平行四边形"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 选择条件判定一个四边形是平行四边形",
            "type": 1,
            "Q": "能够判定一个四边形是平行四边形的条件是（　　）",
            "opts": [
                "一组对角相等",
                "两条对角线互相平分",
                "两条对角线互相垂直",
                "一对邻角的和为<i>180°</i>"
            ],
            "way": "根据平行四边形的判定定理判定即可",
            "step": "    根据平行四边形的判定定理可知，需两组对角分别相等，故A、D错误；两条对角线互相平分，故C错误<br>故选<i>B</i>"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，四边形<i>ABCD</i>中，<i>AD</i> ∥ <i>BC</i>，要判别四边形<i>ABCD</i>是平行四边形，还需满足条件（　　）<img src=\"15.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "<i>∠A + ∠C = 180°</i>",
                "<i>∠B + ∠D = 180°</i>",
                "<i>∠A + ∠B = 180°</i>",
                "<i>∠A + ∠D = 180°</i>"
            ],
            "way": "    根据已知条件四边形<i>ABCD</i>中，<i>AD</i> ∥ <i>BC</i>，可知已经有一组对边平行，则需要另一组对边平行，或同一组对边相等的条件即可，通过分析选项可得解.\n",
            "step": "∵四边形<i>ABCD</i>中，<i>AD</i> ∥ <i>BC</i><br>若<i>∠A + ∠D = 180°</i>，则有<i>AB</i> ∥ <i>DC</i><br>则四边形<i>ABCD</i>是平行四边形（两组对边分别平行的四边形是平行四边形）<br>故选D"
        },
        {
            "kind": "example",
            "label": "题型二 根据平行四边形的判定方法识别平行四边形并计数",
            "type": 9,
            "Q": "如图，<i>D</i>，<i>E</i>，<i>F</i>分别在△<i>ABC</i>的三边<i>BC</i>，<i>AC</i>，<i>AB</i>上，且<i>DE</i> ∥ <i>AB</i>，<i>DF</i> ∥ <i>AC</i>，<i>EF</i> ∥ <i>BC</i>，则图中共有_____个平行四边形，分别是_____.\n<img src=\"16.png\" style=\"float:right;width:10em\">",
            "way": "根据平行四边形的判定定理进行识别即可.\n",
            "step": "∵<i>DE</i> ∥ <i>AB</i>，<i>DF</i> ∥ <i>AC</i>，<i>EF</i> ∥ <i>BC</i>，根据两组对边相等的四边形是平行四边形判断<br>∴四边形<i>AFDE</i>，<i>EFBD</i>，<i>FDCE</i>都是平行四边形.\n<br>故答案为：3个，分别为：平行四边形<i>AFDE</i>，平行四边形<i>EFBD</i>，平行四边形<i>FDCE</i>"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，▱<i>ABCD</i>中，<i>E</i>，<i>F</i>分别为边<i>AB</i>，<i>DC</i>的中点，则图中共有平行四边形的个数是（　　）<img src=\"17.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "opts": [
                "3",
                "4",
                "5",
                "6"
            ],
            "way": "根据已知条件<i>ABCD</i>是平行四边形，可以知：<i>AB</i> ∥ <i>CD</i>且<i>AB</i> = CD</i>，<i>AD</i> ∥ <i>BC</i>且<i>AD</i> = BC</i>，根据线段的中点，即可得<i>DF = FC = AE = EB</i>，再根据平行四边形的判定定理进行识别即可.\n",
            "step": "∵四边形<i>ABCD</i>是平行四边形<br>∴<i>AB</i> ∥ <i>CD</i>且<i>AB = CD</i>，<i>AD</i> ∥ <i>BC</i>且<i>AD = BC</i><br>∵<i>E</i> ，<i>F</i>分别为边<i>AB</i>，<i>DC</i>的中点<br>∴<i>DF = FC = AE = EB</i><br>∴四边形<i>ADFE</i>，<i>EFCB</i>，<i>DEBF</i>都是平行四边形<br>∵四边形<i>ABCD</i>是平行四边形<br>所以图中共有5个平行四边形.\n"
        },
        {
            "kind": "example",
            "label": "题型三  平行四边形的判定",
            "type": 9,
            "Q": "如图，在▱<i>ABCD</i>中，点<i>E</i>，<i>F</i>是对角线<i>AC</i>上的两点，且<i>AE = CF</i>.\n<img src=\"18.png\" style=\"float:right;width:10em;vertical-align:middle;\"><br>求证：<i>∠EBF = ∠FDE</i>",
            "way": "    已知四边形<i>ABCD</i>是平行四边形，则它的对角线互相平分，结合线段相等，即可利用对角线判定方法判定.\n",
            "step": "<img src=\"19.png\" style=\"float:right;width:10em;vertical-align:middle;\">连接<i>BD</i>，与<i>AC</i>相交于点<i>O</i><br>∵四边形<i>ABCD</i>是平行四边形<br>∴ <i>OB = OD</i>，<i>OA = OC</i>（平行四边形对角线互相平分）<br>∵点 <i>E</i>，<i>F</i>是对角线<i>AC</i>上的两点，<br>且 <i>AE = CF</i><br>∴<i>OA－AE = OC－CF</i><br>即：<i>OE = OF</i><br>∴四边形<i>BEDF</i>是平行四边形<br>∴<i>∠EBF = ∠FDE</i>（平行四边形对角相等）"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "<img src=\"20.png\" style=\"float:right;width:10em;vertical-align:middle;\">如图，已知<i>AB</i>，<i>CD</i>相交于点<i>O</i>，<i>AC</i> ∥ <i>BD</i>，<i>OA = OB</i>，<i>E</i>，<i>F</i>分别是<i>OC</i>，<i>OD</i>的中点，求证：四边形<i>AEBF</i>是平行四边形.\n",
            "way": "    因为<i>AC</i> ∥ <i>BD</i>，可得一组内错角相等，结合三角形全等，得到线段关系，再利用平行四边形的判定定理判定即可.\n",
            "step": "证明：<br>∵<i>AB</i>，<i>CD</i>相交于点<i>O</i>，<i>AC</i> ∥ <i>BD</i><br>∴<i>∠C = ∠D</i><br>在△<i>AOC</i>和△<i>BOD</i>中，<br><img src=\"1.svg\" style=\"vertical-align:middle;width:8em\"><br>∴△<i>AOC</i><span class=\"mathquill-rendered-math\">≌</span>△<i>BOD</i>（AAS）<br>∴<i>OC = OD</i><br>∵<i>E</i>，<i>F</i>分别是<i>OC</i>，<i>OD</i>的中点<br>∴ <i>OE = OF</i><br>∴四边形<i>AEBF</i>是平行四边形（对角线互相平分的四边形是平行四边形）",
            "notice": "    在证明平行四边形的方法中，利用三角形全等知识证明线段与角度之间的关系往往是解题关键.\n"
        },
        {
            "kind": "kpt",
            "name": "三角形的中位线及其定理",
            "sub": [
                "<img src=\"21.png\" style=\"width:10em;vertical-align:middle;\">",
                {
                    "kind": "kpt",
                    "name": "定义",
                    "sub": [
                        "连接三角形两边中点的线段.\n（任意一个三角形都有三条中位线）"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "三角形的中位线定理",
                    "sub": [
                        "三角形的中位线平行于三角形的第三边，并且等于第三边的一半.\n<br>数学表达为：<br>∵ <i>D</i>，<i>E</i>分别是<i>AB</i>，<i>AC</i>的中点，<br>∴<i>DE</i> ∥ <i>BC</i>，<span class=\"mathquill-rendered-math\">DE = \\frac{1}{2}BC</span>.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一 利用三角形的中位线进行证明",
            "type": 9,
            "Q": "如图所示，在△<i>ABC</i>中，点<i>D</i>在<i>BC</i>上，且<i>DC = AC</i>，<i>CE</i> ⊥ <i>AD</i>于点<i>E</i>，点<i>F</i>是<i>AB</i>的中点.\n<br>求证：<i>EF</i> ∥ <i>BC</i>.\n<img src=\"22.png\" style=\"float:right;width:10em;vertical-align:middle;\">",
            "way": "欲证<i>EF</i> ∥ <i>BC</i>，只要证<i>EF</i>为△<i>ABD</i> 的中位线，结合条件证明点<i>E</i>是<i>AD</i>的中点即可.\n",
            "step": "证明：<br>∵ <i>DC = AC</i>，且<i>CE</i> ⊥ <i>AD</i>于点<i>E</i><br>∴<i>AE = DE</i><br>又∵点<i>F</i>是<i>AB</i>的中点，<br>∴<i>EF</i>是△<i>ABD</i>的中位线<br>∴ <i>EF</i> ∥ <i>BC</i>",
            "notice": "    利用三角形的中位线不仅可以证明直线平行，也可以证明线段的倍分关系.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "已知，如图所示，<i>E</i>，<i>F</i>，<i>G</i>，<i>H</i>分别是四边形<i>ABCD</i>各边的中点，连接<i>EF</i>，<i>FG</i>，<i>GH</i>，<i>HE</i>.\n<img src=\"23.png\" style=\"float:right;width:10em;vertical-align:middle;\"><br>求证：四边形<i>EFGH</i>是平行四边形.\n",
            "way": "    添加辅助线，构造三角形的中位线，再结合平行四边形的判定方法判定即可.\n",
            "step": "<img src=\"24.png\" style=\"float:right;width:10em;vertical-align:middle;\">证明：连接<i>AC</i><br>∵点<i>E</i>，<i>H</i>分别为<i>AD</i>，<i>CD</i>的中点<br>∴<i>EH</i> ∥ <i>AC</i>，<i>EH</i> = </i><span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AC</i><br>又∵点<i>F</i>，<i>G</i>分别为<i>AB</i>，<i>BC</i>的中点，<br>∴ <i>FG</i> ∥ <i>AC</i>，<i>FG</i> = </i><span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AC</i><br>∴<i>EH</i> ∥ <i>FG</i>且<i>EH = FG</i><br>四边形<i>EFGH</i>是平行四边形.\n",
            "notice": "    若一个题的已知条件中的中点比较多，要充分利用“三角形的中位线”，作辅助线构造中位线，也是解题关键.\n"
        }
    ]
}