{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第1讲　全等三角形",
            "description": "1.\n了解全等形的定义，会判断两个图形是不是全等形<br/>2.\n理解全等三角形的概念，学会判断对应元素的方法.\n<br/>3.\n掌握全等三角形的性质，能利用全等三角形的性质解决相关的证明和计算问题.\n"
        },
        {
            "kind": "kpt",
            "name": "全等形的概念",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "定义：",
                    "sub": [
                        "能够<b>完全重合</b>的两个图形叫做全等形.\n<br><b>注：</b>（1）图形的全等与它们的位置无关，只要满足能够完全重合即可.\n完全重合包含两层含义：图形的形状相同、大小相等；<br>（2）全等形的周长、面积分别相等，但周长或面积相等的两个图形不一定是全等形.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "几种常用的全等变换方式",
                    "sub": [
                        "平移、翻折、旋转.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用全等形的定义识别全等形",
            "type": 1,
            "Q": "如图，请仔细观察 <i>A</i>、<i>B</i>、<i>C</i>、<i>D</i> 四个答案，其中与下方图案完全相同的是（　　）<br/><img style=\"width:4em;vertical-align:middle;padding:5px;\" src=\"1.png\">",
            "way": "能够完全重合的两个图形叫做全等形，将选项中的图形绕正六边形的中心旋转，与题干的图形完全相同的即为所求.\n",
            "step": "观察图形可知，<br>只有选项 <i>C</i> 中的图形旋转后与图中的正六边形完全相同.\n<br>故选：<i>C</i>.\n",
            "notice": "1.\n此题运用定义识别全等形确定两个图形全等要符合两个条件：①形状相同，②大小相等；是否是全等形与位置无关；<br/>2.\n判断两个图形是否为全等形还可以通过平移、旋转、翻折等方法把两个图形叠合在一起，看它能否完全重合，即用<b>叠合法</b>判断.\n",
            "A": "",
            "opts": [
                "<img style=\"width:4em;vertical-align:middle;\" src=\"2.png\">",
                "<img style=\"width:4em;vertical-align:middle;\" src=\"3.png\">",
                "<img style=\"width:4em;vertical-align:middle;\" src=\"4.png\">",
                "<img style=\"width:4em;vertical-align:middle;\" src=\"5.png\">"
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列图形中，属于全等形的是（　　）",
            "way": "",
            "step": "",
            "notice": "",
            "A": "B",
            "opts": [
                "<img style=\"width:3.81em;vertical-align:middle;\" src=\"6.png\">",
                "<img style=\"width:3.00em;vertical-align:middle;\" src=\"7.png\">",
                "<img style=\"width:4.44em;vertical-align:middle;\" src=\"8.png\">",
                "<img style=\"width:4.19em;vertical-align:middle;\" src=\"9.png\">"
            ]
        },
        {
            "kind": "kpt",
            "name": "全等三角形",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "全等三角形的定义",
                    "sub": [
                        "能够完全重合的两个三角形叫做全等三角形.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "全等三角形对应元素",
                    "sub": [
                        "把两个全等的三角形重合到一起：（1）<b>对应顶点：</b>重合的顶点；（2）<b>对应边：</b>重合的边；（3）<b>对应角：</b>重合的角.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "全等三角形的表示法",
                    "sub": [
                        "如图 Δ<i>ABC</i> 和 Δ<i>DEF</i> 全等，记作 Δ<i>ABC</i> ≌ Δ<i>DEF</i>，符号“≌”读作全等于.\n其中“∽”表示形状相同，“ = ”表示大小相等.\n<br><img style=\"width:13.75em;vertical-align:middle;\" src=\"11.png\">"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "对应元素的确定方法",
                    "sub": [
                        "<b>字母顺序确定法：</b>根据书写规范，按照对应顶点确定对应边、对应角，如 Δ<i>CAB</i> ≌ Δ<i>FDE</i>，则 <i>AB</i> 与 <i>DE</i>、<i>AC</i> 与 <i>DF</i>、<i>BC</i> 与 <i>EF</i> 是对应边，∠<i>A</i> 和∠<i>D</i>、∠<i>B</i> 和∠<i>E</i>、∠<i>C</i> 和∠<i>F</i> 是对应角.\n<br><b>注：</b>记两个三角形全等时，通常把表示对应顶点的字母写在对应的位置上，字母顺序不能随意书写.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　找对应元素",
            "type": 9,
            "Q": "已知 Δ<i>ABC</i> ≌ Δ<i>EDC</i>，指出其对应边和对应角.\n",
            "way": "用“≌”表示两个三角形全等时，对应顶点的字母写在对应的位置上，先把两个三角形顶点处的字母按照同样的顺序排成一排：<i>A</i>→<i>B</i>→<i>C</i>，<i>E</i>→<i>D</i>→<i>C</i>，然后按照同样的顺序写出对应元素.\n",
            "step": "<i>AB</i> 与 <i>ED</i>，<i>AC</i> 与 <i>EC</i>，<i>BC</i> 与 <i>DC</i> 是对应边；∠<i>A</i> 与∠<i>E</i>，∠<i>B</i> 与∠<i>D</i>，∠<i>ACB</i> 与∠<i>ECD</i> 是对应角.\n",
            "notice": "表示三角形全等时，对应顶点的字母写在对应的位置上，所以不看图形就能说出对应角和对应边.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，已知 △<i>ABD</i> ≌ △<i>CDB</i>，∠<i>ABD</i> = ∠<i>CDB</i>，写出其余的对应边和对应角.\n<br/><img style=\"width:11.19em;vertical-align:middle;\" src=\"12.png\">",
            "way": "在 Δ<i>ABD</i> 和 Δ<i>CDB</i> 中，∠<i>ABD</i> = ∠<i>CDB</i>，则∠<i>ABD</i> 与∠<i>CDB</i> 是对应角，它们所对的边 <i>AD</i> 与 <i>CB</i> 是对应边，公共边 <i>BD</i> 与 <i>DB</i> 是对应边，余下的一对边 <i>AB</i> 与 <i>CD</i> 是对应边，由对应边所对的角是对应角可确定其他两组对应角.\n",
            "step": "<i>BD</i> 与 <i>DB</i>，<i>AD</i> 与 <i>CB</i>，<i>AB</i> 与 <i>CD</i> 分别是对应边；∠<i>A</i> 与∠<i>C</i>，∠<i>ADB</i> 与∠<i>CBD</i> 分别是其余的对应角.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型二　利用图形的变换方式找对应元素",
            "type": 9,
            "Q": "△<i>ABC</i> 绕其顶点 <i>C</i> 顺时针旋转得到 △<i>DEC</i>，则 △<i>ABC</i>________△<i>DEC</i>，其中对应顶点分别是________、________、________；对应边分别是________、________、________，对应角分别是________、________、________.\n<br/><img style=\"width:15.00em;vertical-align:middle;\" src=\"13.png\">",
            "way": "将 Δ<i>ABC</i> 绕其顶点 <i>C</i> 旋转得到 Δ<i>DCE</i>，只改变了图形的位置，而没有改变形状和大小，故 Δ<i>ABC</i> 与 Δ<i>DCE</i> 全等，进而可写出对应边与对应角.\n",
            "step": "△<i>ABC</i> 绕其顶点 <i>C</i> 顺时针旋转得到 △<i>DEC</i>，则 △<i>ABC</i> ≌ △<i>DEC</i>，<br/>所以，对应顶点为：<i>A</i> 与 <i>D</i>，<i>B</i> 与 <i>E</i>，<i>C</i> 与 <i>C</i>；<br/>对应边为：<i>AB</i> = <i>DE</i>，<i>AC</i> = <i>DC</i>，<i>BC</i> = <i>EC</i>；<br/>对应角为：∠<i>A</i> = ∠<i>D</i>，∠<i>B</i> = ∠<i>DEC</i>，∠<i>ACB</i> = ∠<i>DCE</i>.\n<br/>故答案为：≌，<i>A</i> 与 <i>D</i>，<i>B</i> 与 <i>E</i>，<i>C</i> 与 <i>C</i>；<i>AB</i> = <i>DE</i>，<i>AC</i> = <i>DC</i>，<i>BC</i> = <i>EC</i>；∠<i>A</i> = ∠<i>D</i>，∠<i>B</i> = ∠<i>DEC</i>，∠<i>ACB</i> = ∠<i>DCE</i>.\n",
            "notice": "旋转变换前后对应位置的边是对应边，对应位置的角是对应角.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，若把 △<i>ABC</i> 绕 <i>A</i> 点顺时针旋转一定角度，就得到 △<i>ADE</i>，请写出图中所有的对应边是________，对应角是________.\n<br/><img style=\"width:11.00em;vertical-align:middle;\" src=\"14.png\">",
            "way": "",
            "step": "∵△<i>ABC</i> 绕 <i>A</i> 点顺时针旋转一定角度，就得到 △<i>ADE</i>，<br>\n∴△<i>ABC</i> ≌ △<i>ADE</i>，<br>\n∴ 对应边为：<i>AB</i> 和 <i>AD</i>，<i>AC</i> 和 <i>AE</i>，<i>BC</i> 和 <i>DE</i>，对应角为：∠<i>BAC</i> 和∠<i>EAD</i>，∠<i>B</i> 和∠<i>D</i>，∠<i>C</i> 和∠<i>E</i>，<br>\n故答案为：<i>AB</i> 和 <i>AD</i>，<i>AC</i> 和 <i>AE</i>，<i>BC</i> 和 <i>DE</i>；∠<i>BAC</i> 和∠<i>DAE</i>，∠<i>B</i> 和∠<i>D</i>，∠<i>C</i> 和∠<i>E</i>.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "kpt",
            "name": "全等三角形的性质",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "全等三角形的性质",
                    "sub": [
                        "对应边<b>相等</b>，对应角<b>相等</b>.\n<br><b>拓展：</b>（1）全等三角形的对应元素相等.\n其中，对应元素包括：对应边、对应角、对应中线、对应高、对应角平分线、对应周长、对应面积等.\n<br>（2）在应用全等三角形性质时，要先确定两个条件：<br>　①两个三角形全等；<br>　②找对应元素.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "易错警示",
                    "sub": [
                        "周长相等的两个三角形不一定全等，面积相等的两个三角形也不一定全等.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用全等三角形的性质求线段长",
            "type": 9,
            "Q": "如图，已知点 <i>A</i>，<i>D</i>，<i>B</i>，<i>F</i> 在同一条直线上，Δ<i>ABC</i> ≌ Δ<i>FDE</i>，<i>AB</i> = 8<i>cm</i>，<i>BD</i> = 6<i>cm</i>，求 <i>FB</i> 的长.\n<br/><img style=\"width:7.13em;vertical-align:middle;\" src=\"15.png\">",
            "way": "由全等三角形的性质可知 <i>AB</i> = <i>FD</i>，由等式的性质可得 <i>AD</i> = <i>FB</i>，所以要求 <i>FB</i> 的长，只需求 <i>AD</i> 的长.\n",
            "step": "Δ<i>ABC</i> ≌ Δ<i>FDE</i>，∴ <i>AB</i> = <i>FD</i>.\n<br/>∴ <i>AB</i>－<i>DB</i> = <i>FD</i>－<i>DB</i>，即 <i>AD</i> = <i>FB</i>.\n<br/>∵ <i>AB</i> = 8<i>cm</i>，<i>BD</i> = 6<i>cm</i>，∴ <i>AD</i> = <i>AB</i>－<i>DB</i> = 8－6 = 2(<i>cm</i>).\n<br/>∴ <i>FB</i> = <i>AD</i> = 2<i>cm</i>.\n",
            "notice": "（1）全等三角形的性质在几何证明和计算中起着重要作用，当所求线段不是全等三角形的边时，可利用等式的性质进行转换，从而找到所求线段与已知线段的关系.\n<br>（2）本题通过全等三角形的性质可把线段 <i>AB</i> <b>转化</b>成线 <i>DF</i> 再利用等式的性质可把求线段 <i>FB</i> 的长<b>转化</b>成求线段 <i>AD</i> 的长.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如果 △<i>ABC</i> ≌ △<i>DEF</i>，且 △<i>ABC</i> 的周长为100<i>cm</i>，<i>A</i>、<i>B</i> 分别与 <i>D</i>、<i>E</i> 对应，<i>AB</i> = 30<i>cm</i>，<i>DF</i> = 25<i>cm</i>，则 <i>BC</i> 的长为（　　）",
            "way": "",
            "step": "∵△<i>ABC</i> ≌ △<i>DEF</i>，<i>A</i>，<i>B</i>，<i>C</i> 分别与 <i>D</i>，<i>E</i>，<i>F</i> 对应∴ <i>AC</i> = <i>DF</i> = 25<i>cm</i>.\n又∵△<i>ABC</i>的周长是100<i>cm</i><br><i>AB</i> = 30<i>cm</i> ∴ <i>BC</i> = 100－<i>AB</i>－<i>AC</i> = 100－30－25 = 45<i>cm</i> ∴ <i>BC</i> 的长等于45<i>cm</i>.\n<br>故答案为：A.\n",
            "notice": "",
            "A": "",
            "opts": [
                "45<i>cm</i>",
                "55<i>cm</i>",
                "30<i>cm</i>",
                "25<i>cm</i>"
            ]
        },
        {
            "kind": "example",
            "label": "题型二　利用全等三角形的性质求角的度数",
            "type": 9,
            "Q": "如图，已知 <i>Rt</i>△<i>ABC</i> ≌ <i>Rt</i>△<i>CDE</i>，∠<i>B</i> =∠<i>D</i> = 90°，且 <i>B</i>，<i>C</i>，<i>D</i> 三点在一条直线上.\n求∠<i>ACE</i> 的度数.\n<br/><img style=\"width:10.63em;vertical-align:middle;\" src=\"16.png\">",
            "way": "根据 <i>Rt</i>△<i>ABC</i> ≌ <i>Rt</i>△<i>CDE</i> 可得∠<i>BCA</i> =∠<i>DEC</i>，再根据直角三角形两锐角互余可得∠<i>CED</i> +∠<i>ECD</i> = 90°，进而得到∠<i>BCA</i> +∠<i>ECD</i> = 90°，再根据角之间的关系可得∠<i>ACE</i> = 90°.\n",
            "step": "∵ <i>Rt</i>△<i>ABC</i> ≌ <i>Rt</i>△<i>CDE</i>，<br>∴∠<i>BCA</i> =∠<i>DEC</i>，<br>∵△<i>DCE</i> 是直角三角形，<br>∴∠<i>CED</i> +∠<i>ECD</i> = 90°，<br>∴∠<i>BCA</i> +∠<i>ECD</i> = 90°，<br>∴∠<i>ACE</i> = 180°－90° = 90°.\n",
            "notice": "（1）利用全等三角形的性质求角的度数的方法：利用全等三角形的性质先确定两个三角形中角的对应关系，由这种关系实现已知角和未知角之间的转换，从而求出所要求的角的度数.\n<br>（2）本题主要利用了全等角形对应角相等的性质通过全等三角形把属于两个三角形的∠<i>ACB</i>、<br>∠<i>ECD</i> 联系在一起，并将它作为一个<b>整体</b>求出其度数的和.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "（中考真题）如图，△<i>ABC</i> ≌ △<i>A'B'C'</i>，其中∠<i>A</i> = 36°</var>，∠<i>C'</i> = 24°，则∠<i>B</i> = ________.\n<br/><img style=\"width:13.25em;vertical-align:middle;\" src=\"17.png\">",
            "way": "",
            "step": "本题主要考查全等三角形的判定与性质和三角形的基本概念.\n<br/>因为△<i>ABC</i> ≌ △<i>A'B'C'</i>，所以∠<i>C</i> = ∠<i>C'</i> = 24°.\n由三角形内角和等于180°得，∠<i>B</i> = 180° －∠<i>A</i>－∠<i>C</i> = 180°－36°－24° = 120°.\n<br/>故本题正确答案为120°.\n",
            "notice": "",
            "A": ""
        },
        {
            "kind": "example",
            "label": "题型三　利用全等三角形的性质解决图形平移翻折问题",
            "type": 9,
            "Q": "如图所示是重叠的两个直角三角形.\n将其中一个直角三角形沿 <i>BC</i> 方向平移得到 △<i>DEF</i>.\n如果 <i>AB</i> = 8<i>cm</i>，<i>BE</i> = 4<i>cm</i>，<i>DH</i> = 3<i>cm</i>，则图中线段 <i>CF</i> 为________<i>cm</i>，阴影部分面积为________<i>cm</i><sup>2</sup>.\n<br/><img style=\"width:9.50em;vertical-align:middle;padding:5px;\" src=\"18.png\">",
            "way": "先根据平移的性质得出 △<i>ABC</i> ≌ △<i>DEF</i>，则 <i>S</i><sub>△<i>ABC</i></sub> = <i>S</i><sub>△<i>EFD</i></sub>，<i>S</i><sub>阴影</sub> = <i>S</i><sub>梯形<i>ABEH</i></sub>，并且 <i>AB</i> = <i>DE</i>，得出 <i>HE</i> 的长，进而求出 <i>S</i><sub>梯形<i>ABEH</i></sub>.\n",
            "step": "由题意知 △<i>ABC</i> ≌ △<i>DEF</i>，所以 <i>DE = AB</i> = 8<i>cm</i>，<i>S</i><sub>Δ<i>ABC</i></sub> = <i>S</i><sub>Δ<i>DEF</i></sub>.\n<br/>∵ <i>S</i><sub>Δ<i>ABC</i></sub> = <i>S</i><sub>梯形<i>ABEH</i></sub> + <i>S</i><sub>Δ<i>HEC</i></sub>，<i>S</i><sub>Δ<i>DEF</i></sub> = <i>S</i><sub>梯形<i>DHCF</i></sub> + <i>S</i><sub>Δ<i>HEC</i></sub>，<br/>∴ <i>S</i><sub>梯形<i>ABEH</i></sub> = <i>S</i><sub>梯形<i>DHCF</i></sub>.\n<br/>∵ <i>DH + HE = DE</i>.\n<br/>∴ <i>HE = DE－DH</i> = 8－3 = 5<i>cm</i>.\n<br/>∴ <i>S</i><sub>ABEH</sub> = <span class=\"mathquill-rendered-math\">\\frac{1}{2}(AB + HE)\\cdot BE</span><span class=\"mathquill-rendered-math\">=\\frac{1}{2}×(8 + 5)×4</span>= 26<i>cm</i><sup>2</sup>.\n<br/>∴ <i>S</i><sub>梯形<i>DHCF</i></sub> = 26<i>cm</i><sup>2</sup>.\n",
            "notice": "本题运用了等面积法将所求阴影部分的面积转化成梯形 <i>ABEH</i> 的面积.\n利用全等三角形的性质解平移问题的方法：先由平移变换得到全等三角形，然后利用全等三角形的性质构建对应角或边相等，进一步确定待求的角（或边）与已知角（或边）的关系，从而求出这个角（或这条边）.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，将矩形纸片 <i>ABCD</i> 折叠，使点 <i>D</i> 与点 <i>B</i> 重合，点 <i>C</i> 落在 <i>C'</i> 处，折痕为 <i>EF</i>，若 <i>AB</i> = 1，<i>BC</i> = 2，则 △<i>ABE</i> 和 △<i>BC'F</i> 的周长之和为（　　）.\n<br/><img style=\"width:9.19em;vertical-align:middle;padding:5px;\" src=\"19.png\">",
            "way": "",
            "step": "根据折叠的性质得：<var class=\"latex\">BE = ED</var>，<var class=\"latex\">BC' = DC</var>，<var class=\"latex\">C'F = CF</var>，在矩形 <var class=\"latex\">ABCD</var> 中 <var class=\"latex\">AB = DC</var>，<var class=\"latex\">AD = BC</var>，故 △<var class=\"latex\">ABE</var> 和 △<var class=\"latex\">BC'F</var> 的周长之和为<br/><var class=\"latex\">AB + BE</var><var class=\"latex\"> + AE + BC'</var><var class=\"latex\"> + BF + C'F</var><var class=\"latex\"><br/> = AB + ED</var><var class=\"latex\"> + AE + DC</var><var class=\"latex\"> + BF + CF</var><var class=\"latex\"><br/> = AB + AD</var><var class=\"latex\"> + DC + BC = 6</var>.\n<br/>故本题正确答案为C.\n",
            "notice": "",
            "A": "",
            "opts": [
                "3",
                "4",
                "6",
                "8"
            ]
        },
        {
            "kind": "example",
            "label": "题型四　利用全等三角形的性质综合应用",
            "type": 9,
            "Q": "如图所示，△<i>ADF</i> ≌ △<i>CBE</i>，且点 <i>A</i>，<i>F</i>，<i>E</i>，<i>C</i> 在同一直线上，猜想 <i>DF</i> 与 <i>BE</i> 的位置关系，并加以说明.\n<br/><img style=\"width:10.88em;vertical-align:middle;padding:5px;\" src=\"20.png\">",
            "way": "观察图形易知 <i>DF</i> 与 <i>BE</i> 是两个全等三角形的对应边，所以从数量的角度看是相等；从位置的角度看，两条线段是平行的，需要证明内错角相等，而全等三角形的对应角相等，进而可以证明.\n",
            "step": "<i>DF</i> ∥ <i>BE</i><br/>∵△<i>ADF</i> ≌ △<i>CBE</i><br/>∴∠<i>AFD</i> = ∠<i>CEB</i><br/>又∵∠<i>AFD</i> + ∠<i>DFE</i> = 180°，∠<i>CEB</i> + ∠<i>BEF</i> = 180°<br/>∴∠<i>DFE</i> = ∠<i>BEF</i><br/>∴ <i>DF</i> ∥ <i>BE</i>.\n",
            "notice": "（1）两条线段的关系要从数量关系和位置关系两个方面去考虑.\n<br>（2）数量关系一般是相等，通过全等的性质可以证明；位置关系一般是平行或者垂直，从图中可以直接看出.\n<br>（3）<b>证线段位置关系</b>通常考虑平行、垂直两种特殊关系，<b>证平行的方法</b>是转化为证明同位角相等、内错角相等或同旁内角互补，这些角的关系一般可由全等三角形的性质提供；<b >证垂直的方法</b>是转化为证明它们的夹角为90°或三角形的两角互余.\n",
            "A": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，在 <i>Rt</i>△<i>ABC</i> 中，∠<i>ACB</i> = 90°，且 <i>AC</i> = <i>BC</i> = 4<i>cm</i>，已知 △<i>BCD</i> ≌ △<i>ACE</i>.\n求四边形 <i>AECD</i> 的面积.\n<br/><img style=\"width:11.38em;vertical-align:middle;padding:5px;\" src=\"21.png\">",
            "way": "线段 <i>AC</i> 把四边形 <i>AECD</i> 分成了两部分，由 Δ<i>ACE</i> ≌ Δ<i>BCD</i>，易求出四边形 <i>AECD</i> 的面积.\n",
            "step": "∵△<i>BCD</i> ≌ △<i>ACE</i>，<br>∴△<i>AEC</i> 与 △<i>BCD</i> 的面积相等，<br>∴ <i>S</i><sub>四边形<i>AECD</i></sub><br>= <i>S</i><sub>△<i>ACD</i></sub> + <i>S</i><sub>△<i>AEC</i></sub> = <i>S</i><sub>△<i>ACD</i></sub> + <i>S</i><sub>△<i>BCD</i></sub> = <i>S</i><sub>△<i>ACB</i></sub> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>× 4 × 4 = 8<i>cm</i><sup>2</sup>.\n",
            "notice": "此题考查全等三角形的性质，关键是根据全等三角形的性质得出 △<i>AEC</i> 与 △<i>BCD</i> 的面积相等.\n",
            "A": ""
        }
    ]
}