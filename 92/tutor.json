{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第二讲　简单的轴对称图形和利用轴对称进行设计",
            "description": "1.\n经历探索简单图形轴对称性的过程，进一步体会轴对称的特征，发展空间观念；<br>2.\n探索并了解角的平分线、线段垂直平分线的有关性质；<br>3.\n能够按要求作出简单平面图形经过轴对称后的图形；<br>4.\n能利用轴对称图形进行一些图案设计.\n"
        },
        {
            "kind": "kpt",
            "name": "等腰三角形的性质",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "性质1：",
                    "sub": [
                        "等腰三角形的两个底角<b>相等</b>（简写成“<b>等边对等角</b>”）.\n<br><b>注：</b>适用条件：必须在同一个三角形中.\n<br>应用格式：在Δ<i>ABC</i>中，因为<i>AB = AC</i>，所以∠<i>B</i> = ∠<i>C</i>.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "性质2：",
                    "sub": [
                        "等腰三角形的<b>顶角平分线、底边上的中线、底边上的高</b>相互重合（简写成“<b>三线合一</b>”）.\n<br><b>注：</b>（1）含义：这是等腰三角形所特有的性质，它实际上是一组定理，应用过程中，在三角形是等腰三角形的前提下，“顶角的平分线、底边上的中线、底边上的高”只要知道其中“一线”，就可以说明是其他“两线”.\n<img src=\"1.png\" style=\"width:8.56em;vertical-align:middle;padding:5px;float:right;\"><br>应用格式：如图，在Δ<i>ABC</i>中，<br>①∵ <i>AB = AC</i>，<i>AD</i> ⊥ <i>BC</i>，<br>∴ <i>AD</i> 平分∠<i>BAC</i>（或 <i>BD</i> = <i>CD</i>）；<br>②∵ <i>AB = AC</i>，<i>BD = DC</i>，<br>∴ <i>AD</i> ⊥ <i>BC</i>（或 <i>AD</i> 平分∠<i>BAC</i>）；<br>③∵ <i>AB = AC</i>，<i>AD</i> 平分∠<i>BAC</i>，<br>∴ <i>BD = DC</i>（或 <i>AD</i> ⊥ <i>BC</i>）.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用等要三角形的性质1求角",
            "type": 9,
            "Q": "（1）在 Δ<i>ABC</i> 中，<i>AB = AC</i>，若∠<i>A</i> = 50°，求∠<i>B</i> 的度数；<br>（2）若等腰三角形的一个角为70°，求顶角的度数；<br>（3）若等腰三角形的一个角为90°，求顶角的度数；<br>（4）若等腰三角形的一个角为100°，求顶角的度数.\n",
            "way": "给出的条件中，若底角、顶角已确定，可直接运用三角形的内角和定理与等腰三角形的两底角相等的性质求解：若给出的条件中底角、顶角不确定，则要分两种情况求解.\n",
            "step": "（1）∵ <i>AB = AC</i>，∴∠<i>B</i> = ∠<i>C</i>（等边对等角）.\n<br>∠<i>A</i> + ∠<i>B</i> + ∠<i>C</i> = 180°（三角形内角和定理），<br>∴ 50 + 2∠<i>B</i> = 180°，解得∠<i>B</i> = 65°.\n<br>（2）当底角为70°时，顶角为180°－2 × 70° = 40°，<br>当顶角为70°时，底角为<span class=\"mathquill-rendered-math\">\\frac{180°－70°}{2}</span>= 55°，符合题意，<br>∴ 顶角的度数为40°或70°.\n<br>（3）若顶角为90°，则底角为<span class=\"mathquill-rendered-math\">\\frac{180°－90°}{2}</span>= 45°.\n<br>若底角为90°，则三个内角的和将大于180°，不符合三角形内角和定理.\n<br>∴ 顶角的度数为90°.\n<br>（4）若顶角为100°，则底角为<span class=\"mathquill-rendered-math\">\\frac{180°－100°}{2}</span>= 40°，<br>若底角为100°，则两底角和为200°，不符合三角形内角和定理.\n<br>∴ 顶角的度数为100°.\n",
            "notice": "（1）在等腰三角形中求角时，要看给出的角是否已确定为顶角或底角若已确定，则直接利用三角形的内角和定理求解；若没有指出所给的角是顶角还是底角，则要分两种情况讨论，并看是否符合三角形内角和定理.\n<br>（2）若等腰三角形中给出的一内角是直角或钝角，则此角必为顶角.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "等腰三角形一腰上的高与另一腰的夹角为50°，则底角的度数为________.\n",
            "step": "分两种情况讨论：<br>①若∠<i>A</i> &gt; 90°，如图1所示：<br><img src=\"2.png\" style=\"width:7.75em;vertical-align:middle;padding:5px;\"><br>∵ <i>BD</i> ⊥ <i>AC</i>，∴∠<i>A</i> + ∠<i>ABD</i> = 90°，<br>∵∠<i>ABD</i> = 50°，<br>∴∠<i>A</i> = 90°－50° = 40°，<br>∵ <i>AB = AC</i>，<br>∴∠<i>ABC</i> = ∠<i>C</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>(180°－40°) = 70°；<br>②若∠<i>A</i> &gt; 90°，如图2所示：<br><img src=\"3.png\" style=\"width:11.56em;vertical-align:middle;padding:5px;\"><br>同①可得：∠<i>DAB</i> = 90°－50° = 40°，<br>∴∠<i>BAC</i> = 180°－40° = 140°，<br>∵ <i>AB = AC</i>，<br>∴∠<i>ABC</i> = ∠<i>C</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>(180°－140°) = 20°；<br>综上所述：等腰三角形底角的度数为70°或20°.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用等腰三角形的性质1求边长或周长",
            "type": 9,
            "Q": "等腰三角形的两边长分别为4和6，求这个等腰三角形的周长.\n",
            "way": "根据等腰三角形的定义可得，等腰三角形的腰长可能是4，也可能是6，由三角形的周长等于三条边长的和，可求出三角形的周长.\n",
            "step": "①若4是腰，则三角形的三边是4、4、6，因为4 + 4 &gt; 6，所以这三个数据能组成三角形，故三角形的周长是14；<br>②若4是底，则三角形的三边是6、6、4，因为6 + 6 &gt; 4，所以这三个数据能组成三角形，故三角形的周长是16.\n<br>故此等腰三角形的周长是14或16.\n",
            "notice": "此类问题容易出错的地方是：①忽视角形的三边关系；②没有注意到分类讨论，如本例易直接误认为边长为4或者是6，而没有考虑到这两种情况均成立.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "等腰三角形的两边长分别为4和8，求这个等腰三角形的周长.\n",
            "step": "①若4是腰，则另一腰也是4，底是8，但是4 + 4 = 8，故不构成三角形，舍去.\n<br>②若4是底，则腰是8，8.\n<br>4 + 8 &gt; 8，符合条件.\n成立.\n<br>故周长为：4 + 8 + 8 = 20.\n<br>故答案为：20.\n"
        },
        {
            "kind": "example",
            "label": "题型三　利用等腰三角形的性质2解决有关线段和角的问题",
            "type": 9,
            "Q": "如图，在等腰三角形 <i>ABC</i> 中，<i>AB = AC</i>，<i>AD</i> 是 <i>BC</i> 边上的中线，∠<i>ABC</i> 的平分线 <i>BG</i>，交 <i>AD</i> 于点 <i>E</i>，<i>EF</i> ⊥ <i>AB</i>，垂足为 <i>F</i>.\n<br><img src=\"4.png\" style=\"width:6.12em;vertical-align:middle;padding:5px;\"><br>①若∠<i>BAD</i> = 20°，则∠<i>C</i> = ________.\n<br>②求证：<i>EF = ED</i>.\n",
            "way": "①根据等腰三角形三线合一的性质可得 <i>AD</i> ⊥ <i>BC</i>，且∠<i>CAD</i> = ∠<i>BAD</i>，再根据直角三角形两锐角互余列式计算即可得解；<br>②根据角平分线上的点到角的两边的距离相等证明即可.\n",
            "step": "①∵ <i>AB = AC</i>，<i>AD</i> 是 <i>BC</i> 边上的中线，<br>∴ <i>AD</i> ⊥ <i>BC</i>，∠<i>CAD</i> = ∠<i>BAD</i>，（等腰三角形三线合一）.\n<br>∵∠<i>BAD</i> = 20°，<br>∴∠<i>CAD</i> = 20°，<br>∴∠<i>C</i> = 90°－∠<i>CAD</i> = 90°－20° = 70°；<br>②证明：<br>∵ <i>AD</i> ⊥ <i>BC</i>，<i>EF</i> ⊥ <i>AB</i>，<i>BG</i> 平分∠<i>ABC</i>，<br>∴ <i>EF = ED</i>.\n",
            "notice": "（1）等腰三角形的“三线合一”的性质是说明角相等、线段相等和垂直关系的既重要又简便的方法；因为题目的说明或计算所求结果大多都是单一的，所以“三线合一”性质的应用也是单一的，一般得出一个结论，因此应用要灵活.\n<br>（2）在等腰三角形中，作“三线”中“一线”，利用“三线合一”是等腰三角形中常用的方法.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，已知 <i>AB = AC</i>，<i>AD = AE</i>.\n<br><img src=\"5.png\" style=\"width:11.37em;vertical-align:middle;padding:5px;\"><br>求证：<i>BD = CE</i>.\n",
            "step": "证明：<br>作 <i>AF</i> ⊥ <i>BC</i> 于 <i>F</i>，<br><img src=\"6.png\" style=\"width:9.5em;vertical-align:middle;padding:5px;\"><br>∵ <i>AB = AC</i>（已知），<br>∴ <i>BF = CF</i>（三线合一），<br>又∵ <i>AD = AE</i>（已知），<br>∴ <i>DF = EF</i>（三线合一），<br>∴ <i>BF</i>－<i>DF</i> = <i>CF</i>－<i>EF</i>，即 <i>BD = CE</i>（等式的性质）.\n"
        },
        {
            "kind": "kpt",
            "name": "线段的垂直平分线",
            "sub": [
                {
                    "kind": "kpt",
                    "name": "线段的垂直平分线定义：",
                    "sub": [
                        "经过线段中点并且垂直于这条线段的直线，叫做这条线段的垂直平分线.\n简称中垂线.\n<br><b>注：</b>线段的垂直平分线必须满足两个条件：<img src=\"8.png\" style=\"width:11.68em;vertical-align:middle;float:right;\"><br>①经过线段的中点；<br>②垂直于这条线段.\n<br>如图，<i>DC</i> 是 <i>AB</i> 的垂直平分线↔<img src=\"7.svg\" style=\"width:5em;vertical-align:middle;\">"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "线段的垂直平分线性质：",
                    "sub": [
                        "线段垂直平分线上的点与这条线段两个端点的距离相等；<b>条件：</b>点在线段的垂直平分线上；<img src=\"9.png\" style=\"width:12em;vertical-align:middle;float:right;\"><br><b>结论：</b>这个点到线段两端点的距离相等.\n<br>表达方式：如图，<i>l</i> ⊥ <i>AB</i>，<i>AO = BO</i>，点 <i>P</i> 在 <i>l</i> 上，则 <i>AP = BP</i>.\n"
                    ]
                },
                {
                    "kind": "kpt",
                    "name": "线段的垂直平分线作用：",
                    "sub": [
                        "可用来证明两线段相等.\n"
                    ]
                }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用线段的垂直平分线的性质找相等项",
            "type": 1,
            "Q": "（中考真题）如图，四边形 <i>ABCD</i> 中，<i>AC</i> 垂直平分 <i>BD</i>，垂足为 <i>E</i>，下列结论不一定成立的是（　　）<br><img src=\"10.jpg\" style=\"width:9.93em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "<i>AB = AD</i>",
                "<i>AC</i> 平分∠<i>BCD</i>",
                "<i>AB = BD</i>",
                "△<i>BEC</i> ≌ △<i>DEC</i>"
            ],
            "way": "根据线段垂直平分线的性质得出 <i>AB</i> 与 <i>AD</i> 的关系，结合三角形全等对四个选项进行逐一验证.\n",
            "step": "∵ <i>AC</i> 垂直平分 <i>BD</i>，<br>∴ <i>AB = AD</i>，<i>BC = CD</i>，<br>∴ <i>AC</i> 平分∠<i>BCD</i>，<i>EB = DE</i>，<br>∴∠<i>BCE</i> = ∠<i>DCE</i>，<br>在 <i>Rt</i>△<i>BCE</i> 和 <i>Rt</i>△<i>DCE</i> 中，<br><i>BE = ED</i>，<i>BC = CD</i>，<br>∴ <i>Rt</i>△<i>BCE</i> ≌ <i>Rt</i>△<i>DCE</i>（<i>HL</i>），<br>故选：C.\n",
            "notice": "平面几何图形问题的解决方法：分析图形，结合已知条件对基本图形的形状进行判定是常用的方去，然后再根据具体图形的性质作出判断即可.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，<i>AC = AD</i>，<i>BC = BD</i>，则有（　　）<br><img src=\"11.png\" style=\"width:8.56em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "<i>AB</i> 垂直平分 <i>CD</i>",
                "<i>CD</i> 垂直平分 <i>AB</i>",
                "<i>AB</i> 与 <i>CD</i> 互相垂直平分",
                "<i>CD</i> 平分∠<i>ACB</i>"
            ],
            "step": "答案：A.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用线段的垂直平分线的性质求线段的长",
            "type": 1,
            "Q": "如图，△<i>ABC</i> 中，<i>AB</i> = 5，<i>AC</i> = 6，<i>BC</i> = 4，边 <i>AB</i> 的垂直平分线交 <i>AC</i> 于点 <i>D</i>，则 △<i>BDC</i> 的周长是（　　）<br><img src=\"12.png\" style=\"width:9.5em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "8",
                "9",
                "10",
                "11"
            ],
            "way": "由线段的垂直平分线的性质可知 <i>DA = DB</i>，那么 Δ<i>BCD</i> 的周长其实是 <i>AC</i> 与 <i>BC</i> 的长度和.\n",
            "step": "对图形进行点标注，如图所示：<br><img src=\"12.png\" style=\"width:9.5em;vertical-align:middle;padding:5px;\"><br>∵ <i>ED</i> 垂直平分 <i>AB</i>，<br>∴ <i>AD = BD</i>.\n<br>∵ △<i>BDC</i> 的周长 = <i>DB + BC + CD</i>，<br>∴ △<i>BDC</i> 的周长 = <i>AD + BC + CD = AC + BC</i> = 6 + 4 = 10.\n<br>故选C.\n",
            "notice": "本题运用<b>转化思想</b>，用线段垂直平分线的性质把 <i>BD</i> 的长转化成 <i>AD</i> 的长，从而把未知的 <i>BD</i> 与 <i>CD</i> 的长度和转化成已知的线段 <i>AC</i> 的长.\n本题中 <i>AC</i> 的长、<i>BC</i> 的长及 Δ<i>BCD</i> 的周长三者可互相转化，知其二可求第三者.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，△<i>ABC</i> 中，<i>AB、AC</i> 的垂直平分线分别交 <i>BC</i> 于点 <i>D，E</i>，已知 △<i>ADE</i> 的周长为12<i>cm</i>，则 <i>BC</i> = ________.\n<br><img src=\"13.png\" style=\"width:11.12em;vertical-align:middle;padding:5px;\">",
            "step": "∵ <i>DF、EG</i> 分别是线段 <i>AB、AC</i> 的垂直平分线，<br>∴ <i>AD = BD，AE = CE</i>，<br>∴ <i>AD + DE + AE = BD + DE + CE = BC</i>，<br>∵ △<i>ADE</i> 的周长为12<i>cm</i>，即 <i>AD + DE + AE</i> = 12<i>cm</i>，<br>∴ <i>BC</i> = 12<i>cm</i>.\n"
        },
        {
            "kind": "example",
            "label": "题型三　利用线段的垂直平分线的性质求角度",
            "type": 9,
            "Q": "如图，在 △<i>ABC</i> 中，∠<i>A</i> = 40°，∠<i>B</i> = 90°，线段 <i>AC</i> 的垂直平分线 <i>MN</i> 与 <i>AB</i> 交于点 <i>D</i>，与 <i>AC</i> 交于点 <i>E</i>，则∠<i>BCD</i> 的度数是________.\n<br><img src=\"14.png\" style=\"width:6.31em;vertical-align:middle;padding:5px;\">",
            "way": "根据垂直平分线的性质计算.\n∠<i>BCD</i> = ∠<i>BCN</i>－∠<i>DCA</i>.\n据此解答即可.\n",
            "step": "∵ △<i>ABC</i> 中，∠<i>B</i> = 90°，∠<i>A</i> = 40°，<br>∴∠<i>BCN</i> = 180°－∠<i>B</i>－∠<i>A</i> = 180°－90°－40° = 50°.\n<br>∵ <i>DN</i> 是 <i>AC</i> 的垂直平分线，<br>∴ <i>DA = DC</i>，∠<i>A</i> = ∠<i>DCA</i> = 40°，∠<i>BCD</i> = ∠<i>BCN</i>－∠<i>DCA</i> = 50°－40° = 10°，<br>即∠<i>BCD</i> 的度数是10°.\n",
            "notice": "利用线段的垂直平分线的性质得出边相等，从而得出三角形全等，再利用全等三角形中对应角相等确定∠<i>DCE</i> 的度数，根据角度差解决问题.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图所示，线段 <i>AC</i> 的垂直平分线交线段 <i>AB</i> 于点 <i>D</i>，∠<i>A</i> = 50°，则∠<i>BDC</i> = （　　）<br><img src=\"15.png\" style=\"width9.81em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "50°",
                "100°",
                "120°",
                "130°"
            ],
            "step": "∵ <i>DE</i> 是线段 <i>AC</i> 的垂直平分线，<br>∴ <i>DA = DC</i>，<br>∴∠<i>DCA</i> = ∠<i>A</i> = 50°，<br>∴∠<i>BDC</i> = ∠<i>DCA</i> + ∠<i>A</i> = 100°，<br>故选：B.\n"
        },
        {
            "kind": "example",
            "label": "题型四　利用作线段的垂直平分线解决实际问题",
            "type": 9,
            "Q": "如图，某城市规划局为了方便居民的生活，计划在三个住宅小区 <i>A，B，C</i> 之间修建一个购物中心，试问：该购物中心应建于何处，才能使得它到三个小区的距离相等？<br><img src=\"16.png\" style=\"width:11.31em;vertical-align:middle;padding:5px;\">",
            "way": "本题转化为数学问题就是要找一个点，使它到三角形的三个顶点的距离相等.\n首先考虑到 <i>A，B</i> 两点距离相等的点应该在线段 <i>AB</i> 的垂直平分线上，到 <i>B，C</i> 两点距离相等的点应该在线段 <i>BC</i> 的垂直平分线上，两条垂直平分线的交点即为所求的点.\n",
            "step": "连接 <i>AB</i>，分别以 <i>A、B</i> 为圆心，大于<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AB</i> 为半径画弧，两弧交于两点，连接这两点即是作 <i>AB</i> 的垂直平分线；<br>同理连接 <i>BC</i>，作出 <i>BC</i> 的垂直平分线，两条直线交于点 <i>P</i>，则点 <i>P</i> 就是商场的位置；<br><img src=\"17.png\" style=\"width:11.31em;vertical-align:middle;padding:5px;\">",
            "notice": "解决作图选点性问题：若要找到某两个点的距离相等的点，一般在这两点所连线段的垂直平分线上去找；若要找到某两条不平行的直线的距离相等的点，则一般在这两条直线相交所成的角的平分线上去找.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "某地区要在 <i>S</i> 区域内修建一个超市 <i>M</i>，如图，按照要求，超市 <i>M</i> 到两个新建的居民小区 <i>A、B</i> 的距离相等，到两条公路 <i>OC</i> 和 <i>OD</i> 的距离也相等，这个超市应建于何处（在图上标出它的位置）？<br>要求：用尺规作图，保留作图痕迹，不写作法.\n<br><img src=\"18.png\" style=\"width:12.93em;vertical-align:middle;padding:5px;\">",
            "step": "如图所示：点 <i>M</i> 即为所求.\n<br><img src=\"19.png\" style=\"width:12.93em;vertical-align:middle;padding:5px;\">"
        },
        {
            "kind": "kpt",
            "name": "轴对称中的剪纸问题",
            "sub": [
                {
                    "kind": "example",
                    "label": "题型一　剪纸图形的判断",
                    "type": 1,
                    "Q": "剪纸是中国的民间艺术.\n剪纸方法很多，如图是一种剪纸方法的图示（先将纸折叠，然后再剪，展开后即得到图案），如下所示的四副图案，不能用上述方法剪出的是（　　）<br><img src=\"20.png\" style=\"width:35.18em;vertical-align:middle;padding:5px;\">",
                    "opts": [
                        "<img src=\"21.png\" style=\"width:5.75em;vertical-align:middle;\">",
                        "<img src=\"22.png\" style=\"width:5.75em;vertical-align:middle;\">",
                        "<img src=\"23.png\" style=\"width:5.75em;vertical-align:middle;\">",
                        "<img src=\"24.png\" style=\"width:5.75em;vertical-align:middle;\">"
                    ],
                    "way": "由剪纸得到的图形都是轴对称图形，选项中只有C项是中心对称图形，不是轴对称图形.\n",
                    "step":"答案：C.\n",
                    "notice": "实际动手试一下可以很直观的展现出答案.\n"
                }
            ]
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "用一张正方形纸按图中的过程进行折叠，再把折叠后的纸剪去阴影部分，则展开后得到的图形是（　　）<br><img src=\"25.png\" style=\"width:17.87em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "<img src=\"26.png\" style=\"width:5em;vertical-align:middle;\">",
                "<img src=\"27.png\" style=\"width:5em;vertical-align:middle;\">",
                "<img src=\"28.png\" style=\"width:5em;vertical-align:middle;\">",
                "<img src=\"29.png\" style=\"width:5em;vertical-align:middle;\">"
            ],
            "step": "根据题目中的折叠方式，再把折叠后的纸剪去阴影的部分，然后展开，可知剪去的阴影部分为正方形的四个角.\n<br>故本题正确答案为A.\n"
        },
        {
            "kind": "example",
            "label": "题型二　设计剪纸的图案",
            "type": 9,
            "Q": "取一张长30<i>cm</i>、宽6<i>cm</i>的纸条，将它每3<i>cm</i>一段，一反一正像“手风琴”那样折叠起来，并在折叠好的纸上画出字母 <i>E</i>.\n用小刀把画出的字母 <i>E</i> 挖去，拉开“手风琴”，你就可以得到一条以字母 <i>E</i> 为图案的花边（如图所示）.\n<br><img src=\"30.png\" style=\"width:16.43em;vertical-align:middle;padding:5px;\"><br>（1）在你所得的花边中，相邻两个图案有什么关系？相间的两个图案又有什么关系？<br>（2）如果以相邻两个图案为一组构成一个图案，任两个图案之间有什么关系？三个图案为一组呢？<br>（3）在上面的活动中，如果先把纸条纵向对折，再折成“手风琴”，然后继续上面的步骤，此时会得到的花边是轴对称图形吗？先猜一猜再做一做.\n",
            "way": "（1）因为是在折叠好的纸上画出字母 <i>E</i>，所以相邻两个图案成轴对称，相间的两个图案全等且是可以通过平移得到的；<br>（2）根据轴对称的定义可知两个图案为一组成轴对称关系，三个图案为一组也成轴对称关系；<br>（3）按上面方法可得到是轴对称图形.\n",
            "step": "（1）相邻两个图案成轴对称，相间的两个图案全等且是可以通过平移得到的；<br>（2）两个图案为一组成轴对称关系，三个图案为一组也成轴对称关系；<br>（3）是轴对称图形.\n",
            "notice": "经过折叠、剪切后得到的图案都是轴对称图形，折痕所在的直线是相邻两个图形的对称轴.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "取一张长20<i>cm</i>、宽18<i>cm</i>的长方形彩纸，将它每4.5<i>cm</i>一段，一正一反折3次，在折叠好的纸上画出如图所示的图形，然后将阴影部分用剪刀剪去，将纸打开，你会发现剪出了一个字，这个字是什么？<br><img src=\"31.png\" style=\"width:21.5em;vertical-align:middle;padding:5px;\">",
            "step": "剪出的这个字是“囍”.\n理由如下：<br>根据题目中的折法，展开后有3条对称轴，有4个面，且相邻面关于折痕是对称的.\n<br>分析可知将阴影部分剪去后可知该图形是半个“喜”字，<br>由展开后的4个面中相邻面关于折痕是对称的可知这个字是“囍”.\n"
        }
    ]
}