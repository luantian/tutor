{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第二讲 解一元二次方程（公式法，因式分解法）",
            "description": "1.\n理解一元二次方程求根公式的推导过程；<br>2.\n了解公式法的概念，能熟练应用公式法解一元二次方程；<br>3.\n理解因式分解法的实质，熟练运用因式分解法解一元二次方程.\n"
        },
        {
            "kind": "kpt",
            "name": "根的判别式",
            "sub": [
                "　　<b>1.\n定义：</b>式子<i>b</i><sup>2</sup> －4<i>ac</i>叫做一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0根的判别式，通常用希腊字母“Δ”表示，即Δ = <i>b</i><sup>2</sup> －4<i>ac</i>.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n一元二次方程的根的个数的判断方法：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) 当Δ &gt; 0时，方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)有两个不相等的实数根；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) 当Δ = 0时，方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)有两个相等的实数根；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) 当Δ &lt; 0时，方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)无实数根.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  根的判别式求法",
            "type": 9,
            "Q": "求下列一元二次方程的根的判别式的值<br>(1) <span class=\"mathquill-rendered-math\">－\\frac{1}{4}{{x}^{2}}－x = 1</span>； (2) <span class=\"mathquill-rendered-math\">{{x}^{2}} = －\\sqrt{2}x－\\frac{1}{3}</span>.\n",
            "way": "根的判别式是在一般形式下确定的，因此应先将方程化成一般形式，然后算出根的判别式的值.\n",
            "step": "(1) 原方程化为<span class=\"mathquill-rendered-math\">\\frac{1}{4}{{x}^{2}} + x + 1 = 0</span>，<span class=\"mathquill-rendered-math\">\\Delta = {{1}^{2}}－4 × 1 × \\frac{1}{4} = 0</span>.\n<br>(2) 原方程化为<span class=\"mathquill-rendered-math\">{{x}^{2}} + \\sqrt{2}x + \\frac{1}{3} = 0</span>，<span class=\"mathquill-rendered-math\">\\Delta = {{\\left( \\sqrt{2} \\right)}^{2}}－4 × 1 × \\frac{1}{3} = \\frac{2}{3}</span>.\n",
            "notice": "<b>求一元二次方程的根的判别式时应注意两点：</b>一是将方程化成一般形式后才能确定<i>a</i>，<i>b</i>，<i>c</i>的值；二是确定<i>a</i>，<i>b</i>，<i>c</i>的值时不要漏掉符号.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "在方程<i>x</i><sup>2</sup> －4<i>x</i> = 0中， 根的判别式Δ的值为（　　）",
            "opts": [
                "－16",
                "16",
                "－4",
                "4"
            ],
            "step": "答案：B.\n"
        },
        {
            "kind": "example",
            "label": "题型二  根的情况的判别",
            "type": 1,
            "Q": "关于<i>x</i>的一元二次方程<i>x</i><sup>2</sup>  + <i>ax</i>－1 = 0的根的情况是（　　）",
            "opts": [
                "没有实数根",
                "只有一个实数根",
                "有两个相等的实数根",
                "有两个不相等的实数根"
            ],
            "way": "判断上述方程的根的情况，只要看根的判别式Δ = <i>b</i><sup>2</sup> －4<i>ac</i>的值的符号就可以了.\n",
            "step": "∵<i>a</i> = 1，<i>b</i> = <i>a</i>，<i>c</i> = －1，<br>∴Δ = <i>b</i><sup>2</sup> －4<i>ac</i> = <i>a</i><sup>2</sup> －4 × 1 × (－1) = <i>a</i><sup>2</sup>  + 4 &gt; 0，<br>∴方程有两个不相等实数根.\n<br>故选D.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "一元二次方程<i>x</i><sup>2</sup>  + <i>x</i> + 3 = 0的根的情况是（　　）",
            "opts": [
                "有两个不相等的实数根",
                "有两个相等的实数根",
                "没有实数根",
                "无法确定"
            ],
            "step": "答案：B.\n"
        },
        {
            "kind": "example",
            "label": "题型三 利用根的判别式求待定字母系数的取值范围（或值）",
            "type": 1,
            "Q": "关于<i>x</i>的一元二次方程(<i>m</i>－2)<i>x</i><sup>2</sup>  + 2<i>x</i> + 1 = 0有实数根，则<i>m</i>的取值范围是（　　）",
            "opts": [
                "<i>m</i> &le; 3",
                "<i>m</i> < 3",
                "<i>m</i> &lt; 3且<i>m</i> ≠ 2",
                "<i>m</i> &le; 3且<i>m</i> ≠ 2"
            ],
            "way": "根据一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)的根的判别式Δ = <i>b</i><sup>2</sup> －4<i>ac</i>的意义得到<i>m</i>－2 ≠ 0且Δ &ge; 0，即2<sup>2</sup> －4 × (<i>m</i>－2) × 1 &ge; 0，然后解不等式组即可得到<i>m</i>的取值范围.\n",
            "step": "∵关于<i>x</i>的一元二次方程(<i>m</i>－2)<i>x</i><sup>2</sup>  + 2<i>x</i> + 1 &ge; 0有实数根，<br>∴<i>m</i>－2 ≠ 0且Δ &ge; 0，即2<sup>2</sup> －4 × (<i>m</i>－2) × 1 &ge; 0，解得<i>m</i> &le; 3，<br>∴<i>m</i>的取值范围是<i>m</i> &le; 3且<i>m</i> ≠ 2.\n<br>故选：D.\n",
            "notice": "利用根的判别式求待定字母系数的取值范围时，易忽视二次项系数不为零的隐含条件.\n"
        },
        {
            "kind": "ques",
            "type": 2,
            "Q": "若关于<i>x</i>的一元二次方程<i>x</i><sup>2</sup> －2<i>x</i> + <i>k</i> = 0无实数根，则实数<i>k</i>的取值范围是________.\n",
            "step": "∵关于<i>x</i>的一元二次方程<i>x</i><sup>2</sup> －2<i>x</i> + <i>k</i> = 0无实数根，<br>∴Δ = <i>b</i><sup>2</sup> －4<i>ac</i> = (－2)<sup>2</sup> －4 × 1 × <i>k</i> &lt; 0，<br>∴<i>k</i> &gt; 1.\n"
        },
        {
            "kind": "kpt",
            "name": "公式法解一元二次方程",
            "sub": [
                "　　<b>1.\n求根公式的定义：</b>当Δ &ge; 0时，方程<i>ax<sup>2</sup>  + bx + c</i> = 0(<i>a</i> ≠ 0)的实数根可写为<span class=\"mathquill-rendered-math\">x = \\frac{－b ± \\sqrt{{{b}^{2}}－4ac}}{2a}</span>的形式，这个式子叫做一元二次方程<i>ax<sup>2</sup>  + bx + c</i> = 0的<b>求根公式</b>.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n用公式法解一元二次方程的一般步骤：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) 把一元二次方程化成一般形式；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) 确定公式中<i>a</i>，<i>b</i>，<i>c</i>的值；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) 求出<i>b</i><sup>2</sup> －4<i>ac</i>的值；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(4) 若<i>b</i><sup>2</sup> －4<i>ac</i> &ge; 0，则把<i>a</i>，<i>b</i>及<i>b</i><sup>2</sup> －4<i>ac</i>的值代入求根公式求解，当<i>b</i><sup>2</sup> －4<i>ac</i> < 0时，方程无实数解．"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  利用公式法解一元二次方程",
            "type": 9,
            "Q": "用公式法解方程：<br>(1) <i>x</i><sup>2</sup>  + <i>x</i>－12 = 0； (2) 3<i>x</i><sup>2</sup> －2<i>x</i>－5 = 0.\n",
            "way": "先计算判别式的值，然后根据求根公式求出方程的解.\n",
            "step": "(1) Δ = 1<sup>2</sup> －4 × 1 × (－12) = 49，<br><span class=\"mathquill-rendered-math\">x = \\frac{－1 ± 7}{2 × 1}</span>，<br>所以<i>x</i><sub>1</sub> = －4，<i>x</i><sub>2</sub> = 3；<br>(2) Δ = (－2)<sup>2</sup> －4 × 3 × (－5) = 64，<br><span class=\"mathquill-rendered-math\">x = \\frac{2 ± 8}{2 × 3}</span>，<br>所以<i>x</i><sub>1</sub> = －1，<i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\frac{5}{3}</span>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "用公式法解方程：<br>(1) 4<i>x</i><sup>2</sup> －3<i>x</i>－2 = 0<br>(2) <span class=\"mathquill-rendered-math\">{x}^{2} + 2 = 2\\sqrt{2}x</span><br>(3) (<i>x</i>－2)(1－3<i>x</i>) = 6",
            "step": "(1) 这里<i>a</i> = 4，<i>b</i> = －3，<i>c</i> = －2，<br>∵Δ = 9 + 32 = 41，<br>∴<span class=\"mathquill-rendered-math\">x = \\frac{3 ± \\sqrt{41}}{8}</span>；<br>(2) 方程整理得：<span class=\"mathquill-rendered-math\">{x}^{2}－2\\sqrt{2}x + 2 = 0</span>，<br>这里<i>a</i> = 1，<i>b</i> = <span class=\"mathquill-rendered-math\">－2\\sqrt{2}</span>，<i>c</i> = 2，<br>∵Δ = 8－8 = 0，<br>∴<span class=\"mathquill-rendered-math\">x = \\frac{2\\sqrt{2} ± 0}{2} = \\sqrt{2}</span>，<br>则<i>x</i><sub>1</sub> = <i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\sqrt{2}</span>.\n<br>(3) 3<i>x</i><sup>2</sup> －7<i>x</i> + 8 = 0，<br>Δ = (－7)<sup>2</sup> －4 × 3 × 8 = －47 &lt; 0，<br>所以没有实数解.\n"
        },
        {
            "kind": "example",
            "label": "题型二  分类思想在方程根的情况中的应用",
            "type": 9,
            "Q": "已知方程<i>m</i><sup>2</sup> <i>x</i><sup>2</sup>  + (2<i>m</i> + 1)<i>x</i> + 1 = 0有实数根，求<i>m</i>的取值范围.\n",
            "way": "此题要要分类讨论：当<i>m</i><sup>2</sup> = 0，即<i>m</i> = 0，方程变为：<i>x</i> + 1 = 0，有解；当<i>m</i><sup>2</sup>  ≠ 0，即<i>m</i> ≠ 0，原方程要有实数根，则Δ &ge; 0，即Δ = (2<i>m</i> + 1)<sup>2</sup> －4<i>m</i><sup>2</sup> = 4<i>m</i> + 1 &ge; 0，解得<i>m</i> &ge; －<span class=\"mathquill-rendered-math\">\\frac{1}{4}</span>，则<i>m</i>的范围是<i>m</i> &ge; －<span class=\"mathquill-rendered-math\">\\frac{1}{4}</span>且<i>m</i> ≠ 0；最后综合两种情况得到<i>m</i>的取值范围.\n",
            "step": "当<i>m</i><sup>2</sup> = 0，即<i>m</i> = 0，方程变为：<i>x</i> + 1 = 0，有解；<br>当<i>m</i><sup>2</sup>  ≠ 0，即<i>m</i> ≠ 0，原方程要有实数根，则Δ &ge; 0，<br>即Δ = (2<i>m</i> + 1)<sup>2</sup> －4<i>m</i><sup>2</sup> = 4<i>m</i> + 1 &ge; 0，<br>解得<span class=\"mathquill-rendered-math\">m &ge; －\\frac{1}{4}</span>，<br>则<i>m</i>的范围是<span class=\"mathquill-rendered-math\">m &ge; －\\frac{1}{4}</span>且<i>m</i> ≠ 0；<br>所以，<i>m</i>的取值范围为<span class=\"mathquill-rendered-math\">m &ge; －\\frac{1}{4}</span>.\n",
            "notice": "当方程的二次项系数为字母参数时，需要根据题意对参数是否取取0进行判断或分情况讨论.\n如果本题说“已知关于x的一元二次方程……”，那<i>m</i> ≠ 0.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "已知关于x的方程<span class=\"mathquill-rendered-math\">{{x}^{2}}－\\left( 2k + 1 \\right)x + 4\\left( k－\\frac{1}{2} \\right) = 0</span><br>(1) 求证：这个方程总有两个实数根.\n<br>(2) 若等腰△<i>ABC</i>的一边长<i>a</i> = 4，另两边<i>b</i>、<i>c</i>恰好是这个方程的两个实数根，求△<i>ABC</i>的周长.\n",
            "step": "(1) 证明：<span class=\"mathquill-rendered-math\">\\Delta = {{\\left( 2k + 1 \\right)}^{2}}－4 × 1 × 4\\left( k－\\frac{1}{2} \\right)</span><br><span class=\"mathquill-rendered-math\"> = 4{{k}^{2}}－12k + 9</span><br><span class=\"mathquill-rendered-math\"> = {{\\left( 2k－3 \\right)}^{2}}</span>，<br>∵无论 <i>k</i> 取什么实数值，<span class=\"mathquill-rendered-math\">{{\\left( 2k－3 \\right)}^{2}}\\ge 0</span>，<br>∴<span class=\"mathquill-rendered-math\">\\Delta \\ge 0</span>，<br>∴无论 <i>k</i> 取什么实数值，方程总有实数根；<br>(2) 解：∵<span class=\"mathquill-rendered-math\">x = \\frac{2k + 1 ± \\left( 2k－3 \\right)}{2}</span>，<br>∴<i>x</i><sub>1</sub> = 2<i>k</i>－1，<i>x</i><sub>2</sub> = 2，<br>∴等腰三角形的三边分别为4、2、2<i>k</i>－1，分两种情况讨论：<br>①2<i>k</i>－1 = 4，求得三角形的周长 = 4 + 4 + 2 = 10；<br>②2<i>k</i>－1 = 2，不满足三角形三边关系，此情况不存在.\n综上所述，△<i>ABC</i>的周长为10.\n"
        },
        {
            "kind": "kpt",
            "name": "因式分解法解一元二次方程",
            "sub": [
                "　　<b>1.\n定义：</b>先因式分解，使方程化为两个一次式的乘积等于0的形式，再使这两个一次式分别等于0，从而实现降次，这种解一元二次方程的方法叫做<b>因式分解法</b>.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n因式分解法解一元二次方程的一般步骤：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) 移项：整理方程，使其右边为0；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) 化积：将方程左边分解为两个一次式的乘积；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) 转化：令每个一次式分别为0，得到两个一元一次方程；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(4) 求解：分别解这两个一元一次方程，它们的解就是原方程的解.\n<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.\n常用的因式分解的方法：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) 提公因式法；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) 公式法：平方差公式、完全平方公式；<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) 十字相乘法：<i>x</i><sup>2</sup>  + (<i>a + b</i>)<i>x + ab</i> = (<i>x + a</i>)(<i>x + b</i>).\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  提公因式法解一元二次方程",
            "type": 9,
            "Q": "解下列一元二次方程：<br>(1) <i>x</i><sup>2</sup>  + 4<i>x</i> = 0； (2)（易错题）2(<i>x</i>－3) = 3<i>x</i>(<i>x</i>－3).\n",
            "way": "(1) 利用提取公因式法对等式的左边进行因式分解；<br>(2) 方程利用因式分解法求出解即可.\n",
            "step": "(1) <i>x</i><sup>2</sup>  + 4<i>x</i> = 0，<br><i>x</i>(<i>x</i> + 4) = 0，<br>则<i>x</i> = 0或<i>x</i> + 4 = 0，<br>解得x<sub>1</sub> = 0，x<sub>2</sub> = －4；<br>(2) 方程整理得：2(<i>x</i>－3)－3<i>x</i>(<i>x</i>－3) = 0，<br>分解因式得：(<i>x</i>－3)(2－3<i>x</i>) = 0，<br>解得：x<sub>1</sub> = 3，x<sub>2</sub> = <span class=\"mathquill-rendered-math\">\\frac{2}{3}</span>.\n",
            "notice": "1.\n用因式分解法解一元二次方程时，不要急于将方程化成一般形式，要结合方程的特点适当变形，发现并提取公因式或运用公式，将方程化成两个一次因式的积的形式.\n<br>2.\n解方程(2)时千万不能将方程两边除以(<i>x</i>－3)，否则会漏掉一解.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "解方程：(<i>x</i>－5)(<i>x</i>－6) = <i>x</i>－5",
            "step": "(<i>x</i>－5)(<i>x</i>－6)－(<i>x</i>－5) = 0，<br>(<i>x</i>－5)(<i>x</i>－6－1) = 0，<br><i>x</i>－5 = 0或<i>x</i>－6－1 = 0，<br>所以<i>x</i><sub>1</sub> = 5，<i>x</i><sub>2</sub> = 7.\n"
        },
        {
            "kind": "example",
            "label": "题型二 公式法解一元二次方程",
            "type": 9,
            "Q": "解方程：4(<i>x</i>－3)<sup>2</sup> －25(<i>x</i>－2)<sup>2</sup> = 0",
            "way": "方程左边符合平方差公式分解因式的特征，所以可以用平方差公式分解.\n",
            "step": "[2<i>x</i>(<i>x</i>－3)]<sup>2</sup> －[5(<i>x</i>－2)]<sup>2</sup> = 0<br>[2(<i>x</i>－3) + 5(<i>x</i>－2)][2(<i>x</i>－3)－5(<i>x</i>－2)] = 0<br>(7<i>x</i>－16)(－3<i>x</i> + 4) = 0<br>∴7<i>x</i>－16 = 0或－3<i>x</i> + 4 = 0，<br>∴<i>x</i><sub>1</sub> = <span class=\"mathquill-rendered-math\">\\frac{16}{7}</span>，<i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\frac{4}{3}</span>",
            "notice": "解此类题的技巧：右化零，左分解，两因式，各求解.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "解方程：(2<i>y</i> + 1)<sup>2</sup>  + 2(2<i>y</i> + 1) + 1 = 0",
            "step": "套用完全平方公式<br>(2<i>y</i> + 1)<sup>2</sup>  + 2(2<i>y</i> + 1) + 1 = 0<br>[(2<i>y</i> + 1) + 1]<sup>2</sup> = 0<br>(2<i>y</i> + 2)<sup>2</sup> = 0<br><i>y</i> = －1"
        },
        {
            "kind": "example",
            "label": "题型三  十字相乘法解x<sup>2</sup>  + px + q = 0型的方程",
            "type": 9,
            "Q": "用十字相乘解下列方程：<br>(1) <i>x</i><sup>2</sup> －<i>x</i>－2 = 0；<br>(2) <i>x</i><sup>2</sup>  + 2<i>x</i>－3 = 0；<br>(3) <i>x</i><sup>2</sup> －5<i>x</i> + 6 = 0.\n",
            "step": "(1) 分解因式得：(<i>x</i>－2)(<i>x</i> + 1) = 0，<br>解得：<i>x</i><sub>1</sub> = 2，<i>x</i><sub>2</sub> = －1；<br>(2) 分解因式得：(<i>x</i>－1)(<i>x</i> + 3) = 0，<br>解得：<i>x</i><sub>1</sub> = 1，<i>x</i><sub>2</sub> = －3；<br>(3) 分解因式得：(<i>x</i>－2)(<i>x</i>－3) = 0，<br>解得：<i>x</i><sub>1</sub> = 2，<i>x</i><sub>2</sub> = 3；",
            "notice": "解<i>x</i><sup>2</sup>  + <i>px</i> + <i>q</i> = 0型的方程，当左边的多项式因式分解时，必须保证常数项分解的两个因数的和为一次项系数，两根的符号与两因式的常数项的符号正好相反.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "用十字相乘法解下列方程：<br>(1) <i>x</i><sup>2</sup> －6<i>x</i>－16 = 0； (2) <i>x</i><sup>2</sup>  + 2015<i>x</i>－2016 = 0； (3) <span class=\"mathquill-rendered-math\">{x}^{2}－(\\sqrt{2} + \\sqrt{3})x + \\sqrt{6} = 0</span>.\n",
            "way": "(2) 设法找到两个数<i>a</i>，<i>b</i>，使它们的和为<i>a + b</i> = －2015，积为<i>ab</i> = －2016，经过反复试验可发现这两个数分别为1，－2016，所以<i>x</i><sup>2</sup>  + 2015<i>x</i>－2016 = 0可转化为(<i>x</i>－1)(<i>x</i> + 2016) = 0；同理，(2)、(3)等号的左边也可分解成两个一次因式的乘积.\n",
            "step": "(1) 原方程可化为(<i>x</i>－8)(<i>x</i> + 2) = 0<br>∴<i>x</i>－8 = 0或<i>x</i> + 2 = 0<br>∴<i>x</i><sub>1</sub> = 8，<i>x</i><sub>2</sub> = －2<br>(2) 原方程可化为(<i>x</i>－1)(<i>x</i> + 2016) = 0<br>∴<i>x</i>－1 = 0或<i>x</i> + 2016 = 0<br>∴<i>x</i><sub>1</sub> = 1，<i>x</i><sub>2</sub> = －2016<br>(3) 原方程可化为(<span class=\"mathquill-rendered-math\">x－\\sqrt{2}</span>)(<span class=\"mathquill-rendered-math\">x－\\sqrt{3}</span>) = 0<br>∴<span class=\"mathquill-rendered-math\">x－\\sqrt{2}</span> = 0或<span class=\"mathquill-rendered-math\">x－\\sqrt{3}</span> = 0<br>∴<i>x</i><sub>1</sub> = <span class=\"mathquill-rendered-math\">\\sqrt{2}</span>，<i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\sqrt{3}</span>"
        },
        {
            "kind": "kpt",
            "name": "一元二次方程的解法的选用",
            "sub": [
                "　　<b>1.\n解一元二次方程的方法：</b>直接开平方法、配方法、公式法、因式分解法.\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2.\n解一元次方程方法的选择顺序：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;先特殊后一般，即先考虑直接开平方法和因式分解法，最后再使用公式法；一般情况下，不用配方法.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一  选择适当的方法解一元二次方程",
            "type": 9,
            "Q": "用适当的方法解下列一元二次方程：<br>(1) <i>x</i><sup>2</sup> －2<i>x</i>－3 = 0； (2) 2<i>x</i><sup>2</sup> －7<i>x</i>－6 = 0； (3) (<i>x</i>－1)<sup>2</sup> －3(<i>x</i>－1) = 0.\n",
            "way": "方程(1)选择配方法；方程(2)选择公式法；方程(3)选择因式分解法.\n",
            "step": "(1) <i>x</i><sup>2</sup> －2<i>x</i>－3 = 0，移项得<i>x</i><sup>2</sup> －2<i>x</i> = 3<br>配方得(<i>x</i>－1)<sup>2</sup> = 4，<i>x</i>－1 = ± 2；<br>∴<i>x</i><sub>1</sub> = 3，<i>x</i><sub>2</sub> = －1<br>(2) 2<i>x</i><sup>2</sup> －7<i>x</i>－6 = 0<br>∵<i>a</i> = 2，<i>b</i> = －7，<i>c</i> = －6，<br>∴Δ = <i>b</i><sup>2</sup> －4<i>ac</i> = 97 &gt; 0<br>∴<i>x</i><sub>1</sub> = <span class=\"mathquill-rendered-math\">\\frac{7 + \\sqrt{97}}{4}</span>，<i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\frac{7－\\sqrt{97}}{4}</span><br>(3) (<i>x</i>－1)<sup>2</sup> －3(<i>x</i>－1) = 0<br>(<i>x</i>－1)(<i>x</i>－1－3) = 0<br>即(<i>x</i>－1)(<i>x</i>－4) = 0<br>∴<i>x</i>－1 = 0或<i>x</i>－4 = 0<br>∴<i>x</i><sub>1</sub> = 1，<i>x</i><sub>2</sub> = 4",
            "notice": "解一元二次方程时，优先考虑因式分解法，其次是公式法，如果方程的系数较大，且一次项系数是偶数，可以用配方法.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "用恰当的方法解下列一元二次方程：<br>(1) <i>x</i><sup>2</sup> －6<i>x</i>－3 = 0；<br>(2) 9<i>x</i><sup>2</sup>  + 1 = 6<i>x</i>；<br>(3) <i>x</i><sup>2</sup> －1 = 4(<i>x</i> + 1)；<br>(4) (2<i>x</i> + 3)<sup>2</sup> －(<i>x</i> + 2)<sup>2</sup> = 0.\n",
            "step": "(1) <i>x</i><sup>2</sup> －6<i>x</i>－3 = 0，<br><i>a</i> = 1，<i>b</i> = －6，<i>c</i> = －3，<br>∴Δ = <i>b</i><sup>2</sup> －4<i>ac</i> = 36 + 12 = 48，<br>则<span class=\"mathquill-rendered-math\">x = \\frac{－b ± \\sqrt{{{b}^{2}}－4ac}}{2a} = \\frac{6 ± 4\\sqrt{3}}{2} = 3 ± 2\\sqrt{3}</span>，<br>∴<i>x</i><sub>1</sub> = <span class=\"mathquill-rendered-math\">3 + 2\\sqrt{3}</span>，<i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">3－2\\sqrt{3}</span>；<br>(2) 9<i>x</i><sup>2</sup>  + 1 = 6<i>x</i>，<br>(3<i>x</i>－1)<sup>2</sup> = 0，<br>∴<i>x</i><sub>1</sub> = <i>x</i><sub>2</sub> = <span class=\"mathquill-rendered-math\">\\frac{1}{3}</span>；<br>(3) <i>x</i><sup>2</sup> －1 = 4(<i>x</i> + 1)，<br>(<i>x</i> + 1)(<i>x</i>－1) = 4(<i>x</i> + 1)，<br>(<i>x</i> + 1)(<i>x</i>－1－4) = 0，<br>∴<i>x</i><sub>1</sub> = －1，<i>x</i><sub>2</sub> = 5；<br>(4) (2<i>x</i> + 3)<sup>2</sup> －(<i>x</i> + 2)<sup>2</sup> = 0，<br>(2<i>x</i> + 3 + <i>x</i> + 2)(2<i>x</i> + 3－<i>x</i>－2) = 0，<br>(3<i>x</i> + 5)(<i>x</i> + 1) = 0，<br>解得：<i>x</i><sub>1</sub> = <span class=\"mathquill-rendered-math\">－\\frac{5}{3}</span>，<i>x</i><sub>2</sub> = －1.\n"
        },
        {
            "kind": "example",
            "label": "题型二  利用换元法解一元二次方程",
            "type": 9,
            "Q": "解方程：(3<i>x</i> + 2)<sup>2</sup> －8(3<i>x</i> + 2) + 15 = 0",
            "way": "本题如将3<i>x</i> + 2看成一个整体，用另外的一个字母代替，可使解方程更简单.\n",
            "step": "设3<i>x</i> + 2 = <i>y</i>，原方程化为<i>y</i><sup>2</sup> －8<i>y</i> + 15 = 0，<br>即(<i>y</i>－3)(<i>y</i>－5) = 0，解得<i>y</i><sub>1</sub> = 3，<i>y</i><sub>2</sub> = 5，<br>当<i>y</i> = 3时，3<i>x</i> + 2 = 3，<i>x</i> = <span class=\"mathquill-rendered-math\">\\frac{1}{3}</span>，<br>当<i>y</i> = 5时，3<i>x</i> + 2 = 5，<i>x</i> = 1，<br>故原方程的解为x<sub>1</sub> = <span class=\"mathquill-rendered-math\">\\frac{1}{3}</span>，<i>x</i><sub>2</sub> = 1.\n",
            "notice": "运用此方法时，要先找出相同的整体进行换元，从而化简方程，解完方程后还要注意还元.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "已知<i>x</i>为实数，且满足(<i>x</i><sup>2</sup>  + <i>x</i> + 1) + 2(<i>x</i><sup>2</sup>  + <i>x</i> + 1)－3 = 0，那么<i>x</i><sup>2</sup>  + <i>x</i> + 1的值为（  ）",
            "opts": [
                "1",
                "－3",
                "－3或1",
                "－1或3"
            ],
            "step": "设<i>x</i><sup>2</sup>  + <i>x</i> + 1是<i>y</i>，<br>则原方程变形为：<i>y</i><sup>2</sup>  + 2<i>y</i>－3 = 0，<br>解方程得：<i>y</i><sub>1</sub> = －3，<i>y</i><sub>2</sub> = 1，<br>①<i>x</i><sup>2</sup>  + <i>x</i> + 1 = －3，此方程无解，舍去；<br>②<i>x</i><sup>2</sup>  + <i>x</i> + 1 = 1，此方程有解.\n<br>则<i>x</i><sup>2</sup>  + <i>x</i> + 1的值是1.\n<br>故选A.\n"
        }
    ]
}