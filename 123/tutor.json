{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "第三讲：分式方程",
            "description": "1.\n了解分式方程的概念和检验根的意义，会解可化为一元一次方程的分式方程.\n<br>2.\n会列出分式方程解简单的应用问题.\n"
        },
        {
            "kind": "kpt",
            "name": "分式方程的定义",
            "sub": [
                "分母中含有未知数的方程叫分式方程.\n<br><b>注：</b>分式方程和整式方程的区别就在于分母中是否有未知数（不是一般的字母系数）.\n分母中含有未知数的方程是分式方程，分母中不含有未知数的方程是整式方程.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　方程的有关概念",
            "type": 1,
            "Q": "下列方程是分式方程的是（　　）",
            "opts": [
                "<span class=\"mathquill-rendered-math\">\\frac{x－b}{a}</span> = 2－<span class=\"mathquill-rendered-math\">\\frac{x－a}{b}</span>（<i>a</i>，<i>b</i>为常数）",
                "<span class=\"mathquill-rendered-math\">x + \\frac{1}{x}</span> = <span class=\"mathquill-rendered-math\">c + \\frac{1}{c}</span>（<i>c</i>为常数）",
                "<span class=\"mathquill-rendered-math\">x + \\frac{b}{2}</span> = 5（<i>b</i>为常数）",
                "<span class=\"mathquill-rendered-math\">\\frac{x + 1}{2}</span> = 3"
            ],
            "way": "利用分母中含有未知数的方程叫做分式方程，进而判断即可．",
            "step": "A、<span class=\"mathquill-rendered-math\">\\frac{x－b}{a}</span> = 2－<span class=\"mathquill-rendered-math\">\\frac{x－a}{b}</span>（<i>a</i>，<i>b</i>为常数），是整式方程，不合题意<br><span class=\"mathquill-rendered-math\">x + \\frac{1}{x}</span> = <span class=\"mathquill-rendered-math\">c + \\frac{1}{c}</span>（<i>c</i>为常数），是分式方程，符合题意<br><span class=\"mathquill-rendered-math\">x + \\frac{b}{2}</span> = 5（<i>b</i>为常数），是整式方程，不合题意<br><span class=\"mathquill-rendered-math\">\\frac{x + 1}{2}</span> = 3，是整式方程，不合题意",
            "notice": "此题主要考查了分式方程的定义，正确把握定义是解题关键.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列关于的方程，是分式方程的是（　　）",
            "opts": [
                "<span class=\"mathquill-rendered-math\">\\frac{3 + x}{2}－3 = \\frac{2 + x}{5}</span>",
                "<span class=\"mathquill-rendered-math\">\\frac{2x－1}{7} = \\frac{x}{2}</span>",
                "<span class=\"mathquill-rendered-math\">\\frac{x}{\\pi } + 1 = \\frac{2－x}{3}</span>",
                "<span class=\"mathquill-rendered-math\">\\frac{1}{2 + x} = 1－\\frac{2}{x}</span>"
            ],
            "way": "",
            "step": "<i>A</i>、方程分母中不含未知数，故不是分式方程；<br><i>B</i>、方程分母中不含未知数，故不是分式方程；<br><i>C</i>、方程分母中不含表示未知数的字母，<span class=\"mathquill-rendered-math\">\\pi</span> 是常数；<br><i>D</i>、方程分母中含未知数 <i>x</i>，故是分式方程.\n<br>故选 <i>D</i>.\n",
            "notice": "判断一个方程是否为分式方程，主要是依据分式方程的定义，也就是看分母中是否含有未知数（注意：仅仅是字母不行，必须是表示未知数的字母）.\n"
        },
        {
            "kind": "kpt",
            "name": "分式方程的解法",
            "sub": [
                "<b>解分式方程的基本思想：</b>将分式方程转化为整式方程.\n转化方法是方程两边都乘以最简公分母，去掉分母.\n在去分母这一步变形时，有时可能产生使最简公分母为零的根，这种根叫做原方程的增根.\n因为解分式方程时可能产生增根，所以解分式方程时必须验根.\n<br><b>解分式方程的一般步骤：</b><br>（1）方程两边都乘以最简公分母，去掉分母，化成整式方程（注意：当分母是多项式时，先分解因式，再找出最简公分母）；<br>（2）解这个整式方程，求出整式方程的解；<br>（3）检验：将求得的解代入最简公分母，若最简公分母不等于0，则这个解是原分式方程的解，若最简公分母等于0，则这个解不是原分式方程的解，原分式方程无解.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　解分式方程",
            "type": 9,
            "Q": "解分式方程：（1）<span class=\"mathquill-rendered-math\">\\frac{x}{x－1} + \\frac{2}{1－x} = 3</span>.\n（2）<span class=\"mathquill-rendered-math\">\\frac{x－1}{x－2} + \\frac{1}{2－x} = 3</span>",
            "way": "分式方程去分母转化为整式方程，求出整式方程的计算得到<i>x</i>的值，经检验即可得到分式方程的解.\n",
            "step": "去分母得：<i>x－2 = 3x－3</i>，<br>计算得出：<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>，<br>经检验<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>是分式方程的解.\n<br>（2）原方程即<span class=\"mathquill-rendered-math\">\\frac{x－1}{x－2}－\\frac{1}{x－2} = 3</span>.\n<br>方程两边都乘以<span class=\"mathquill-rendered-math\">(x－2)</span>，得<span class=\"mathquill-rendered-math\">x－1－1 = 3(x－2)</span>.\n<br>计算得出<span class=\"mathquill-rendered-math\">x = 2</span>.\n<br>经检验<span class=\"mathquill-rendered-math\">x = 2</span>是原方程的增根，<br>∴ 原方程无解.\n",
            "notice": "解分式方程一般有三个步骤：<br>（1）“去”，即去分母，将原方程化为整式方程.\n<br>（2）“解”，即解这个整式方程.\n<br>（3）“验”，即验根.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "解方程：（1）<span class=\"mathquill-rendered-math\">\\frac{2}{3x－1}－1 = \\frac{3}{6x－2}</span>（2）<span class=\"mathquill-rendered-math\">\\frac{5x + 2}{2x－3} + \\frac{19}{4x－6} = \\frac{7}{2}</span>",
            "way": "先去分母将原分式方程转化为整式方程，求得<i>x</i>值后再代入原分式方程检验即可.\n",
            "step": "（1）方程两端同时乘以<span class=\"mathquill-rendered-math\">(6x－2)</span>得：<span class=\"mathquill-rendered-math\">2 × 2－(6x－2) = 3</span>，化简得：<span class=\"mathquill-rendered-math\">4－6x+2 = 3</span>，解得：<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>，检验：当<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>时，<span class=\"mathquill-rendered-math\">6x－2 = 1 ≠ 0</span>，所以<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>是原分式方程的解，故原分式方程的解为<span class=\"mathquill-rendered-math\">x = \\frac{1}{2}</span>.\n<br>（2）由题意知<span class=\"mathquill-rendered-math\">\\frac{5x + 2}{2x－3} + \\frac{19}{4x－6} = \\frac{7}{2}</span>，去分母得<span class=\"mathquill-rendered-math\">2(5x + 2) + 19 = 7(2x－3)</span>，整理得，<i>4x = 44</i>，<i>x = 11</i>.\n<br><b>经校验，<i>x = 11</i>是原方程的解</b>.\n",
            "notice": "本题考查解分式方程的能力.\n"
        },
        {
            "kind": "example",
            "label": "题型二　由分式方程的解求出字母的值",
            "type": 9,
            "Q": "已知关于<i>x</i>的方程<span class=\"mathquill-rendered-math\">\\frac{2ax}{a－x} = \\frac{2}{3}</span>的根是<i>x = 1</i>，求<i>a</i>的值.\n",
            "way": "首先将<i>x = 1</i>代入方程<span class=\"mathquill-rendered-math\">\\frac{2ax}{a－x} = \\frac{2}{3}</span>，得到关于<i>a</i>的方程；<br>然后再解关于<i>a</i>的分式方程，求出方程的解即可.\n",
            "step": "∵关于<i>x</i>的方程<span class=\"mathquill-rendered-math\">\\frac{2ax}{a－x} = \\frac{2}{3}</span>的根是<i>x = 1</i>，<br>∴将<i>x = 1</i>代入方程<span class=\"mathquill-rendered-math\">\\frac{2ax}{a－x} = \\frac{2}{3}</span>，<br>得：<span class=\"mathquill-rendered-math\">\\frac{2a}{a－1} = \\frac{2}{3}</span>，<br>即<span class=\"mathquill-rendered-math\">3 × 2a = 2(a－1)</span><br>解得：<span class=\"mathquill-rendered-math\">a = －\\frac{1}{2}</span><br>经检验<span class=\"mathquill-rendered-math\">a = －\\frac{1}{2}</span>满足方程.\n<br>∴<i>a</i>的值为－<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span>.\n",
            "notice": "本题是有关求分式方程中参数的取值问题，熟练掌握分式方程的解法，方程的解的定义是解题的关键.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "若<i>x = 3</i>是分式方程<span class=\"mathquill-rendered-math\">\\frac{a－2}{x}－\\frac{1}{x－2} = 0</span>的根，则<i>a</i>的值是（　　）",
            "opts": [
                "5",
                "－5",
                "3",
                "－3"
            ],
            "way": "",
            "step": "将<i>x = 3</i>代入分式方程可得：<span class=\"mathquill-rendered-math\">\\frac{a－2}{x}－\\frac{1}{x－2} = 0</span>，化简得<span class=\"mathquill-rendered-math\">\\frac{a－2}{3} = 1</span>，解得<i>a = 5</i>.\n故本题正确答案为<i>A</i>.\n",
            "notice": "本题主要考查分式方程及其解法.\n"
        },
        {
            "kind": "kpt",
            "name": "解分式方程产生增根的原因",
            "sub": [
                "<b>增根：</b>方程变形时，可能产生不适合原方程的根，这种根叫做原方程的<b>增根</b>.\n<br><b>增根的原因：</b>去分母时，方程两边同乘的最简公分母是含有字母的式子，这个式子有可能为零，对于整式方程来说，求出的根成立，而对于原分式方程来说，分式无意义，所以这个根是原分式方程的增根.\n<br><b>注：</b>解分式方程一定要检验根，这种检验与整式方程不同，不是检查解方程过程中是否有错误，而是检验是否出现增根，它是在解方程的过程中没有错误的前提下进行的.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用方程根的情况求字母参数的值",
            "type": 9,
            "Q": "关于<i>x</i>的分式方程<span class=\"mathquill-rendered-math\">\\frac{x－a}{x－1}－\\frac{3}{x} = 1</span>.\n<br>（１）若此方程有增根1，求<i>a</i>的值；<br>（２）若此方程有增根，求<i>a</i>的值；<br>（３）若此方程无解，求<i>a</i>的值.\n",
            "way": "若一个数为分式方程的增根，则这个数一定是去分母后的整式方程的根；利用这个结论可求待定字母的值.\n分式方程无解必须具备：最简公分母等于0或去分母后的整式方程无解.\n",
            "step": "（１）去分母并整理，得<span class=\"mathquill-rendered-math\">(a + 2)x = 3</span>.\n<br>因为1是原方程的增根，所以<span class=\"mathquill-rendered-math\">(a + 2) × 1 = 3</span>，<i>a = 1</i>.\n<br>（２）因为原分式方程有增根，所以<span class=\"mathquill-rendered-math\">x(x－1) = 0</span>，<i>x = 0</i>或<i>x = 1</i>.\n<br>又因为整式方程<span class=\"mathquill-rendered-math\">(a + 2)x = 3</span>有根，所以<i>x = 1</i>.\n<br>因此原分式方程的增根为1.\n所以<span class=\"mathquill-rendered-math\">(a + 2) × 1 = 3</span>，所以<i>a = 1</i>.\n<br>（３）去分母并整理得：<span class=\"mathquill-rendered-math\">(a + 2)x = 3</span> .\n<br>①当<i>a + 2 = 0</i>时，该整式方程无解，此时<i>a = －2</i>.\n<br>②当<i>a + 2 ≠ 0</i>时，要使原方程无解，则<span class=\"mathquill-rendered-math\">x(x－1) = 0</span>，<i>x = 0</i>或<i>x = 1</i>，把<i>x = 0</i>入整式方程，<i>a</i>的值不存在，把<i>x = 1</i>代入整式方程，得<i>a = 1</i>.\n<br>综合①②得： <i>a = －2</i>或<i>a = 1</i>.\n",
            "notice": "分式方程有增根，一定存在使最简公分母等于0的未知数的值.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "试问当<i>m</i>取何值时，分式方程<span class=\"mathquill-rendered-math\">\\frac{1}{x－3} + \\frac{m}{3－x} = 4</span>会产生增根？",
            "way": "解这类题，一般先将分式方程化为整式方程，求出<i>x</i>值(用<i>m</i>的代数式表示)，再找出使原方程分母为0的<i>x</i>值，得到等式，从而求出<i>m</i>的值",
            "step": "在方程两边同时乘以<i>x－3</i>，得<span class=\"mathquill-rendered-math\">1－m = 4(x－3)</span>.\n解得<span class=\"mathquill-rendered-math\">x = \\frac{13－m}{4}</span>.\n又因为<i>x = 3</i>是原分式方程的增根，从而有<span class=\"mathquill-rendered-math\">x = \\frac{13－m}{4} = 3</span>，所以<i>m = 1</i>.\n即当<i>m = 1</i>时，原分式方程的解会产生增根.\n",
            "notice": "解这类问题关键是认真观察，得出使原分式方程中分母为零的<i>x</i>的值，从而构建出关于待定字母<i>m</i>的等式.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "若关于<i>x</i> 的方程<span class=\"mathquill-rendered-math\">\\frac{2}{x－4} = 3 + \\frac{m}{4－x}</span> 有增根，求<i>m</i> 的值.\n",
            "way": "增根是分式方程化为整式方程后产生的使分式方程的分母为0的根.\n有增根，那么最简公分母<i>x－4 = 0</i>，所以增根是<i>x = 4</i>，把增根代入化为整式方程的方程即可求出未知字母的值.\n",
            "step": "方程两边都乘<span class=\"mathquill-rendered-math\">(x－4)</span>，得<br><span class=\"mathquill-rendered-math\">2 = 3(x－4)－m</span><br>∵当最简公分母<i>x－4 = 0</i>时，方程有增根，<br>把<i>x</i>－4 = 0代入整式方程，得<br><i>m = －2</i>.\n",
            "notice": "增根问题可按如下步骤进行：<br>①代入最简公分母确定增根的值；<br>②化分式方程为整式方程；<br>③把增根代入整式方程即可求得相关字母的值.\n"
        },
        {
            "kind": "example",
            "label": "题型二　根据分式方程的解确定字母参数的取值范围",
            "type": 9,
            "Q": "已知关于<i>x</i>的方程<span class=\"mathquill-rendered-math\">\\frac{x}{x－3}－2 = \\frac{m}{x－3}</span>有一个正数解，求<i>m</i>的取值范围.\n",
            "way": "解此题首先是将分式方程化为整式方程，用含字母的式子表示，然后根据条件确定字母的取值范围.\n",
            "step": "方程两边都乘<span class=\"mathquill-rendered-math\">(x－3)</span>，得<span class=\"mathquill-rendered-math\">x－2(x－3) = m </span>，解得：<i>x = 6－m </i>，∵x＞0，∴<i>6－m ＞ 0</i>，∴<i>m &gt; 6</i>，且<i>x ≠ 3</i>，∴<i>m ≠ 3</i>.\n∴<i>m &gt; 6</i>且<i>m ≠ 3</i>.\n故答案为：<i>m &gt; 6</i>且<i>m ≠ 3</i>",
            "notice": "此题考查了分式方程的解法的知识，解答本题时，易漏掉<i>m ≠ 3</i>，这是因为忽略了<i>x－3 ≠ 0</i>这个隐含的条件而造成的，这应引起同学们的足够重视.\n能根据已知和方程的解得出<i>m</i>的范围是解此题的关键.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "已知关于<i>x</i>的分式方程<span class=\"mathquill-rendered-math\">\\frac{2x-m}{x+1}=2 + <span class=\"mathquill-rendered-math\">\\frac{1}{x}</span></span>，则m的取值范围是（　　）",
            "opts": [
                "<i>m</i> &ge; 3", "<i>m</i> &le; －3", "<i>m</i> &gt －3 且 <i>m</i> ≠ －2", "<i>m</i> &ge; －3且 <i>m</i> ≠ －2"
            ],
            "way": "分式方程去分母转化为整式方程，由分式方程的解为负数确定出<i>m</i>的范围即可．",
            "step": "分式方程去分母得：2<i>x</i><sup>2</sup> － <i>mx</i> = 2<i>x</i><sup>2</sup> + 2<i>x</i> + <i>x</i> + 1<br>整理得：（<i>m</i>+3）<i>x</i>=﹣1，<br>当<i>m</i> + 3 ≠ 0，即 m ≠ ﹣3时，<i>x</i> = ﹣<span class=\"mathquill-rendered-math\">\\frac{1}{m+3}</span>，﹣<span class=\"mathquill-rendered-math\">\\frac{1}{m+3}</span> ≠ ﹣1 且 ﹣<span class=\"mathquill-rendered-math\">\\frac{1}{m+3}</span> ≠ 0<br> 由分式方程解为负数，得到﹣<span class=\"mathquill-rendered-math\">\\frac{1}{m+3}</span> &lt 0，且 <i>m</i> ≠ ﹣2，<i>m</i> ≠ ﹣3，<br>解得：<i>m</i> &gt ﹣3 且 <i>m</i> ≠ ﹣2，",
            "notice": "本题考查了一元一次方程的解，利用方程的解是正数得出不等式是解题关键.\n"
        },
        {
            "kind": "kpt",
            "name": "分式方程的实际应用",
            "sub": [
                "分式方程的应用主要就是列方程解应用题.\n<br><b>列分式方程解应用题按下列步骤进行：</b><br>（１）审题了解已知数与所求各量所表示的意义，弄清它们之间的数量关系；<br>（2）设未知数；<br>（3）找出能够表示题中全部含义的相等关系，列出分式方程；<br>（4）解这个分式方程；<br>（5）验根，检验是否是增根；<br>（6）写出答案.\n"
            ]
        },
        {
            "kind": "example",
            "label": "题型一　工程问题",
            "type": 9,
            "Q": "一项工程，若由甲、乙两公司合作18天可以完成，若甲、乙两公司单独完成此项工程，甲公司所用时间是乙公司的1.5倍.\n求甲、乙两公司单独完成此项工程，各需多少天？",
            "way": "设乙公司单独完成此项工程需<i>x</i>天，则甲公司单独完成需要<i>1.5x</i>天，然后根据两队合作18天完成列出关于x的方程求解即可；",
            "step": "设乙公司单独完成此项工程需<i>x</i>天，则甲公司单独完成需要<i>1.5x</i>天.\n<br>由题意，得<span class=\"mathquill-rendered-math\">\\frac{1}{x} + \\frac{2}{3x} = \\frac{1}{18}</span><br>解得：<i>x = 30</i>，经检验<i>x = 30</i>是原方程的解.\n<br>则<i>1.5x = 45</i>.\n<br>答：甲公司单独完成需要45天，乙公司单独完成需要30天.\n",
            "notice": ""
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "根据城市规划设计，某市工程队准备为该城市修建一条长4800米的公路.\n铺设<i>600m</i>后，为了尽量减少施工对城市交通造成的影响，该工程队增加人力，实际每天修建公路的长度是原计划的2倍，结果9天完成任务，该工程队原计划每天铺设公路多少米？",
            "way": "设原计划每天铺设公路<i>x</i>米，根据实际每天修建公路的长度是原计划的2倍，结果9天完成任务，以时间做为等量关系可列方程求解.\n",
            "step": "设原计划每天铺设公路<i>x</i>米，根据题意，得<span class=\"mathquill-rendered-math\">\\frac{600}{x} + \\frac{4800－600}{2x} = 9</span><br>去分母，得<i>1200 + 4200 = 18x</i>（或<i>18x = 5400</i>）<br>解得<i>x = 300</i>.\n<br>经检验，<i>x = 300</i>是原方程的解且符合题意.\n<br>答：原计划每天铺设公路300米.\n",
            "notice": "本题考查理解题意能力，关键是以时间做为等量关系，列出方程求解.\n"
        },
        {
            "kind": "example",
            "label": "题型二　销售问题",
            "type": 9,
            "Q": "“母亲节”前夕，某商店根据市场调查，用3000元购进第一批盒装花，上市后很快售完，接着又用5400元购进第二批这种盒装花.\n已知第二批所购花的盒数比第一批所购花盒数多100盒，且每盒花的进价比第一批的进价少3元.\n设第一批盒装花的进价是<i>x</i>元，则根据题意可列方程为________",
            "way": "设第一批盒装花的进价是<i>x</i>元/盒，则第一批进货的数量是：<span class=\"mathquill-rendered-math\">\\frac{3000}{x}</span> ，第二批进货的数量是：<span class=\"mathquill-rendered-math\">\\frac{5400}{x－3}</span> ，再根据等量关系：第二批进货的数量 = 第一批进货的数量 + 100可得方程.\n",
            "step": "设第一批盒装花的进价是<i>x</i>元/盒，则<span class=\"mathquill-rendered-math\">\\frac{3000}{x} = \\frac{5400}{x－3} + 100</span><br>故答案是：<span class=\"mathquill-rendered-math\">\\frac{3000}{x} = \\frac{5400}{x－3} + 100</span>",
            "notice": "解分式方程的基本思想是“转化思想”，把分式方程转化为整式方程求解.\n分式方程根的检验，除了要检验它是不是增根，还要看它是否符合实际情况.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "某商家预测一种衬衫能畅销市场，就用12000元购进了一批这种衬衫，上市后果然供不应求，商家又用了26400元购进了第二批这种衬衫，所购数量是第一批购进量的2倍，但每件进价贵了10元.\n<br>（1）该商家购进的第一批衬衫是多少件？<br>（2）若两批衬衫都按每件150元的价格销售，则两批衬衫全部售完后的利润是多少元？",
            "way": "（1）设第一批衬衫<i>x</i>件，则第二批衬衫为<i>2x</i>件，接下来依据第二批衬衫每件进价贵了10元列方程求解即可；<br>（2）先求得每一批衬衫的数量和进价，然后再求得两批衬衫的每一件衬衫的利润，最后根据利润 = 每件的利润<i>x</i>件数求解即可.\n",
            "step": "（1）设第一批衬衫<i>x</i>件，则第二批衬衫为<i>2x</i>件.\n<br>根据题意得： <span class=\"mathquill-rendered-math\">\\frac{12000}{x} = \\frac{26400}{2x}－10</span><br>解得；<i>x = 120</i> . 检验<i>x = 120</i>是原方程的解且符合题意。.\n<br>答；该商家购进的第一批衬衫是120件.\n<br>（2）12000 ÷ 120 = 100，100 + 10 = 110.\n<br>两批衬衫全部售完后的利润 = 120 × （150﹣100） + 240 × （150﹣110） = 15600元.\n<br>答：两批衬衫全部售完后的利润是15600元.\n",
            "notice": "本题主要考查的是分式方程的应用，依据第二批衬衫每件进价贵了10元列出关于<i>x</i>的方程是解题的关键.\n"
        },
        {
            "kind": "example",
            "label": "题型三　行程问题",
            "type": 9,
            "Q": "吉首城区某中学组织学生到距学校<i>20km</i>的德夯苗寨参加社会实践活动，一部分学生沿“谷韵绿道”骑自行车先走，半小时后，其余学生沿319国道乘汽车前往，结果他们同时到达（两条道路路程相同），已知汽车速度是自行车速度的2倍，求骑自行车学生的速度.\n<br><img src=\"1.png\" style=\"width:30em;\">",
            "way": "首先设骑自行车学生的速度是<i>x</i>千米/时，则汽车速度是<i>2x</i>千米/时，由题意可得等量关系；骑自行车学生行驶20千米所用时间－汽车行驶20千米所用时间 = 0.5小时，根据等量关系，列出方程即可.\n",
            "step": "解：设骑自行车学生的速度是<i>x</i>千米/时，由题意得：<span class=\"mathquill-rendered-math\">\\frac{20}{x}－\\frac{20}{2x} = 0.5</span><br>解得：<i>x = 20</i> ，<br>经检验：<i>x = 20</i> 是原分式方程的解，<br>答：骑自行车学生的速度是20千米/时.\n",
            "notice": "本题主要考查分式方程的应用.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "一慢车和一快车同时从<i>A</i>地到<i>B</i>地，<i>A</i>，<i>B</i>两地相距276公里，慢车的速度是快车速度的三分之二，结果快车比慢车早到达2小时，求快车，慢车的速度.\n",
            "way": "直接利用路程 ÷ 速度 = 时间，结合快车比慢车早到达2小时，进而得出等式求出答案.\n",
            "step": "设快车的速度为：<i>xkm/h</i>，则慢车的速度为：<span class=\"mathquill-rendered-math\">\\frac{2}{3}</span><i>km/h</i> 根据题意可得：<span class=\"mathquill-rendered-math\">\\frac{276}{x} + 2 = </span><span class=\"mathquill-rendered-math\">\\frac{276}{\\frac{2}{3}x}</span>解得：<i>x</i> = 69 ，<br>经检验得：<i>x</i> = 69 是原方程的根，<br>故 69 × <span class=\"mathquill-rendered-math\">\\frac{2}{3}</span> = 46",
            "notice": "本题主要考查列分式方程解应用题，考查学生分析和解决问题的能力.\n"
        }
    ]
}