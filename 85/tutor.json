{
    "couID": 1,
    "pages": [
        {
            "kind": "cover",
            "title": "两直线的位置关系",
            "description": "1.\n理解平行线的概念和同一平面内两条直线的位置关系；<br>2.\n理解邻补角、对顶角、垂线等概念及性质；<br>3.\n会用三角尺或量角器过一点画一条直线的垂线，学会用本节知识解释生活中的一些现象及解决一些简单的实际问题；<br>4.\n垂线的性质1和垂线的性质2及点到直线的距离.\n"
        },
        {
            "kind": "kpt",
            "name": "平行线的定义",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "定义：",
                         "sub": ["在同一平面内，<b>不相交</b>的两条直线叫做平行线.\n<br>表示方法：用“ ∥ ”表示平行，如图，记作 “<i>AB</i> ∥ <i>CD</i>” 或 “<i>CD</i> ∥ <i>AB</i>”，读作 “<i>AB</i> 平行于 <i>CD</i>” 或 “<i>CD</i> 平行于 <i>AB</i>”.\n<br><img src=\"1.png\" style=\"width:12em;vertical-align:middle;padding:5px;\">"
                         ]
                     }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　利用平行线的定义进行判断",
            "type": 9,
            "Q": "判断下列说法是否正确，并说明理由.\n<br>（1）不相交的两条直线是平行线；<br>（2）在同一平面内，两条不相交的线段是平行线.\n",
            "way": "（1）没有强调两条直线在同一平面内；<br>（2）两条线段不相交不能保证这两条线段所在的直线不相交.\n",
            "step": "（1）不正确；理由：根据定义，它缺少了”在同一平面内”这一条件.\n<br>（2）不正确；理由：定义中指出的是两条不相交的”直线”，而不是”线段”.\n",
            "notice": "平行线的定义有三个特征：一是在同一平面内；二是不相交；三是都是直线.\n三者缺一不可.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列说法中，正确的是（　　）",
            "opts": [
                "在同一平面内，不相交的两条射线是平行线",
                "在同一平面内，相交的两条线段是平行线",
                "在同一平面内，两条不同的直线的位置关系不相交就平行",
                "不相交的两直线是平行线"
            ],
            "way": "",
            "step": "答案：C.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用平行线的定义识别空间平行",
            "type": 9,
            "Q": "如图的长方体中，与 <i>AA'</i> 平行的棱有哪几条？与 <i>AB</i> 平行的棱有哪几条？分别用符号把它们表示出来.\n<br><img src=\"2.png\" style=\"width:10.81em;vertical-align:middle;padding:5px;\">",
            "way": "根据平行线的定义，结合生活常识，观察图形可解此题.\n",
            "step": "由图可以知道，和棱 <i>AB</i> 平行的棱有 <i>CD</i>，<i>A'B'</i>，<i>C'D'</i>；<br>与棱 <i>AA'</i> 平行的棱有 <i>DD'</i>，<i>BB'</i>， <i>CC'</i> .\n",
            "notice": "本题运用<b>概念辨析法</b>和<b>定义法</b>，发挥空间想象能力，根据平行线的定义解答.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，在长方体 <i>ABCD－A'B'C'D'</i> 中，与棱 <i>AD</i> 平行的棱有________条，它们是________________.\n<br><img src=\"2.png\" style=\"width:10.81em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "与棱 <i>AD</i> 平行的棱有：<i>BC</i>，<i>B'C'</i> ，<i>A'D'</i>，共有三条.\n"
        },
        {
            "kind": "kpt",
            "name": "对顶角",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "",
                         "sub": ["若两条直线只有一个公共点，则称这两条直线为<b>相交线</b>.\n"
                         ]
                     },
                     {
                         "kind": "kpt",
                         "name": "对顶角定义：",
                         "sub": ["两个角有一个公共<b>顶点</b>，并且一个角的两边分别是另一个角的两边的<b>反向延长线</b>，具有这种位置关系的两个角，互为对顶角.\n"
                         ]
                     },
                     {
                         "kind": "kpt",
                         "name": "性质：",
                         "sub": ["对顶角相等.\n<br><b>注：</b>互为对顶角的两个角相等，但相等的两个角不一定互为对顶角.\n"
                         ]
                     }
            ]
        },
        {
            "kind": "video",
            "title": "相交线",
            "name": "相交线",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18523.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　利用对顶角的定义进行识别",
            "type": 1,
            "Q": "下列图形中，∠1与∠2是对顶角的是（　　）",
            "opts": [
                "<img src=\"3.png\" style=\"width:6.62em;\">",
                "<img src=\"4.png\" style=\"width:3.43em;\">",
                "<img src=\"5.png\" style=\"width:3.87em;\">",
                "<img src=\"6.png\" style=\"width:7.31em;\">"
            ],
            "way": "判断两个角是不是对顶角，要紧扣对顶角的定义，<br>A图中∠1和∠2的顶点不同；<br>B图中∠1和∠2的两边都不是互为反向延长线；<br>C图中的∠1和∠2符合定义；<br>D图中∠1和∠2有一条公共边.\n",
            "step": "利用对顶角的定义可得出符合条件的只有C.\n",
            "notice": "<b>判断两个角是否互为对顶角的方法：</b><br>（1）对顶角都是<b>成对</b>出现的，当两个角互为对顶角时，其中一个角叫做另一个角的对顶角；<br>（2）对顶角的两边互为反向延长线，即在同一直线上，其实质是对顶角是两直线相交所成的没有公共边的两个角.\n；<br>（3）对顶角的条件：（1）有公共顶点；（2）两边互为反向延长线.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "下列图形中∠1和∠2是的对顶角的是（　　）",
            "opts": [
                "<img src=\"7.png\" style=\"width:5.37em;\">",
                "<img src=\"8.png\" style=\"width:6.25em;\">",
                "<img src=\"9.png\" style=\"width:7.68em;\">",
                "<img src=\"10.png\" style=\"width:7.43em;\">"
            ],
            "way": "",
            "step": "答案：D.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用对顶角的性质求角的度数",
            "type": 9,
            "Q": "已知直线 <i>AB</i>，<i>CD</i>，<i>EF</i> 相交于点 <i>O</i>，∠<i>DOE</i> = 90°，∠<i>AOE</i> = 36°，求∠<i>BOC</i> 的度数.\n<br><img src=\"11.png\" style=\"width:9.31em;vertical-align:middle;padding:5px;\">",
            "way": "因为∠<i>BOC</i> = ∠<i>AOD</i> 或∠<i>BOC</i> = ∠<i>BOF</i> + ∠<i>COF</i>，所以有两种途径：求∠<i>AOD</i> 或∠<i>BOF</i>，∠<i>COF</i>.\n它们都可由已知∠<i>DOE</i> = 90°，∠<i>AOE</i> = 36°</i>求出.\n",
            "step": "方法一：因为直线 <i>AB</i>，<i>CD</i> 相交于点 <i>O</i>，<br>所以∠<i>BOC</i> = ∠<i>AOD</i>（对顶角相等）<br>因为∠<i>DOE</i> = 90°，∠<i>AOE</i> = 36°，<br>所以∠<i>AOD</i> = ∠<i>DOE</i> + ∠<i>AOE</i> = 90° + 36° = 126°.\n<br>所以∠<i>BOC</i> = ∠<i>AOD</i> = 126°<br><br>方法二：因为直线 <i>AB</i>，<i>CD</i>，<i>EF</i> 相交于点 <i>O</i>，<br>所以∠<i>COF</i> = ∠<i>DOE</i>，∠<i>BOF</i> = ∠<i>AOE</i>（对顶角相等）.\n<br>因为∠<i>DOE</i> = 90°，∠<i>AOE</i> = 36°，<br>所以∠<i>COF</i> = 90°，∠<i>BOF</i> = 36°，<br>所以∠<i>BOC</i> = ∠<i>COF</i> + ∠<i>BOF</i> = 126°.\n",
            "notice": "在进行计算和推理时，“对顶角相等”这个结论常常被用来将要求的角和特征相同的两个角转化成与已知条件相关的角来求解，即对项角构建了一个已知条件和待求结论之间的“桥梁”.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图所示，直线 <i>AB</i>，<i>CD</i>，<i>EF</i> 相交于点 <i>O</i>，∠1 = 20°，∠<i>BOC</i> = 90°，求∠2的度数.\n<br><img src=\"12.png\" style=\"width:7.75em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "∵∠1 = 20°，∠<i>BOC</i> = 90°，<br>∴∠<i>BOE</i> = ∠<i>BOC</i>－∠1 = 90°－20° = 70°，<br>∴∠2 = ∠<i>BOE</i> = 70°.\n"
        },
        {
            "kind": "kpt",
            "name": "余角和补角",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "",
                         "sub": ["<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tr align=\"center\"><td>分类名称</td><td>定义</td><td>图形</td><td>数学语言</td><td>性质</td></tr><tr><td align=\"center\">互余</td><td style=\"padding:0 5px;\">若两个角的和等于90°(直角)，就说这两个角互为余角</td><td><img src=\"13.png\" style=\"height:5em;\"></td><td style=\"padding:0 5px;\">若∠1 + ∠2 = 90°，就说∠1是∠2的余角，或∠1与∠2互为余角</td><td style=\"padding:0 5px;\">同角(等角)的余角相等</td></tr><tr><td align=\"center\">互补</td><td style=\"padding:0 5px;\">若两个角的和等于180°(平角)，就说这两个角互为补角</td><td><img src=\"14.png\" style=\"height:5em;\"></td><td style=\"padding:0 5px;\">若∠<i>α</i> + ∠<i>β</i> = 180°，则说∠<i>α</i>是∠<i>β</i>的补角，或∠<i>β</i>与∠<i>α</i>互为补角</td><td style=\"padding:0 5px;\">同角(等角)的补角相等</td></tr></table>"
                          ]
                      },
                      {
                         "kind": "kpt",
                         "name": "注：",
                         "sub": ["（1）互余，互补必须是两个角之间的关系.\n<br>（2）当互补的两个角有公共顶点和公共边时，又称这两个角互为<b>邻补角（简称邻补角）</b>.\n如图所示，∠<i>AOC</i> 和∠<i>BOC</i> 互为邻补角.\n<br><img src=\"15.png\" style=\"width:14.87em;vertical-align:middle;\">"
                          ]
                      }
            ]
        },
        {
            "kind": "video",
            "title": "余角和补角",
            "name": "余角和补角",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/19417.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　利用余角、补角的定义进行识别",
            "type": 1,
            "Q": "下列说法正确的有（　　）<br>①锐角的余角是锐角，锐角的补角是锐角；<br>②直角没有补角；<br>③钝角没有余角，钝角的补角是锐角；<br>④直角的补角还是直角；<br>⑤一个角的补角与它的余角的差为90°；<br>⑥两个角相等，它们的补角也相等.\n",
            "opts": [
                "3个",
                "4个",
                "5个",
                "6个"
            ],
            "way": "主要紧扣锐角、直角、钝角、余角、补角的特征进行判断，除①②不正确外，其他说法都正确.\n",
            "step": "答案：B.\n",
            "notice": "由于互余的两个角之和90°，那么这两个角都为锐角；互补的两个角之和为180°，那么这两个角为一个锐角一个钝角或两个角都为直角.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，已知 <i>OD</i>，<i>OE</i> 分别平分 ∠<i>AOC</i>，∠<i>BOC</i>，<i>A</i>，<i>O</i>，<i>B</i> 三点在一条直线上，<i>OF</i> 为 <i>OD</i> 的反向延长线，请分别写出∠<i>AOD</i>的余角和补角.\n<br><img src=\"16.png\" style=\"width:13.62em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "∠<i>AOD</i> 的余角有∠<i>COE</i>、∠<i>EOB</i>；<br>∠<i>AOD</i> 的补角有∠<i>BOD</i>、∠<i>COF</i>.\n<br>故答案为：<br>∠<i>AOD</i> 的余角有∠<i>COE</i>、∠<i>EOB</i>；<br>∠<i>AOD</i> 的补角有∠<i>BOD</i>、∠<i>COF</i>.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用互余与互补的定义进行计算",
            "type": 9,
            "Q": "如图，∠<i>AOB</i> 和∠<i>AOD</i> 分别是∠<i>AOC</i> 的余角和补角，且 <i>OC</i> 是∠<i>BOD</i> 的平分线.\n求∠<i>AOC</i> 和∠<i>BOD</i>.\n<br><img src=\"17.png\" style=\"width:7.5em;vertical-align:middle;padding:5px;\">",
            "way": "此题中角的关系比较复杂，适宜用方程去解决.\n",
            "step": "设∠<i>AOC = x°</i>，则∠<i>AOB</i> = (90－<i>x</i>)°，∠<i>AOD</i> = (180－<i>x</i>)°.\n<br>∠<i>BOC</i> = ∠<i>AOC</i>－∠<i>AOB</i> = [<i>x</i>－(90－<i>x</i>)]° = (2<i>x</i>－90)°.\n<br>∠<i>COD</i> = ∠<i>AOD</i>－∠<i>AOC</i> = [(180－<i>x</i>)－<i>x</i>]° = (180－2<i>x</i>)°.\n<br>因为 <i>OC</i> 是∠<i>BOD</i> 的平分线，所以∠<i>BOC</i> = ∠<i>COD</i>，即<br>2<i>x</i>－90 = 180－2<i>x</i>.\n<br>解之，得 <i>x</i> = 67.5.\n<br>所以∠<i>AOC</i> = 67.5°.\n<br>∠<i>BOD</i> = ∠<i>AOD</i>－∠<i>AOB</i> = 90°.\n",
            "notice": "（1）用方程解几何题是<b>方程思想</b>的应用；（2）图中∠<i>BOD</i> 是同一个角的补角与余角的差，所以∠<i>BOD</i> = 90°，与锐角∠<i>AOC</i> 的大小无关；（3）<b>方程中一般不出现单位</b>.\n"
        },
        {
            "kind": "video",
            "title": "余角和补角之列方程",
            "name": "余角和补角之列方程",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/19418.mp4"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "（1）若∠<i>α</i> 的余角与∠<i>β</i> 的补角相等，求∠<i>α</i> 与∠<i>β</i> 的关系；<br>（2）有两个角，它们的度数比为7：3，而它们的差为72°，求这两个角的关系；<br>（3）互余的两个角的差为20°，求这两个角中较小的那个角的补角.\n",
            "way": "",
            "step": "（1）由题意：90°－∠<i>α</i> = 180°－∠<i>β</i>，<br>∴∠<i>β</i> = 900 + ∠<i>α</i>；<br>（2）设这两个角的度数分别为7<i>x</i>°和3<i>x</i>°，<br>由题意，得：7<i>x</i>－3<i>x</i> = 72，<br>解得：<i>x</i> = 18，<br>∴ 这两个角的度数分别为126°和54°，<br>∵ 126° + 54° = 180°，<br>∴ 这两个角互补；<br>（3）设较小的角为 <i>x</i>°，则较大的角为(90－<i>x</i>)°，<br>∴ (90－<i>x</i>)－<i>x</i> = 20，<br>解得：<i>x</i> = 35，<br>∴ 180°－35° = 145°，<br>即较小的那个角的补角为145°.\n"
        },
        {
            "kind": "example",
            "label": "题型三　利用余角或补角的性质进行推理",
            "type": 9,
            "Q": "如图，直线 <i>AB</i> 与∠<i>COD</i> 的两边 <i>OC</i>、<i>OD</i> 分别相交于点 <i>E</i>、<i>F</i>，∠1 + ∠2 = 180°，找出图中与∠2相等的角，并说明理由.\n<br><img src=\"18.png\" style=\"width:13.62em;vertical-align:middle;padding:5px;\">",
            "way": "已知∠1 + ∠2 = 180°，说明∠2是∠1的补角，根据同角（或等角）的补角相等，找出图中∠1的其他补角和∠2的其他补角的补角，便可确定与∠2相等的角.\n",
            "step": "（1）∠2 = ∠6，因为∠2 + ∠5 = 180°，∠6 + ∠5 = 180°，所以∠2 = ∠7；<br>（2）∠2 = ∠3，因为∠1 + ∠3 = 180°，∠1 + ∠2 = 180°，所以∠2 = ∠3；<br>（3）∠2 = ∠4，因为∠1 + ∠4 = 180°，∠1 + ∠2 = 180°，所以∠2 = ∠4.\n",
            "notice": "“同角（或等角）的余角相等”，“同角（或等角）的补角相等”的实质是等量代换，只不过在特定的背景下使用起来更便捷罢了.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，<i>OD</i>，<i>OE</i> 分别是∠<i>AOC</i> 和∠<i>BOC</i> 的平分线，且∠<i>DOE</i> = 90°，请问 <i>A</i>，<i>O</i>，<i>B</i> 三点在同一条直线上吗？为什么？<br><img src=\"19.png\" style=\"width:11.12em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "<i>A</i>，<i>O</i>，<i>B</i> 三点在同一条直线上.\n<br>理由：∵∠<i>DOE</i> = 90°，<br>∴∠<i>DOC</i> + ∠<i>COE</i> = 90°，<br>又∵ <i>OD</i>，<i>OE</i> 分别是∠<i>AOC</i> 和∠<i>BOC</i> 的平分线，<br>∴∠<i>AOC</i> = 2∠<i>DOC</i>，∠<i>COB</i> = 2∠<i>COE</i>，<br>∴∠<i>AOC</i> + ∠<i>COB</i> = 2∠<i>DOC</i> + 2∠<i>COE</i> = 2(∠<i>DOC</i> + ∠<i>COE</i>) = 180°，<br>∴ <i>A</i>，<i>O</i>，<i>B</i> 三点在同一条直线上.\n"
        },
        {
            "kind": "kpt",
            "name": "垂线",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "定义：",
                         "sub": ["在两条直线 <i>AB</i> 和 <i>CD</i> 相交所成的4个角中，如果有一个角是直角，就说这两条直线互相垂直，记作“<i>AB</i> ⊥ <i>CD</i>”，读作“<i>AB</i> 垂直于 <i>CD</i>”，其中一条直线叫做另一条直线的垂线，它们的交点 <i>O</i> 叫做垂足.\n如图所示.\n<br><img src=\"20.svg\" style=\"width:10em;vertical-align:middle;\"><br><b>注：</b>在两条直线相交所成的四个角中，只要其中有一个角是直角，即可由邻补角与对顶角的性质，得到另外三个角也是直角.\n"
                         ]
                      },
                      {
                         "kind": "kpt",
                         "name": "推理格式：",
                         "sub": ["如图上图，因为∠<i>AOC</i> = 90°（已知），所以 <i>AB</i> ⊥ <i>CD</i>（垂直定义）.\n<br>反过来：因为 <i>AB</i> ⊥ <i>CD</i>（已知），所以∠<i>AOC</i> = 90°（垂直定义）.\n"
                         ]
                      }
            ]
        },
        {
            "kind": "video",
            "title": "垂线",
            "name": "垂线",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18524.mp4"
        },
        {
            "kind": "video",
            "title": "角度计算之多解问题",
            "name": "角度计算之多解问题",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18526.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　利用垂直的定义说明两线垂直",
            "type": 1,
            "Q": "如图所示，直线 <i>AB</i>，<i>CD</i> 相交于点 <i>O</i>，下列条件中，不能说明 <i>AB</i> ⊥ <i>CD</i> 的是（         ）",
            "opts": [
                "∠<i>AOD</i> = 90°",
                "∠<i>AOC</i> = ∠<i>BOC</i>",
                "∠<i>BOC</i> + ∠<i>BOD</i> = 180°",
                "∠<i>AOC</i> + ∠<i>BOD</i> = 180°"
            ],
            "way": "本题考查了垂直的定义，熟练掌握垂直的定义是解决本题的关键.\n",
            "step": "<i>A</i>、∠<i>AOD</i> = 90°，根据垂直的定义，可以判定两直线垂直；<br><i>B</i>、∠<i>AOC</i> 和∠<i>BOC</i> 互为邻补角，邻补角相等且和又是180°，所以可以得到∠<i>BOC</i> = 90°，能判定两直线垂直；<br><i>C</i>、∠<i>BOC</i> 与∠<i>BOD</i> 互为邻补角，邻补角的和是180°，不能判定垂直；<br><i>D</i>、∠<i>AOC</i> 与∠<i>BOD</i> 是对顶角，对顶角相等，和又是180°，所以可得到∠<i>AOC</i> = 90°，能判定两直线垂直.\n<br>故选C.\n",
            "notice": "本题应用定义法.\n根据垂直的定义对选项逐一判断.\n垂直的定义告诉我们，要说明 <i>AB</i> ⊥ <i>CD</i>，只需说明有一个角是直角即可.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "给出条件：①两条直线相交成直角；②两条直线互相垂直；③一条直线是另一直线的垂线，并且能否以上述任何一个为条件得出另外两个为内容的结论，正确的是（　　）",
            "opts": [
                "能",
                "不能",
                "有的能有的不能",
                "无法确定"
            ],
            "way": "",
            "step": "答案：A.\n"
        },
        {
            "kind": "example",
            "label": "题型二　利用垂直的性质求角的度数",
            "type": 1,
            "Q": "如图，三条直线相交于点 <i>O</i>.\n若 <i>CO</i> ⊥ <i>AB</i>，∠1 = 56°，则等于（　　）<br><img src=\"21.png\" style=\"width:15em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "30°",
                "34°",
                "45°",
                "56°"
            ],
            "way": "由图可知∠2的对顶角是∠1的余角，根据∠1的度数可求出∠2的度数.\n如图所示，由对顶角相等可知，∠2 = ∠3，因为 <i>CO</i> ⊥ <i>AB</i>，所以∠<i>BOC</i> = 90°，即∠1 + ∠3 = 90°，因为∠1 = 56°，所以∠3 = 90°－∠1 = 34°.\n因此∠2 = ∠3 = 34°.\n",
            "step": "<img src=\"22.png\" style=\"width:15em;vertical-align:middle;padding:5px;\"><br>故本题正确答案为B.\n",
            "notice": "根据垂直的定义可知，两条直线垂直，则两条直线所成的四个角都是直角.\n"
        },
        {
            "kind": "video",
            "title": "与垂直有关的常见计算",
            "name": "与垂直有关的常见计算",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/27657.mp4"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "已知∠<i>AOB</i> 为钝角，过 <i>O</i> 点画 <i>OC</i> ⊥ <i>OB</i>，画 <i>OD</i> ⊥ <i>OA</i>，如图所示.\n若∠<i>COD</i> = 40°，则∠<i>AOB</i> 的度数为（　　）<br><img src=\"23.png\" style=\"width:20em;vertical-align:middle;\">",
            "opts": [
                "140°",
                "160°",
                "120°",
                "110°"
            ],
            "way": "",
            "step": "答案：A.\n"
        },
        {
            "kind": "kpt",
            "name": "画垂线",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "垂线的画法",
                         "sub": ["经过一点（已知直线上或直线外），画已知直线的垂线，步骤如下：<br><b>（1）靠线：</b>让直角三角板的一条直角边落在已知直线上；<br><b>（2）过点：</b>沿直线移动，使直角三角板的另一条直角边经过已知点；<br><b>（3）画线：</b>沿另一条直角边画线，则这条直线就是经过这个点的已知直线的垂线；<br><b>（4）标号：</b>标上垂直符号.\n<br><b>注：</b>过一点画射线或线段的垂线，是指画它们所在直线的垂线，垂足有时在射线的反向延长线或线段的延长线上，如图.\n<br><img src=\"24.png\" style=\"width:16.25em;vertical-align:middle;padding:5px;\">"
                         ]
                     },
                     {
                         "kind": "kpt",
                         "name": "垂线的性质：",
                         "sub": ["在同一平面内，过一点有且只有一条直线与已知直线垂直.\n"
                         ]
                     }
            ]
        },
        {
            "kind": "example",
            "label": "题型一　垂线的画法",
            "type": 9,
            "Q": "请你在下列图形中按照下列要求画图：<br>（1）过 <i>A</i> 点画直线 <i>AE</i> 垂直于线段 <i>BC</i>，垂足为 <i>E</i>；<br>（2）过 <i>C</i> 点画直线 <i>CF</i> 垂直于线段 <i>AD</i>，垂足为 <i>F</i>；<br>（3）过 <i>D</i> 点画直线 <i>DM</i> 垂直于线段 <i>AE</i>，垂足为 <i>M</i>；<br>（4）过 <i>B</i> 点画直线 <i>BN</i> 垂直于线段 <i>AE</i>，垂足为 <i>N</i>.\n<br><img src=\"25.svg\" style=\"width:15em;vertical-align:middle;padding:5px;\">",
            "way": "当两条直线相交所成的四个角中，有一个角是直角时，就说这两条直线互相垂直，其中的一条直线叫做另一条直线的垂线.\n注意到垂线的概念中，只是规定了两直线交角的大小(90°)，并没有规定两条直线的位置如何.\n也就是说，不论一条直线的位置如何，只要另一条与它的交角是90°，其中任何一条直线就是另一条直线的垂线.\n",
            "step": "（1）让三角尺的直角的一边与 <i>BC</i> 重合，另一直角边经过点 <i>A</i> 画出垂线，垂足为点 <i>E</i> 即可；<br>（2）让三角尺的直角的一边与 <i>AE</i> 重合，另一直角边经过点 <i>D</i> 画出垂线，垂足为点 <i>M</i> 即可；<br>（3）让三角尺的直角的一边与 <i>AD</i> 重合，另一直角边经过点 <i>C</i> 画出垂线交 <i>AD</i> 的延长线于点 <i>F</i>，垂足为点 <i>F</i> 即可；<br>（4）让三角尺的直角的一边与 <i>AE</i> 重合，另一直角边经过点 <i>B</i> 画出垂线交 <i>AE</i> 的延长线于点 <i>N</i>，垂足为点 <i>N</i> 即可.\n<br><img src=\"26.svg\" style=\"width:15em;vertical-align:middle;padding:5px;\">",
            "notice": "过已知点画已知直线的垂线，实际上就是过已知点画一条直线，使所画直线与已知直线相交所成的角是90°.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图所示，过点 <i>P</i> 画直线 <i>AB</i> 垂线 <i>CD</i>，三角尺放法正确的是（　　）",
            "opts": [
                "<img src=\"27.svg\" style=\"width:9em;\">",
                "<img src=\"28.svg\" style=\"width:9em;\">",
                "<img src=\"29.svg\" style=\"width:9em;\">",
                "<img src=\"30.svg\" style=\"width:9em;\">"
            ],
            "way": "",
            "step": "答案：C.\n"
        },
        {
            "kind": "example",
            "label": "题型二　垂线的性质1的应用",
            "type": 1,
            "Q": "已知直线 <i>AB</i>，<i>CB</i>，<i>l</i> 在同一平面内，若 <i>AB</i>⊥ <i>l</i>，垂足为 <i>B</i>，<i>CB</i> ⊥ <i>l</i>，垂足也为 <i>B</i>，则符合题意的图形可以是（　　）",
            "opts": [
                "<img src=\"31.png\" style=\"width:4.56em;\">",
                "<img src=\"32.png\" style=\"width:5.06em;\">",
                "<img src=\"33.png\" style=\"width:5.06em;\">",
                "<img src=\"34.png\" style=\"width:5.12em;\">"
            ],
            "way": "根据题意可知，过点 <i>B</i> 有 <i>AB</i>，<i>CB</i> 都与直线 <i>l</i> 垂直，由垂线的性质可知，在平面内，过一点有且只有一条直线与已知直线垂直，所以 <i>A</i>，<i>B</i>，<i>C</i> 三点在一条直线上，且点 <i>B</i> 在直线 <i>l</i> 上.\n",
            "step": "A项，存在两个点 <i>B</i>.\n故A项不符合题意.\n<br>B项，直线 <i>AB</i> 和 <i>AC</i> 与直线 <i>l</i> 的垂足均为点 <i>A</i>.\n故B项不符合题意.\n<br>C项，直线 <i>AB</i> 和 <i>CB</i> 与直线 <i>l</i> 的垂足均为点 <i>B</i>.\n故C项符合题意.\n<br>D项，直线 <i>CB</i> 与直线 <i>l</i> 不垂直.\n故D项不符合题意.\n<br>故选C.\n",
            "notice": "要熟练掌握垂线的性质1.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "如图，在同一平面内，<i>OA</i> ⊥ <i>l</i>，<i>OB</i> ⊥ <i>l</i>，垂足为 <i>O</i>，则 <i>OA</i> 与 <i>OB</i> 重合的理由是（　　）<br><img src=\"35.png\" style=\"width:8.12em;vertical-align:middle;padding:5px;\">",
            "opts": [
                "两点确定一条直线",
                "垂线段最短",
                "已知直线的垂线只有一条",
                "同一平面内，过一点有且只有一条直线与已知直线垂直"
            ],
            "way": "",
            "step": "答案：D.\n"
        },
        {
            "kind": "kpt",
            "name": "垂线的性质2及点到直线的距离",
            "sub": [
                     {
                         "kind": "kpt",
                         "name": "垂直的性质2：",
                         "sub": ["直线外一点与直线上各点连接的所有线段中，垂线段最短.\n简单说成：<b>垂线段最短</b>.\n"
                         ]
                     },
                     {
                         "kind": "kpt",
                         "name": "垂线段：",
                         "sub": ["过直线外一点画已知直线的垂线.\n"
                         ]
                     },
                     {
                         "kind": "kpt",
                         "name": "点到直线的距离：",
                         "sub": ["直线外一点到这条直线的垂线段的长度，叫做点到直线的距离.\n"
                         ]
                     }
            ]
        },
        {
            "kind": "video",
            "title": "垂线段最短",
            "name": "垂线段最短",
            "url": "http://v.leleketang.com/dat/ms/ma/k/video/18525.mp4"
        },
        {
            "kind": "example",
            "label": "题型一　（易错题）垂直性质的应用",
            "type": 9,
            "Q": "如图，<i>AB</i> 是一条河流，要铺设管道将河水引到 <i>C</i>、<i>D</i> 两个用水点，现有两种铺设管道的方案；<br>方案一：分别过 <i>C</i>、<i>D</i> 作 <i>AB</i> 的垂线，垂足为 <i>E</i>、<i>F</i>，沿 <i>CE</i>、<i>DF</i> 铺设管道；<br>方案二：连接 <i>CD</i> 交 <i>AB</i> 于点 <i>P</i>，沿 <i>PC</i>，<i>PD</i> 铺设管道.\n<br>这两种铺设管道的方案哪一种更节省材料?为什么?<br><img src=\"36.png\" style=\"width:12.68em;vertical-align:middle;padding:5px;\">",
            "way": "在 <i>Rt</i>Δ<i>CEP</i> 中，根据斜边大于直角边可得 <i>CP</i> &gt; <i>CE</i>，同理可得：<i>DP</i> &gt; <i>DF</i>；由于 <i>CP</i> &gt; <i>CE</i>，<i>DP</i> &gt; <i>DF</i>，故可得 <i>CP + DP &gt; CE + DF</i>，所以方案一更省材料.\n",
            "step": "根据方案一可得 <i>CE</i> ⊥ <i>AB</i>，<i>DF</i> ⊥ <i>AB</i>，则可得 Δ<i>CEP</i> 和 Δ<i>DFP</i> 是直角三角形.\n<br>根据在直角三角形中，斜边大于直角边可得：<br>在直角 Δ<i>CEP</i> 中，<i>CP &gt; CE</i>，在直角 Δ<i>DFP</i> 中，<i>DP &gt; DF</i>，<br>故 <i>CP + DP &gt; CE + DF</i>，即 <i>CD &gt; CE + DF</i>，<br>因此按方案一铺设管道更节省材料.\n",
            "notice": "本题主要利用“垂线段最短”来解决实际问题，解决这类求最短距离问题时，要注意“垂线段最短”与“两点之间，线段最短”的区别，辨明这两条性质的应用条件：点到直线的最短距离，两点间的最短距离.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图所示，要把水渠中的水引到村庄 <i>C</i>，在渠岸 <i>AB</i> 的什么地方开沟，才能使水沟最短？画出图形，并说明道理.\n<br><img src=\"37.png\" style=\"width:6.43em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "过点 <i>C</i> 作 <i>CD</i> ⊥ <i>AB</i>，垂足为 <i>D</i>，<br>根据垂线段最短，可知在 <i>D</i> 处开挖可以使所挖水沟 <i>CD</i> 最短.\n<br><img src=\"38.png\" style=\"width:6.37em;vertical-align:middle;padding:5px;\">"
        },
        {
            "kind": "example",
            "label": "题型二　（易错题）点到直线的距离的理解",
            "type": 1,
            "Q": "若点 <i>P</i> 为直线 <i>m</i> 外一点，点 <i>A</i>，<i>B</i>，<i>C</i> 为直线 <i>m</i> 上三点，<i>PA</i> = 4<i>cm</i>，<i>PB</i> = 5<i>cm</i>，<i>PC</i> = 2<i>cm</i>，则点 <i>P</i> 到直线 <i>m</i> 的距离（　　）",
            "opts": [
                "等于4<i>cm</i>",
                "等于2<i>cm</i>",
                "小于2<i>cm</i>",
                "不大于2<i>cm</i>"
            ],
            "way": "点到直线的距离是该点到这条直线的垂线段的长度，而垂线段是该点与直线上各点的连线中最短的.\n从条件看，<i>PC</i> 是三条线段中最短的，但不一定是所有连线中最短的，所以点 <i>P</i> 到直线 <i>m</i> 的距离应该是不大于 2<i>cm</i>.\n",
            "step": "点到直线间，垂线段最短，所以当 <i>PC</i> 是点 <i>P</i> 到 <i>AB</i> 的垂线段时，点 <i>P</i> 到直线 <i>m</i> 的距离是 2<i>cm</i>；<br>当 <i>PC</i> 不是 <i>P</i> 到 <i>AB</i> 的垂线段时，<i>P</i> 到直线 <i>m</i> 的距离小于 2<i>cm</i>.\n<br>综上所述，<i>P</i> 到直线 <i>m</i> 的距离为不大于 2<i>cm</i>.\n<br>故选D.\n",
            "notice": "此题需要就 <i>PC</i> 不是点 <i>P</i> 到 <i>AB</i> 的垂线段和 <i>PC</i> 是点 <i>P</i> 到 <i>AB</i> 的垂线段两种情况进行讨论.\n"
        },
        {
            "kind": "ques",
            "type": 1,
            "Q": "和一个已知点 <i>P</i> 的距离等于 3<i>cm</i> 的直线可以画（　　）",
            "opts": [
                "1条",
                "2条",
                "3条",
                "无数条"
            ],
            "way": "",
            "step": "答案：D.\n"
        },
        {
            "kind": "example",
            "label": "题型三　点到直线的距离的识别及方法",
            "type": 9,
            "Q": "如图所示，在三角形 <i>ABC</i> 中，∠<i>ACB</i> = 90°，<i>CD</i> ⊥ <i>AB</i>，垂足为 <i>D</i>.\n若 <i>AC</i> = 4<i>cm</i>，<i>BC</i> = 3<i>cm</i>，<i>AB</i> = 5<i>cm</i>，则点 <i>A</i> 到直线 <i>BC</i> 的距离为________<i>cm</i>，点 <i>B</i> 到直线 <i>AC</i> 的距离为________<i>cm</i>，点 <i>C</i> 到直线 <i>AB</i> 的距离为________<i>cm</i>.\n<br><img src=\"39.png\" style=\"width:8.68em;vertical-align:middle;padding:5px;\">",
            "way": "根据点到直线的距离的定义可知，点 <i>A</i> 到直线 <i>BC</i> 的距离是线段 <i>AC</i> 的长，点 <i>B</i> 到直线 <i>AC</i> 的距离是线段 <i>BC</i> 的长，点 <i>C</i> 到直线 <i>AB</i> 的距离是线段 <i>CD</i> 的长.\n因为三角形 <i>ABC</i> 的面积 <i>S</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AC·BC</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AB·CD</i>，所以 <i>AC·BC = AB·CD</i>，进而可得 <i>CD</i> = 2.4<i>cm</i>.\n",
            "step": "因为∠<i>ACB</i> = 90°，<i>AC</i> = 4<i>cm</i>，所以则点 <i>A</i> 到直线 <i>BC</i> 的距离为 4<i>cm</i>；<br>因为∠<i>ACB</i> = 90°，<i>BC</i> = 3<i>cm</i>，所以则点 <i>B</i> 到直线 <i>AC</i> 的距离为 3<i>cm</i>；<br>根据三角形的面积公式得<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AB·CD</i> =<span class=\"mathquill-rendered-math\">\\frac{1}{2}</span><i>AC·BC</i>，即 <i>CD</i> =<span class=\"mathquill-rendered-math\">\\frac{AC\\cdot BC}{AB}</span>=<span class=\"mathquill-rendered-math\">\\frac{3 × 4}{5}</span>=<span class=\"mathquill-rendered-math\">\\frac{12}{5}</span><i>cm</i>，所以点 <i>C</i> 到直线 <i>AB</i> 的距离为 2.4<i>cm</i>.\n<br>故答案为：<br>4；3；2.4.\n",
            "notice": "正确理解点到直线的距离及两点间的距离是解决此类同题的关键，解决此类问题应注意：（1）点到直线的距离是点到直线的垂线段的长度，而不是重线，也不是垂线段；（2）距离表示线段的长度，是个数量，与线段不能等同；（3）用垂线段的长度表示到直线的距离，其实质是点与重足两点间的距离，体现了<b>数形结合思想</b>.\n"
        },
        {
            "kind": "ques",
            "type": 9,
            "Q": "如图，点 <i>D</i> 到直线 <i>AB</i> 的距离是线段________的长；线段________的长是点 <i>B</i> 到直线 <i>AC</i> 的距离.\n<br><img src=\"40.png\" style=\"width:9em;vertical-align:middle;padding:5px;\">",
            "way": "",
            "step": "根据题意得：点 <i>D</i> 到直线 <i>AB</i> 的距离是线段 <i>DE</i> 的长；线段 <i>BC</i> 的长是点 <i>B</i> 到直线 <i>AC</i> 的距离.\n<br>故答案为：<i>DE</i>、<i>CB</i>.\n"
        }
    ]
}